<?php

namespace App\Policies;

use App\User;
use App\Listing;
use Illuminate\Auth\Access\HandlesAuthorization;

class ListingPolicy
{

	use HandlesAuthorization;

	public function before(User $user, $ability)
	{
		if ($ability !== 'manage' && $this->manage($user)) {
			return true;
		}
	}

	/**
	 * Determine whether the user can view the aircraft.
	 *
	 * @param  \App\User     $user
	 * @param  \App\Listing $listing
	 *
	 * @return mixed
	 */
	public function view(?User $user, Listing $listing)
	{
		return true;
	}

	/**
	 * Determine whether the user can create aircrafts.
	 *
	 * @param  \App\User $user
	 *
	 * @return mixed
	 */
	public function create(User $user)
	{
		$limit = $user->is_dealer
			? \Settings::get('limits.dealer.posts', 0)
			: \Settings::get('limits.user.posts', 0);

		$count = Listing::search()
			->where('author_id', $user->import_id)
			->count();

		return $count < $limit;
	}

	/**
	 * Determine whether the user can update the aircraft.
	 *
	 * @param  \App\User     $user
	 * @param  \App\Listing $listing
	 *
	 * @return mixed
	 */
	public function update(User $user, Listing $listing)
	{
		return $listing->author_id == $user->import_id;
	}

	/**
	 * Determine whether the user can delete the aircraft.
	 *
	 * @param  \App\User     $user
	 * @param  \App\Listing $listing
	 *
	 * @return mixed
	 */
	public function delete(User $user, Listing $listing)
	{
		return $listing->author_id == $user->import_id;
	}

	/**
	 * Determine whether the user can restore the aircraft.
	 *
	 * @param  \App\User     $user
	 * @param  \App\Listing $listing
	 *
	 * @return mixed
	 */
	public function restore(User $user, Listing $listing)
	{
		return $listing->author_id == $user->import_id;
	}

	/**
	 * Determine whether the user can permanently delete the aircraft.
	 *
	 * @param  \App\User     $user
	 * @param  \App\Listing $listing
	 *
	 * @return mixed
	 */
	public function forceDelete(User $user, Listing $listing)
	{
		return false;
	}

	/**
	 * Determine whether the user can manage aircraft.
	 *
	 * @param \App\User $user
	 *
	 * @return bool
	 */
	public function manage(User $user)
	{
		return false;
	}
}
