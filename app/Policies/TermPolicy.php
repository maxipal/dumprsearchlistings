<?php

namespace App\Policies;

use App\User;
use App\Term;
use Illuminate\Auth\Access\HandlesAuthorization;

class TermPolicy
{

	use HandlesAuthorization;

	public function before(User $user, $ability)
	{
		if ($ability !== 'manage' && $this->manage($user)) {
			return true;
		}
	}

	/**
	 * Determine whether the user can view the term.
	 *
	 * @param  \App\User $user
	 * @param  \App\Term $term
	 *
	 * @return mixed
	 */
	public function view(?User $user, Term $term)
	{
		return true;
	}

	/**
	 * Determine whether the user can create terms.
	 *
	 * @param  \App\User $user
	 *
	 * @return mixed
	 */
	public function create(User $user)
	{
		return true;
	}

	/**
	 * Determine whether the user can update the term.
	 *
	 * @param  \App\User $user
	 * @param  \App\Term $term
	 *
	 * @return mixed
	 */
	public function update(User $user, Term $term)
	{
		return false;
	}

	/**
	 * Determine whether the user can delete the term.
	 *
	 * @param  \App\User $user
	 * @param  \App\Term $term
	 *
	 * @return mixed
	 */
	public function delete(User $user, Term $term)
	{
		return false;
	}

	/**
	 * Determine whether the user can restore the term.
	 *
	 * @param  \App\User $user
	 * @param  \App\Term $term
	 *
	 * @return mixed
	 */
	public function restore(User $user, Term $term)
	{
		return false;
	}

	/**
	 * Determine whether the user can permanently delete the term.
	 *
	 * @param  \App\User $user
	 * @param  \App\Term $term
	 *
	 * @return mixed
	 */
	public function forceDelete(User $user, Term $term)
	{
		return false;
	}

	/**
	 * Determine whether the user can manage aircraft.
	 *
	 * @param \App\User $user
	 *
	 * @return bool
	 */
	public function manage(User $user)
	{
		return false;
	}
}
