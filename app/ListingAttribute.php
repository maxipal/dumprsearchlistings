<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ListingAttribute
 *
 * @mixin  \Eloquent
 *
 * @property string $name
 * @property string $type
 * @property array $options
 */
class ListingAttribute extends Model
{

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var  array
	 */
	protected $fillable = [
		'name',
		'type',
		'options',
		'order',
	];

	protected $table = 'listing_attributes';

	protected $casts = [
		'options' => 'array',
		'order' => 'integer',
	];

}
