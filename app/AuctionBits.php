<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Stylemix\Listing\Entity;

class AuctionBits extends Model
{
    protected $table = 'auction_bits';
    protected $fillable = ['accepted'];
    const CREATED_AT = 'date_added';
    const UPDATED_AT = 'date_added';
    public $dbFields = [
		'id',
		'VID',
		'UID',
		'date_added',
		'offer',
		'accepted',
	];
	protected $with = ['author'];

	public function author()
	{
		return $this->belongsTo('App\User', 'UID', 'import_id');
	}
}
