<?php

namespace App;

use App\Attributes\Author;
use App\Attributes\Manager;
use App\Attributes\TermRelation;
use App\Jobs\UpdateUserCountsJob;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Stylemix\Listing\Attribute\Attachment;
use Stylemix\Listing\Attribute\Boolean;
use Stylemix\Listing\Attribute\Date;
use Stylemix\Listing\Attribute\Enum;
use Stylemix\Listing\Attribute\Id;
use Stylemix\Listing\Attribute\Image;
use Stylemix\Listing\Attribute\Keyword;
use Stylemix\Listing\Attribute\Location;
use Stylemix\Listing\Attribute\LongText;
use Stylemix\Listing\Attribute\Numeric;
use Stylemix\Listing\Attribute\PriceWithSale;
use Stylemix\Listing\Attribute\Slug;
use Stylemix\Listing\Attribute\Text;
use Stylemix\Listing\Entity;
use App\Listeners\ListingObserver;
use App\AuctionBits;

/**
 * Class Listing
 *
 * @property integer $id
 * @property integer $import_group_id
 * @property integer $import_id
 * @property \Carbon\Carbon $import_updated_at
 * @property integer $author_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read string $link
 */
class Listing extends Entity
{
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var  array
	 */
	protected $fillable = ['title'];

	protected $table = 'listings';

	public $dbFields = [
		'id',
		'status',
		'slug',
		'title',
		'author_id',
		'import_group_id',
		'import_id',
		'import_updated_at',
		'created_at',
		'updated_at',
		'deleted_at',
	];

	protected $casts = [
		'import_group_id' => 'int',
		'import_id' => 'int',
		'import_updated_at' => 'datetime',
	];

	protected $hidden = [
		'counter_view',
		'counter_list',
		'counter_phone',
		'counter_profile',
		'counter_contact',
	];

	protected $appends = ['link'];

	public function getLinkAttribute()
	{
		return config('wp.listing_single_path') . $this->slug . '/';
	}

	public function dataAttributes(): HasMany
	{
		return $this->hasMany(ListingData::class, 'entity_id');
	}


	public function auctionBids()
    {
        return $this->hasMany(AuctionBits::class,'VID');
    }

	public static function attributeDefinitions(): array
	{
		return array_merge([
			Id::make(),
			Text::make('title')
				->search(['boost' => 3]),
			Date::make('created_at'),
			Date::make('updated_at'),
			Enum::make('status')
				->source([
					'draft' => 'Draft',
					'private' => 'Private',
					'pending' => 'Pending',
					'publish' => 'Publish',
				]),
			Slug::make(),
			Numeric::make('author_id')
				->integer(),
			Author::make('author'),
			PriceWithSale::make('price')
				->label('Choose the price')
				->affix('$')
				->required()
				->useInFilter()
				->separateFilterPanel()
				->filterOptions([
					'min' => 0,
					'max' => 100000,
					'ranges' => [
						0,
						5000,
						10000,
						15000,
						20000,
						30000,
						40000,
						50000,
						70000,
						80000,
						100000,
						150000,
					],
				]),
			Enum::make('currency')
				->required()
				->source([
					'EUR' => '€',
					'USD' => '$',
					'GBP' => '£',
					'CAD' => 'C$',
					'AUD' => 'A$',
					'BRL' => 'R$',
					'CNY' => '¥',
				]),
			Numeric::make('price_sale'),
			Keyword::make('price_label'),
			Image::make('gallery')
				->multiple()
				->toDirectoryByFilenameHash(),
			Keyword::make('videos')
				->multiple(),
//			Keyword::make('tour360'),
			Keyword::make('stock_number'),
			Attachment::make('brochure')
				->mimeTypes('application/pdf')
				->toDirectoryByDate(),
			Boolean::make('is_special'),
//			Boolean::make('special_front'),
			Boolean::make('sold'),
			Text::make('subscription_id'),
			Text::make('subscription_status'),
			Text::make('subscription_expires'),
			Text::make('subscription_expires_r'),
			Text::make('subscription_title'),
			Boolean::make('auction')->label('Auction')->useInFilter(),
			Enum::make('auction_status')->source([
				'open' => 'Open',
				'closed' => 'Closed',
			]),
			Text::make('badge_text')
				->label('Badge Text'),
			Text::make('phases')->label('Phases'),
			Keyword::make('badge_bg_color')
				->label('Badge Color'),
			LongText::make('content')
				->placeholder('Write the seller notes')
				->useInSearch(),
			Location::make('coordinates')
				->useInFilter(),
			Text::make('location')
				->label('Location')
				->icon('stm-service-icon-pin_big')
				->placeholder('Enter the address')
				->useInList(),
			TermRelation::make('features')
				->label('Additional features')
				->multiple()
				->useInFilter(),
			// Counters
//			Numeric::make('counter_view')->integer()->internal(),
//			Numeric::make('counter_list')->integer()->internal(),
//			Numeric::make('counter_phone')->integer()->internal(),
//			Numeric::make('counter_profile')->integer()->internal(),
//			Numeric::make('counter_contact')->integer()->internal(),
			Text::make('meta_title')
				->placeholder('Enter meta title'),
			LongText::make('meta_description'),
            // Custom
            TermRelation::make('sub-types')
                ->label('Sub Category')
                ->icon('stm-service-icon-pin_big')
                ->placeholder('Sub category'),
            Text::make('sub-location')
                ->label('Sub Location')
                ->icon('stm-service-icon-pin_big')
                ->placeholder('Sub location'),
			Text::make('stm_registered')
				->label('Registered')
				->icon('stm-icon-key')
				->placeholder('Enter date')
				->placeholder2('Enter date'),
			Text::make('stm_vin')
				->label('VIN')
				->icon('stm-service-icon-vin_check')
				->placeholder('Enter VIN')
				->placeholder2('Enter VIN'),
			Text::make('stm_history_label')
				->icon('stm-icon-time')
				->label('History')
				->placeholder('Vehicle History Report')
				->placeholder2('Vehicle History Report')
		], app(Manager::class)->generate());
	}

	protected static function boot()
	{
		parent::boot();
		self::observe(ListingObserver::class);

		// Set calculated values
		self::saving(function (Listing $listing) {
			if (!$listing->isDirty(['condition_id', 'make_id', 'serie_id', 'years'])) {
				return;
			}

			$generated_data = self::generatedData($listing);

			$listing->title = $generated_data['title'];
			$listing->slug = $generated_data['slug'];
		});

		// Calculate total listings if changes made
		self::saved(function (Listing $listings) {
			if (!$listings->isDirty(['author_id', 'status', 'sold'])) {
				return;
			}

			$user = User::query()->where('import_id', $listings->author_id)->first();
			if (!$user) {
				return;
			}

			UpdateUserCountsJob::dispatch($user)
				->delay(now()->addSeconds(10));
		});
	}

	public static function generatedData(Listing $listing)
	{
		$title = join(' ',
			collect([
				//Term::find($listing->condition_id),
				Term::find($listing->make_id),
				Term::find($listing->serie_id),
			])
				->filter()
				->pluck('title')
				->all()
		);

		$slug = Str::slug($title) . '-' . $listing->years;

		if ($listing->id) {
			$slug .= '-' . $listing->id;
		} else {
			$slug .= '-' . (DB::table($listing->getTable())->max('id') + 1);
		}

		return [
			'title' => $title,
			'slug' => $slug
		];
	}
}
