<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportListing extends Model
{
    protected $table = 'import_listings';

    protected $fillable = ['import_group_id', 'import_id', 'import_processes_id'];
}
