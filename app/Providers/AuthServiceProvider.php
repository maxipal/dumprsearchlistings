<?php

namespace App\Providers;

use App\Listing;
use App\Policies\ListingPolicy;
use App\Policies\TermPolicy;
use App\Policies\UserPolicy;
use App\Term;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
		Listing::class => ListingPolicy::class,
		Term::class => TermPolicy::class
    ];

	/**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

		Gate::before(function ($user, $ability) {
			if (in_array('administrator', $user->roles)) {
				return true;
			}
		});
    }
}
