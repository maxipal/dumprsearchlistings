<?php

namespace App\Providers;

use App\Listing;
use App\Components\EntityManager;
use App\Term;
use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Stylemix\Listing\Facades\Entities;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		$this->configureMediable();

    	Schema::defaultStringLength(191);

		\Horizon::auth(function ($request) {
			return true;
		});

		if (app()->environment() != 'production') {
//			\DB::enableQueryLog();
		}
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Override EntityManager
        $this->app->singleton('Stylemix\Listing\EntityManager', function () {
            return EntityManager::getInstance();
        });

        Entities::entity(User::class);
        Entities::entity(Listing::class);
        Entities::entity(Term::class);

		if ($this->app->environment() !== 'production') {
			$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
		}
    }

	protected function configureMediable()
	{
		// Adding support to BMP
		$types   = config('mediable.aggregate_types.image.mime_types', []);
		$types[] = 'image/bmp';
		config()->set('mediable.aggregate_types.image.mime_types', $types);

		$exts   = config('mediable.aggregate_types.image.extensions', []);
		$exts[] = 'bmp';
		config()->set('mediable.aggregate_types.image.extensions', $exts);
	}
}
