<?php

namespace App\Stats;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Stats
{
	/**
	 * @var \Illuminate\Http\Request
	 */
	protected $request;

	/**
	 * @var \Symfony\Component\HttpFoundation\Response
	 */
	protected $response;

	protected $db_time = 0;
	protected $db_count = 0;

	protected $es_time = 0;
	protected $es_count = 0;

	/**
	 * Reset stats before after request
	 */
	public function reset()
	{
		$this->db_time = 0;
		$this->db_count = 0;
		$this->es_time = 0;
		$this->es_count = 0;
	}

	public function addDb(float $time)
	{
		$this->db_count++;
		$this->db_time += $time;
	}

	public function addEs(float $time)
	{
		$this->es_count++;
		$this->es_time += $time;
	}

	/**
	 * Commits collected data into logging channel
	 */
	public function commit()
	{
		/** @var \Illuminate\Log\Logger $logger */
		$logger = Log::channel('stats');
		$logger->debug(json_encode([
			'date' => now()->toIso8601String(),
			'authenticated' => Auth::hasUser(),
			'request' => [
				'method' => $this->getRequest()->getMethod(),
				'uri' => $this->getRequest()->getRequestUri(),
				'user_agent' => $this->getRequest()->userAgent(),
				'referer' => $this->getRequest()->headers->get('referer'),
				'route' => ($route = $this->getRequest()->route()) ? $route->uri() : null,
			],
			'response' => [
				'duration' => microtime(true) - LARAVEL_START,
				'status' => $this->getResponse()->getStatusCode(),
				'cached' => $this->getResponse()->headers->has(config('responsecache.cache_time_header_name')),
			],
			'db' => [
				'count' => $this->db_count,
				'time' => $this->db_time,
			],
			'es' => [
				'count' => $this->es_count,
				'time' => $this->es_time,
			],
		]));
	}

	/**
	 * @return \Illuminate\Http\Request
	 */
	public function getRequest(): \Illuminate\Http\Request
	{
		return $this->request;
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 */
	public function setRequest(\Illuminate\Http\Request $request): void
	{
		$this->request = $request;
	}

	/**
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function getResponse(): \Symfony\Component\HttpFoundation\Response
	{
		return $this->response;
	}

	/**
	 * @param \Symfony\Component\HttpFoundation\Response $response
	 */
	public function setResponse(\Symfony\Component\HttpFoundation\Response $response): void
	{
		$this->response = $response;
	}
}
