<?php

namespace App\Stats;

use Monolog\Formatter\JsonFormatter;

class StatsLogFormatter extends JsonFormatter
{
	public function format(array $record)
	{
		return $record['message'] . PHP_EOL;
	}
}
