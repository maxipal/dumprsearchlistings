<?php

namespace App\Stats;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class StatsServiceProvider extends ServiceProvider
{

	public function register()
	{
		$this->app->singleton(Stats::class);
	}

	public function boot()
	{
		/** @var \App\Http\Kernel $kernel */
		$kernel = $this->app->make(\Illuminate\Contracts\Http\Kernel::class);
		$kernel->prependMiddleware(LogStatsMiddleware::class);

		/** @var \App\Stats\Stats $stats */
		$stats = app(Stats::class);

		// Collect DB queries
		DB::listen(function (QueryExecuted $event) use ($stats) {
			$stats->addDb($event->time);
		});

		// Collect ElasticSearch queries
		Config::set('elasticquent.config.tracer', new ElasticSearchTracer($stats));
	}
}
