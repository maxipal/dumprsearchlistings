<?php

namespace App\Stats;

use Closure;

class LogStatsMiddleware
{

	/**
	 * @var \App\Stats\Stats
	 */
	protected $stats;

	function __construct(Stats $stats)
	{
		$this->stats = $stats;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$this->stats->reset();

		/** @var \Symfony\Component\HttpFoundation\Response $response */
		$response = $next($request);

		$this->stats->setRequest($request);
		$this->stats->setResponse($response);
		$this->stats->commit();

		return $response;
	}
}
