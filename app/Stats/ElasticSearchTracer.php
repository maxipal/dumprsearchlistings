<?php

namespace App\Stats;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ElasticSearchTracer extends NullLogger implements LoggerInterface
{

	/**
	 * @var \App\Stats\Stats
	 */
	private $stats;

	public function __construct(Stats $stats)
	{
		$this->stats = $stats;
	}

	public function debug($message, array $context = [])
	{
		if (isset($context['duration'])) {
			$this->stats->addEs($context['duration']);
		}

		parent::debug($message, $context);
	}
}
