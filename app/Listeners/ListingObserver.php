<?php

namespace App\Listeners;

use App\Components\Seo;
use App\Jobs\ListingNotifyWpJob;
use App\Listing;
use App\WpFlushQueue;

class ListingObserver
{
    protected static $skipNotifyingWp = false;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Listing $listing
	 *
     * @return void
     */
    public function saved(Listing $listing)
    {
		if (!$listing->isDirty()) {
			return;
		}

		WpFlushQueue::pushItems([
			$listing->link,
			config('wp.listing_archive_path'),
			'/',
		]);
		WpFlushQueue::pushItems(Seo::getSeoUrlArray($listing));

    	if (self::$skipNotifyingWp) {
    		return;
		}

    	$event = $listing->wasRecentlyCreated ? 'listing-created' : 'listing-updated';
        dispatch(new ListingNotifyWpJob($listing, $event));
    }

    /**
     * Handle the event.
     *
     * @param  Listing $listing
	 *
     * @return void
     */
    public function deleted(Listing $listing)
    {
		// after forced deletion a listing would not be available in job
		if ($listing->isForceDeleting()) {
			return;
		}

		WpFlushQueue::pushItems([
			config('wp.listing_archive_path'),
			'/',
		]);
		WpFlushQueue::pushItems(Seo::getSeoUrlArray($listing));

		if (self::$skipNotifyingWp) {
			return;
		}

        $event = 'listing-deleted';
        dispatch(new ListingNotifyWpJob($listing, $event));
    }

	/**
	 * Run callback function while skipping WP notification
	 *
	 * @param callable $callback
	 */
	public static function skipNotifyingWp(callable $callback)
	{
		self::$skipNotifyingWp = true;
		try {
			$callback();
		}
		finally {
			self::$skipNotifyingWp = false;
		}
    }
}
