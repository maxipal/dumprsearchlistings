<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Stylemix\Base\Traits\ResourceContexts;

class TermResource extends JsonResource
{

	use ResourceContexts;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray($request)
	{
		if (false !== ($result = $this->toContextArray($request))) {
			return $result;
		}

		return parent::toArray($request);
	}

	/**
	 * Get json array for options context
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function optionsContext($request)
	{
		return $this->resource->toOption($request->primary_key);
	}
}
