<?php

namespace App\Http\Resources;

use App\Attributes\Manager;
use Illuminate\Http\Resources\Json\JsonResource;

class ListingAttribute extends JsonResource
{

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'name' => $this->name,
			'type' => app(Manager::class)->map()->get($this->type, class_basename($this->type)),
			'order' => $this->order,
			'options' => $this->options,
		];
	}
}
