<?php

namespace App\Http\Resources;

use App\Fields\AuthorField;
use App\Listing;
use App\Fields\EditorField;
use App\Fields\FeaturesField;
use App\Fields\LocationField;
use Stylemix\Base\Fields\Base;
use Stylemix\Base\Fields\Select;
use Stylemix\Base\FormResource;
use Stylemix\Listing\Form;

class ListingForm extends FormResource
{

	protected $context;

	/**
	 * List of field definitions defined by descendant class
	 *
	 * @return \Stylemix\Base\Fields\Base[]
	 */
	public function fields()
	{
		$main_taxes = ['condition', 'make', 'serie', 'ca-year', 'trim'];
		$attributes = Listing::getAttributeDefinitions()
			->where('internal', '!=', true);
			//->sortBy('formOrder');

		$form = (new Form())
			->extendAll(function ($field, $attribute) {

				$field->icon = $attribute->icon;
				if($field->component !== 'term-relation-field'){
					$field->attributeInstance = $attribute;
				}
			})
			->replace('author_id', function ($field, $attribute) {
				return AuthorField::make('author_id')
					->panel($field->panel)
					->icon($field->icon)
					->placeholder($field->placeholder)
					->required($field->required)
					->label('Author')
					->attributeInstance($attribute);
			})
			->extend('auction', function ($field) {
				// if (request('context') === 'front') {
					//$field->required = true;
					$field->placeholder = '';
					$field->label = 'Auction';
				// };
			})
			->extend('date_modified', function ($field) {
				// if (request('context') === 'front') {
					//$field->required = true;
					$field->placeholder = '';
					$field->label = 'Date Modified in Inport';
				// };
			})
			->extend('auction_status', function ($field) {
				$field->value = $field->value ?: 'open';
			})
			->extend('phases', function ($field) {
				$field->placeholder = '';
				$field->label = 'Phases';
			})
			->extend('price', function ($field) {
				//$field->required = true;
				$field->label = 'Price';
				if (request('context') === 'front') {
					$field->placeholder = '';
					$field->label = '';
				};
			})
			->extend('price_sale', function ($field) {
				if (request('context') === 'front') {
					$field->placeholder = '';
					$field->label = '';
				};
			})
			->extend('price_label', function ($field) {
				if (request('context') === 'front') {
					$field->placeholder = '';
					$field->label = '';
				};
			})
			->extend('currency', function ($field) {
				$field->value = $field->value ?: 'USD';
				$options = $field->options;
				foreach ($options as &$option) {
					$option['label'] = $option['value'] . ' (' . $option['label'] . ')';
				}
				$field->options = $options;
			})
			->extend('status', function ($field) {
				$field->value = $field->value ?: 'publish';
			})
			->extend('badge_bg_color', function ($field) {
				$field->type = 'color';
			})
			->extend('meta_description', function ($field) {
				$field->maxlength = '255';
			})
			->replace('location', function ($field, $attribute) {
				return LocationField::make('location')
					->label($field->label)
					->panel($field->panel)
					->icon($attribute->icon)
					->placeholder($attribute->placeholder)
					->required($attribute->required);
			})
			->replace('content', function ($field) {
				if (request('context') === 'front') {
					$field->placeholder = '';
					$field->label = '';
					return $field;
				};
				return EditorField::make($field->attribute)
					->panel($field->panel)
					->icon($field->icon)
					->placeholder($field->placeholder)
					->required($field->required);
			})
			->replace('years', function ($field, $attribute) {
				$options = [
					'1950',
					'1960',
					'1970',
					'1980'
				];

				$options = array_merge(
					$options,
					range(1981, 2019)
				);

				return Select::make($field->attribute)
					->required()
					->label($field->label)
					->placeholder($field->placeholder)
					->options(
						collect($options)->map(function ($option) {
							return [
								'value' => $option,
								'label' => $option,
							];
						})->all()
					)
					->attributeInstance($attribute);
			});

		$fields = collect();

		if ($this->context !== 'front') {
			$fields = $fields->merge(
				$form->forAttributes($attributes->whereIn('name', [
					'title',
					'status',
					'slug',
					'author',
					'is_special',
					'special_front',
					'sold',
					'badge_text',
					'badge_bg_color',
				]))
			);
		}

		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['meta_title', 'meta_description']))
				->each->panel('seo')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', $main_taxes))
				->each->panel('primary')
		);
		$fields = $fields->merge(
			$form->forAttributes(
				$attributes->filter(function($attribute) {
					return $attribute->useInSingle && $attribute->modificationOption;
				})
			)->each->panel('modifications')
		);
		$fields = $fields->merge(
			$form->forAttributes(
				$attributes
					->filter(function($attribute) use ($main_taxes) {
						$not_included = $main_taxes;
						return $attribute->useInSingle && !$attribute->modificationOption && !in_array($attribute->name, $not_included);
					})
			)
				->each->panel('secondary')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['location']))
				->each->panel('secondary')
		);
		$fields = $fields->merge([
			FeaturesField::make('features_id')
				->multiple(true)
				->rules('integer')
				->label('Additional features')
				->panel('step2')
		]);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['gallery']))
				->each->component('gallery-field')
				->each->panel('step3')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['videos', 'tour360']))
				->each->panel('step4')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['content', 'brochure','phases']))
				->each->panel('step5')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['price', 'price_sale', 'price_label','auction','auction_status']))
				->each->panel('step6')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['countries','towns']))
				->each->panel('secondary')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['stm_registered']))
				->each->panel('secondary')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['stm_vin']))
				->each->panel('secondary')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name', ['stm_history_label']))
				->each->panel('secondary')
		);
		$fields = $fields->merge(
			$form->forAttributes($attributes->whereIn('name',
				[
					'subscription_id',
					'subscription_status',
					'subscription_expires',
					'subscription_expires_r',
					'subscription_title'
				]
			))
				->each->panel('subscription')
		);

		return $fields;
	}

	/**
	 * @param string $context
	 *
	 * @return $this
	 */
	public function context($context)
	{
		$this->context = $context;

		return $this;
	}
}
