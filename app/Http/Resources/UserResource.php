<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use Stylemix\Base\Traits\ResourceContexts;

class UserResource extends JsonResource
{
	use ResourceContexts;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		if (false !== ($result = $this->toContextArray($request))) {
			return $result;
		}

		$data = parent::toArray($request);

		if (Gate::denies('manage', $this->resource)) {
			$data = Arr::only($data, [
				'id',
				'name',
				'created_at',
				'updated_at',
				'first_name',
				'last_name',
				'is_dealer',
				'link',
				'avatar',
				'logo',
				'phone',
				'address',
				'location',
				'rating',
				'count_listings',
				'conditions',
				'makes',
				'_sort',
				'auction_exclude',
			]);
		}

		return $data;
    }

	/**
	 * Get json array for options context
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function optionsContext($request)
	{
		return $this->resource->toOption($request->primary_key);
	}
}
