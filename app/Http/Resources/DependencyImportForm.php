<?php

namespace App\Http\Resources;

use App\Listing;
use Stylemix\Base\Fields\File;
use Stylemix\Base\FormResource;

class DependencyImportForm extends FormResource
{

	/**
	 * List of field definitions defined by descendant class
	 *
	 * @return \Stylemix\Base\Fields\Base[]
	 */
	public function fields()
	{
		return [
			File::make('file')
				->label('Import file')
				->mimeTypes('text/csv')
				->required(),
		];
	}
}
