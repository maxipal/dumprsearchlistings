<?php

namespace App\Http\Resources;

use App\Attributes\Manager;
use Stylemix\Base\Fields\Checkbox;
use Stylemix\Base\Fields\Input;
use Stylemix\Base\Fields\Select;
use Stylemix\Base\FormResource;

/**
 * Class AttributeForm
 *
 * @package App\Http\Resources
 *
 * @property \App\ListingAttribute $resource
 */
class AttributeForm extends FormResource
{

	/**
	 * List of field definitions defined by descendant class
	 *
	 * @return \Stylemix\Base\Fields\Base[]
	 */
	public function fields()
	{
		$manager   = app(Manager::class);
		$attribute = $this->resource;
		$fields    = [
			Input::make('name')
				->label('Attribute name')
				->max(32)
				->helpText('Use only lowercase symbols and underscore')
				->disabled(!!$attribute),
			Select::make('type')
				->label('Internal type')
				->helpText('The type defines how attribute is stored and indexed')
				->options($manager->selectOptions())
				->disabled(!!$attribute),
		];

		if (!$attribute) {
			return $fields;
		}

		$fields = array_merge($fields, $manager->formFor($attribute->type));

		$fields = array_merge($fields, [
			Input::make('options.label')
				->label('Label')
				->helpText('Human readable attribute label'),

			Input::make('options.placeholder1')
				->label('Placeholder for filtering pages')
				->helpText('Human readable attribute placeholder'),
			Input::make('options.placeholder2')
				->label('Placeholder for add vehicle page')
				->helpText('Human readable attribute placeholder'),

			Checkbox::make('options.required')
				->label('Make this field required'),
			Checkbox::make('options.useInSingle')
				->label('Use on single listing page'),
			Checkbox::make('options.useInList')
				->label('Use on list view'),
			Checkbox::make('options.useInGrid')
				->label('Use on grid view'),
			Checkbox::make('options.useInFilter')
				->label('Use on filter'),
			Checkbox::make('options.useInCompare')
				->label('Use in compare page'),
			Checkbox::make('options.useInMap')
				->label('Use on map page'),
			Checkbox::make('options.separateFilterPanel')
				->label('Place filter in separate panel'),
			Checkbox::make('options.useAsCheckboxes')
				->label('Render terms as checkboxes'),
			Checkbox::make('options.hierarchical')
				->label('Make terms hierarchical'),
			Input::make('options.icon')
				->label('Icon class'),
			Checkbox::make('options.filterSection')
				->label('Put in "Filters" Section on inventory page'),
			Checkbox::make('options.modificationOption')
				->label('Use in Modification Section')
		]);

		return $fields;
	}
}
