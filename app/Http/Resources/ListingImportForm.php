<?php

namespace App\Http\Resources;

use App\Listing;
use Stylemix\Base\Fields\File;
use Stylemix\Base\FormResource;

class ListingImportForm extends FormResource
{

	/**
	 * List of field definitions defined by descendant class
	 *
	 * @return \Stylemix\Base\Fields\Base[]
	 */
	public function fields()
	{
		return [
			File::make('file')
				->label('Import file')
				->mimeTypes('text/csv')
				->required(),
			tap(
				Listing::getAttributeDefinitions()->get('status')->formField(),
				function ($field) {
					$field->label('Status for imported records');
					$field->attribute = 'options.status';
					$field->helpText('Default: Draft');
				}
			),
		];
	}
}
