<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ImportProcessResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
			'file' => $this->file,
			'processor' => $this->processor,
			'status' => $this->status,
			'total' => $this->total,
			'processed' => $this->processed,
			'created' => $this->created,
			'updated' => $this->updated,
			'deleted' => $this->deleted,
			'failed' => $this->failed,
			'options' => $this->options,
		];
    }
}
