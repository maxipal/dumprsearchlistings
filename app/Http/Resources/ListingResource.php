<?php

namespace App\Http\Resources;

use App\Listing;
use App\Components\Seo;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

class ListingResource extends JsonResource
{

	/**
	 * @var string Overridden context
	 */
	protected $context = null;

	/**
	 * @var \Stylemix\Listing\AttributeCollection Overridden attributes
	 */
	protected $attributes = null;

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @return array
	 */
	public function toArray($request)
	{
		$context = $this->context ?? $request->context;

		if ($context && method_exists($this, $method = "get{$context}Context")) {
			return $this->$method($request);
		}

		$array = parent::toArray($request);

		return $array;
	}

	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getListContext($request)
	{
		$array = parent::toArray($request);
		$array = Arr::only($array, [
			'id',
			'slug',
			'title',
			'link',
			'price',
			'price_sale',
			'years',
			'gallery',
			'videos',
			'stock_number',
			'brochure',
			'author',
			'sold',
			'is_special',
			'badge_text',
			'badge_bg_color',
			'created_at',
			'auction',
			'auction_status',
			'date_modified',
			'phases',
			'subscription_id',
			'subscription_status',
			'subscription_expires',
			'subscription_expires_r',
			'subscription_title'
		]);
		$array = $this->appends($array);

		$array['get_title'] = $this->getTitleAttributes()
			->map(function ($attr) {
				return [
					'label' => $attr->label,
					'icon' => $attr->icon,
					'value' => $this->getAttributeValue($attr),
					'useInList' => $attr->useInList,
					'useInGrid' => $attr->useInGrid,
				];
			});
		$array['attributes'] = $this->getListAttributes()
			->map(function ($attr) {
				return [
					'label' => $attr->label,
					'icon' => $attr->icon,
					'value' => $this->getAttributeValue($attr),
					'useInList' => $attr->useInList,
					'useInGrid' => $attr->useInGrid,
				];
			});

		return $array;
	}

	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getMapContext($request)
	{
		$array = parent::toArray($request);
		$array = Arr::only($array, [
			'id',
			'slug',
			'link',
			'title',
			'price',
			'price_sale',
			'years',
			'coordinates',
		]);
		$array = $this->appends($array);

		$array['thumbnail'] = Arr::first($this->gallery);
		$array['condition'] = $this->getAttributeValue('condition');
		$array['attributes'] = $this->getListMapAttributes()
			->map(function ($attr) {
				return [
					'label' => $attr->label,
					'icon' => $attr->icon,
					'value' => $this->getAttributeValue($attr),
					'useInMap' => $attr->useInMap,
				];
			});

		return $array;
	}

	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getSingleContext($request)
	{
		$array = parent::toArray($request);
		$array = $this->appends($array);

		$array['attributes'] = $this->getAttributesForSingle()
			->filter()
			->map(function ($attr) {
				return [
					'label' => $attr->label,
					'icon' => $attr->icon,
					'value' => $this->getAttributeValue($attr),
					'affix' => $attr->affix
				];
			})
			->sortBy('order');

		$array['breadcrumbs'] = app(Seo::class)->breadcrumbs($this->resource);

		return $array;
	}

	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getCompareContext($request)
	{
		$array = parent::toArray($request);
		$array = Arr::only($array, [
			'id',
			'author_id',
			'slug',
			'link',
			'title',
			'price',
			'price_sale',
			'years',
			'features',
		]);
		$array = $this->appends($array);

		$array['thumbnail'] = Arr::first($this->gallery);

		if ($this->attributes) {
			$array['attributes'] = $this->attributes
				->map(function ($attr) {
					return [
						'label' => $attr->label,
						'icon' => $attr->icon,
						'value' => $this->getAttributeValue($attr)
					];
				});
		}

		return $array;
	}

	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getShortContext($request)
	{
		$array = parent::toArray($request);
		$array = Arr::only($array, [
			'id',
			'author_id',
			'slug',
			'link',
			'title',
			'price',
			'price_sale',
		]);
		$array = $this->appends($array);

		$array['thumbnail'] = Arr::first($this->gallery);
		$array['body'] = $this->getAttributeValue('body');

		return $array;
	}

	/**
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return array
	 */
	public function getPrivateContext($request)
	{
		$array = $this->getListContext($request);
		$array['status'] = $this->status;

		if (Gate::allows('update', $this->resource)) {
			$array['counters'] = [
				'counter_view' => (int)$this->counter_view,
				'counter_list' => (int)$this->counter_list,
				'counter_phone' => (int)$this->counter_phone,
				'counter_profile' => (int)$this->counter_profile,
				'counter_contact' => (int)$this->counter_contact,
			];
		}

		return $array;
	}

	/**
	 * Appends additional view data
	 *
	 * @param array $array
	 *
	 * @return array
	 */
	protected function appends($array)
	{
		$array += [
			'currency' => $this->currency ?: 'EUR',
			'currency_text' => $this->currency_text ?: env('CURRENCY', '$'),
		];

		if ($this->price_label) {
			$array['price_view'] = $this->price_label;
		} else {
			$currency_text = $array['currency_text'];
			if($this->price_final){
				$array['price_view'] = $currency_text . number_format($this->price_final, 0, '.', ' ');
			}
			if($this->price_sale){
				$array['price_view'] = $currency_text . number_format($this->price_sale, 0, '.', ' ');
				$array['price_regular_view'] = $this->price_sale ? $currency_text . number_format($this->price, 0, '.', ' ') : '';
			}else{
				$array['price_view'] = $currency_text . number_format($this->price, 0, '.', ' ');
			}
		}

		return $array;
	}

	/**
	 * @param \Stylemix\Listing\Attribute\Base|string $attribute
	 *
	 * @return string
	 */
	protected function getAttributeValue($attribute)
	{
		if (is_string($attribute)) {
			$attribute = Listing::getAttributeDefinitions()->get($attribute);
		}

		$value = $this->resource[$attribute->name];

		switch ($attribute->type) {
			case 'term_relation':
				$value = array_filter($attribute->multiple ? $value : [$value]);
				$value = Arr::pluck($value, 'title');
				if (!$attribute->multiple) {
					$value = Arr::first($value);
				}
		}

		if ($attribute->multiple && is_array($value)) {
			$value = join(', ', $value);
		}

		return $value;
	}

	/**
	 * @return \Stylemix\Listing\Attribute\Base[]|\Stylemix\Listing\AttributeCollection
	 */
	protected function getListAttributes()
	{
//		$list = [
//			[
//				'name' => 'condition',
//			],
//			[
//				'name' => 'location',
//			],
//		];

		$list = Listing::getAttributeDefinitions()
			->filter(function ($attribute) {
				return $attribute->useInList || $attribute->useInGrid;
			})->keyBy('name');

//		$list = collect($list)->map(function ($config) {
//			/** @var \Stylemix\Listing\Attribute\Base $attribute */
//			$attribute = Listing::getAttributeDefinitions()->get($config['name']);
//
//			if (!$attribute) {
//				return null;
//			}
//
//			foreach ($config as $key => $value) {
//				$attribute[$key] = $value;
//			}
//
//			return $attribute;
//		})->filter()->keyBy('name');

		return $list;
	}

	/**
	 * @return \Stylemix\Listing\Attribute\Base[]|\Stylemix\Listing\AttributeCollection
	 */
	protected function getTitleAttributes()
	{
		$allow_title = ['condition', 'ca-year', 'make', 'serie'];
		$list = Listing::getAttributeDefinitions()
			->filter(function ($attribute) use ($allow_title) {
				return in_array($attribute->name, $allow_title);
			})->keyBy('name');

		return $list;
	}

	/**
	 * @return \Stylemix\Listing\Attribute\Base[]|\Stylemix\Listing\AttributeCollection
	 */
	protected function getListMapAttributes()
	{
		return Listing::getAttributeDefinitions()
			->filter(function ($attr) {
				return $attr->useInMap;
			});
	}

	/**
	 * @return \Stylemix\Listing\Attribute\Base[]|\Stylemix\Listing\AttributeCollection
	 */
	protected function getAttributesForSingle()
	{
		return Listing::getAttributeDefinitions()
			->filter(function ($attr) {
				return $attr->useInSingle;
			});
	}

	/**
	 * @param string $context
	 *
	 * @return ListingResource
	 */
	public function setContext(string $context)
	{
		$this->context = $context;

		return $this;
	}

	/**
	 * @param \Stylemix\Listing\AttributeCollection $attributes
	 */
	public function setAttributes(\Stylemix\Listing\AttributeCollection $attributes)
	{
		$this->attributes = $attributes;
	}
}
