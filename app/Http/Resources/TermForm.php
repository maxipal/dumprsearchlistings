<?php

namespace App\Http\Resources;

use App\Fields\EditorField;
use App\Fields\TermRelationField;
use App\Term;
use Illuminate\Support\Arr;
use Stylemix\Base\Fields\Select;
use Stylemix\Base\FormResource;
use Stylemix\Listing\Form;

class TermForm extends FormResource
{

	protected $taxonomy;

	/**
	 * List of field definitions defined by descendant class
	 *
	 * @return \Stylemix\Base\Fields\Base[]
	 */
	public function fields()
	{
		if ($this->resource) {
			$this->taxonomy = $this->resource->taxonomy;
		}

		$attributes = Term::getAttributeDefinitions()
			->where('internal', '!=', true)
			->whereIn('name', [
				'title',
				'slug',
				'image',
				'order',
				'description',
			]);

		$fields = (new Form())
			->replace('description', function($field) {
				return EditorField::make($field->attribute)
					->label($field->label)
					->helpText($field->helpText)
					->required($field->required);
			})
			->forAttributes($attributes);

		if (!$this->taxonomy) {
			$fields->prepend(
				Select::make('taxonomy')
					->label('Taxonomy')
					->options(collect(Term::taxonomies())->map(function ($taxonomy) {
						return [
							'value' => $taxonomy['name'],
							'label' => $taxonomy['label'],
						];
					})->values())
			);
		}

		$fields->push(
			TermRelationField::make('parent_id')
				->label('Parent term')
				->ajax()
				->taxonomy($this->taxonomy)
		);

		if ($this->taxonomy && $depends = Arr::get(Term::dependenceSchema(), $this->taxonomy)) {
			$fields->push(
				TermRelationField::make('depends_id')
					->label('Depends on terms')
					->ajax()
					->multiple()
					->taxonomy($depends)
			);
		}

		return $fields;
	}

	/**
	 * @param mixed $taxonomy
	 *
	 * @return TermForm
	 */
	public function taxonomy($taxonomy)
	{
		$this->taxonomy = $taxonomy;

		return $this;
	}
}
