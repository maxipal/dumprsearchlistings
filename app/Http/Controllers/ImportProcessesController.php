<?php

namespace App\Http\Controllers;

use App\Http\Resources\ListingImportForm;
use App\Http\Resources\DependencyImportForm;
use App\ImportProcess;
use App\Http\Requests\ImportProcessRequest;
use App\Http\Resources\ImportProcessResource;
use App\Jobs\ListingXMLImportPrepareJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class ImportProcessesController extends Controller
{

	/**
	 * Display a listing of importProcess.
	 * @inheritdoc
	 */
	public function index(Request $request)
	{
		$query = ImportProcess::where('processor', $request->processor);

		return ImportProcessResource::collection($query->get());
	}

	public function create(Request $request)
	{
		$this->validate($request, [
			'processor' => 'required|in:aircraft,dependencies',
		]);

		$forms = [
			'aircraft' => function() {
				return new ListingImportForm();
			},
			'dependencies' => function() {
				return new DependencyImportForm();
			},
		];

		return $forms[$request->processor]();
	}

	/**
	 * Store a newly created importProcess in storage.
	 * @inheritdoc
	 */
	public function store(ImportProcessRequest $request)
	{
		$file = $request->file('file')
			->storeAs('import/' . date('Y-m-d'), strtolower(Str::random()) . '.csv');

        $importProcess = ImportProcess::forceCreate([
        	'file' => $file,
			'processor' => $request->processor,
			'options' => $request->options,
			'status' => 'pending',
			'total' => 0,
			'processed' => 0,
			'created' => 0,
			'updated' => 0,
			'deleted' => 0,
			'failed' => 0,
		]);

        $processors = [
        	'aircraft' => function(ImportProcess $import) {
        		return new ListingImportPrepareJob($import);
			},
        	'dependencies' => function(ImportProcess $import) {
        		return new DependencyImportProcessJob($import);
			},
		];

        dispatch(
        	($processors[$importProcess->processor]($importProcess))
				->onQueue('import')
		);

        return new ImportProcessResource($importProcess);
	}

	/**
	 * Display the specified importProcess.
	 * @inheritdoc
	 */
	public function show(ImportProcess $importProcess)
	{
		return new ImportProcessResource($importProcess);
	}

	/**
	 * Update the specified importProcess in storage.
	 * @inheritdoc
	 */
    public function update(Request $request, ImportProcess $importProcess)
	{
        $importProcess->update($request->input());

        return new ImportProcessResource($importProcess);
	}

	/**
	 * Remove the specified importProcess from storage.
	 * @inheritdoc
	 */
	public function destroy(ImportProcess $importProcess)
	{
        $importProcess->delete();

        return response(['message' => 'Import process has been deleted successfully'], 200);
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function startXmlImport(Request $request)
	{
		// source: null | 'url_com'
		ListingXMLImportPrepareJob::dispatch($request->source);

		return Response::json([
			'message' => 'XML import job has been dispatched.',
		]);
	}
}
