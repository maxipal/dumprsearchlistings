<?php

namespace App\Http\Controllers;

use App\Http\Requests\TermRequest;
use App\Http\Resources\TermForm;
use App\Term;
use App\Http\Resources\TermResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;
use App\Components\Filter;
use App\Components\Seo;
use Illuminate\Support\Str;

class TermController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth', ['only' => ['store', 'edit', 'update', 'destroy']]);
//		$this->middleware('cache.response:300', ['only' => ['index']]);
	}

	public function index(Request $request)
	{
		$builder = Term::search($request);

		if ($request->s) {
			$builder->search($request->s);
		}

		if ($request->parent_id) {
			$builder->where('parent_id', $request->parent_id);
		}

		if ($request->depends) {
			foreach (Arr::wrap($request->depends) as $id) {
				$builder->where('depends', $id);
			}
		}

		$terms  = $builder->get();

		return TermResource::collection($terms->paginate($builder->getPerPage()))->additional([
			'took' => $terms->took(),
			'aggregations' => $terms->getAggregations(),
		]);
	}

	public function getParentTerms(Request $request)
	{
		$query = Term::where('parent_id', '!=', null);
		if($request->taxonomy){
			$query = $query->where('taxonomy', $request->taxonomy);
		};
		$terms = $query->get(['parent_id']);
		$collection = $terms->map(function ($array) {
			return $array->parent_id;
		});
		$parent_ids = array_unique( $collection->toArray() );

		$query = Term::whereIn('id', $parent_ids)->get();
		return TermResource::collection($query);
	}


	public function sitemap(Request $request)
	{
		$res = [];
		$builder = Term::all()->sortBy("title");
		foreach ($builder as $index => $term) {
			if($term->taxonomy == 'serie' || $term->taxonomy == 'make'){
				if($term->taxonomy == 'make'){
					$res[$term->id][0] = $term->toArray();
				}else{
					if(!empty($term->depends_id[0]))
						$res[$term->depends_id[0]][] = $term->toArray();
				}
			}
		}

		uasort($res, function($a, $b){
			if ($a[0]['title'] == $b[0]['title']) {
				return 0;
			}
			return ($a[0]['title'] < $b[0]['title']) ? -1 : 1;
		});
		return $res;
	}

	public function suggestions(Request $request)
	{
		if (!$request->s) {
			return ['data' => []];
		}

		$results = Term::search($request)
			->setRequest([
				'query' => [
					'match_phrase_prefix' => [
						'title' => [
							'query' => $request->s,
						],
					],
				],
			])
			->setSource(['id', 'title', 'taxonomy', 'slug'])
			->get();

		if($results){
			$seo = new Seo($request);
			foreach ($results as $key => $value) {
				$link = $seo->inventoryLinkTerm($value);
				$results[$key]->url = config('wp.url').$link;
			}
		}

		return TermResource::collection($results);
	}

	// Get all Countries and Cities in single dropdown
	public function locations(Request $request) {

		$image_url = '';
		$ct_loc = [];

		$countries = Term::search()->where('taxonomy', 'countries')->get();
		$cids = data_get($countries, "*.id");

		$towns = Term::search()->where('taxonomy', 'towns')->where('depends',$cids)->setPerPage(9999)->get()->sortBy("title");

		foreach ($countries as $country) {
			if(!empty($country->image)){
				$image_url = $country->image['url'];
			}else{
				$image_url = '';
			}

			$ct_loc[$country->slug]['name'] = $country->title;
			$ct_loc[$country->slug]['lat'] = $country->loc_lat;
			$ct_loc[$country->slug]['lng'] = $country->loc_lon;
			$ct_loc[$country->slug]['flag_url'] = $image_url;
			$ct_loc[$country->slug]['id'] = $country->id;

			foreach ($towns as $town) {
				if(!in_array($country->id, $town->depends_id)) continue;
				if(!empty($town->image)){
					$image_url = $town->image['url'];
				}else{
					$image_url = '';
				}
				$ct_loc[$country->slug]['towns'][] = array(
					'name' => $town->title,
					'lat' => $town->loc_lat,
					'lng' => $town->loc_lon,
					'flag_url' => $image_url,
					'id' => $town->id,
				);
			}
		}
		return $ct_loc;
	}


	public function termTaxonomies(Request $request)
	{
		if (!$request->taxonomies) {
			return ['data' => []];
		}
		$results = Term::search()->where('id',explode(',', $request->taxonomies))->get();
		return TermResource::collection($results);
	}

	public function create(Request $request)
	{
		$this->authorize('create', Term::class);

		return TermForm::make()->taxonomy($request->taxonomy);
	}

	public function store(TermRequest $request)
	{
		$this->authorize('create', Term::class);

		$request->fill($term = new Term());

		// Slug may be empty string
		// To make auto slug generation work
		// it should not exists in attributes
		if (!$term->slug) {
			unset($term->slug);
		}

		$term->taxonomy = $request->taxonomy;
		$term->save();

		return new TermResource($term);
	}

	public function show($id)
	{
		$term = Term::search()->where('id', $id)->first();

		if (!$term) {
			throw new ModelNotFoundException(Term::class . ': ' . $id);
		}

		$this->authorize('view', $term);

		return new TermResource($term);
	}

	public function slug($slug)
	{
		$term = Term::search()->where('slug', $slug)->first();

		if (!$term) {
			throw new ModelNotFoundException(Term::class . ': ' . $slug);
		}

		return new TermResource($term);
	}

	public function attributes()
	{
		return Term::getAttributeDefinitions();
	}

	public function taxonomies()
	{
		return Term::taxonomies();
	}

	public function edit(Term $term)
	{
		$this->authorize('update', $term);

		return TermForm::make($term);
	}

	public function update(TermRequest $request, Term $term)
	{
		$this->authorize('update', $term);

		$request->fill($term)->save();

		return TermResource::make($term);
	}

	public function bulkUpdate(Request $request)
	{
		$this->validate($request, [
			'ids' => 'array',
			'data' => 'required|array',
		]);

		/** @var \Illuminate\Database\Eloquent\Collection $results */
		$results = Term::whereIn('id', $request->ids)
			->get()
			->each(function (Term $term) use ($request) {
				if (Gate::denies('update', $term)) {
					return;
				}
				$term->fill($request->data)->save();
			});

		return $results->mapInto(TermResource::class);
	}

	public function destroy(Term $term)
	{
		$this->authorize('delete', $term);

		$term->delete();

		return response([], 202);
	}

	public function bulkDestroy(Request $request)
	{
		$this->validate($request, [
			'ids' => 'array',
		]);

		/** @var \Illuminate\Database\Eloquent\Collection $results */
		$results = Term::whereIn('id', $request->ids)
			->get()
			->each(function (Term $term) {
				if (Gate::denies('delete', $term)) {
					return;
				}

				$term->delete();
			});

		return $results->mapInto(TermResource::class);
	}

	public function importFeatures() {
		$groups = [
			'Engine Upgrades' =>
				'Heads,Valve Covers,Engine Dress-Up,Cams,Engine Internals,Engine Block,Radiator,Cooling System,Oil System,Ignition System,Belts,Pulleys,Battery,Hoses,Crate Engine',
			'Air & Fuel Upgrades' =>
				'Air Intake,Filter,Intake Pipe,Intake Manifold,Throttle Body,Intercooler,Piping,Carburetor,Fuel Injection,Fuel System,Fuel Pump,Fuel Cell/Tank,Secondary Fuel System',
			'Exhaust Upgrades' =>
				'Headers,Exhaust Manifold,Hangers & Brackets,Resonator,Catted,Cat Delete,Mufflers,Cut Outs,Tips',
			'Drivetrain Upgrades' =>
				'Clutch Cylinder,Shifter,Transfer Case,Ring & Pinion,Differential,Driveshaft,Clutch Kit,Clutch Disk,Fly Wheel,Torque Converter,CV Joints',
			'Suspension Upgrades' =>
				'Shocks,Struts,Springs,Control Arms,Sway Bars,Crossmembers,Bushings,Motor Mounts,Steering,Camber Kit,Roll Cage',
			'Brake Upgrades' =>
				'Master Cylinder,Slave Cylinder,Disk Brakes,Drum Brakes,Shoes,Cylinders,Big Brake Kit,Calipers,Solid Disk,Pads,Brake Cooling System,Line Lock,Hydro E-Brake',
			'Wheel/Tire Upgrades' =>
				'Lug Nuts/Bolts,Center Lock,Lug Conversion Kit,Wheel Spacer,Painted Wheels',
			'Interior Upgrades' =>
				'Racing Seats,Harness,Steering Wheel,Shift Knob,Pedals,Gauges,Instrument Cluster,A/C,A/C Delete,Seat Delete',
			'Audio Upgrades' =>
				'Head Unit,Tweeters,Speakers,Mid Range,Subwoofer,Custom Box,Crossover,Equalizer,Amplifier,Audio Battery,Capacitor,Wiring,Sound Damping,Competition Audio',
			'Exterior Upgrades' =>
				'Front Bumper,Fenders,Rear Bumper,Side Skirts,Front Lip,Side Lips,Rear Diffuser,Scoops,Hood,Spoiler,Wheelie Bar,Decals,Grille,Roof Rack,Truck Bed,Bed Lid,Side Steps,Bumper Guard',
			'Lighting Upgrades' =>
				'Headlights,LED Headlights,Projector Headlights,HID Head Lights,Halo Kits,Custom Headlights,Fog Lights,Tailights,Aux Lights,Light Bars,Markers,Under-Car Lighting'
		];

		foreach ($groups as $g_name => $group) {
			$slug = Str::slug($g_name);
			$sgroup = Term::firstOrNew(['slug' => $slug, 'taxonomy' => 'feature_group']);
			$sgroup->forceFill([
				'import_id' => 222,
				'title' => html_entity_decode($g_name),
				'slug' => $slug,
				'taxonomy' => 'feature_group',
			]);
			Term::withoutEvents(function () use ($sgroup) {
				$sgroup->save();
			});

			$features = explode(',', $group);
			foreach ($features as $feature) {
				$f_slug = Str::slug($feature);
				$term = Term::firstOrNew(['slug' => $f_slug, 'taxonomy' => 'features']);
				$term->forceFill([
					'import_id' => 333,
					'title' => html_entity_decode($feature),
					'slug' => $f_slug,
					'taxonomy' => 'features',
					'depends_id' => [$sgroup->id],
				]);
				Term::withoutEvents(function () use ($term) {
					$term->save();
				});
			}
		}
	}

}
