<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth', ['only' => ['store', 'update', 'destroy', 'token']]);
//		$this->middleware('cache.response:300', ['only' => ['index']]);
	}

	public function index(Request $request)
	{
		$builder = User::search($request);

		$users = $builder->get();

		return UserResource::collection($users->paginate($builder->getPerPage()));
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'import_id' => 'required|integer',
			'name' => 'required|string',
			'email' => 'required|email',
		]);

		/** @var User $user */
		$user = User::firstOrNew(['email' => $request->email]);

		$data = $request->all();
		if (!$user->exists) {
			$data += [
				'password' => bcrypt(Str::random(8)),
				'roles' => ['subscriber'],
			];
		}

		$user->fill($data)
//			->fillCounts()
			->save();

		return UserResource::make($user);
	}

	public function show(User $user)
	{
		return UserResource::make($user);
	}

	public function update(Request $request, User $user)
	{
		$user->update($request->all());

		return UserResource::make($user);
	}

	public function destroy(User $user)
	{
		$user->delete();

		return response()->json([], 201);
	}

	public function token(User $user)
	{
		$this->authorize('manage', User::class);

		$token = auth('api')->login($user);

		return response()->json([
			'token' => $token,
			'expires_in' => auth('api')->factory()->getTTL() * 60
		]);
	}
}
