<?php

namespace App\Http\Controllers;

use App\Settings\SeoForm;
//use GuzzleHttp\Psr7\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Fluent;
//use Stylemix\Settings\Facades\Settings;
use App\Settings\SeoRequest;


class SettingsController extends Controller
{

	public function listingsSeo(SeoRequest $request)
	{
		$data = $request->fill(new Fluent());
		\Settings::set($data->toArray());

		return response()->json();
    }

	public function listingsSeoForm()
	{
		return SeoForm::make(\Settings::all());
    }

    public function saveCustomize(Request $request) {
		\Settings::set('customize', $request->customize);
	}

}
