<?php

namespace App\Http\Controllers;

use App\Listing;
use App\ListingAttribute;
use App\Attributes\Manager;
use App\Http\Requests\ListingAttributeRequest;
use App\Http\Resources\ListingAttribute as ListingAttributeResource;
use App\Http\Resources\AttributeForm;
use Illuminate\Http\Request;
use Stylemix\Listing\Facades\Entities;

class ListingAttributesController extends Controller
{

	public function index(Request $request)
	{
		return ListingAttributeResource::collection(ListingAttribute::orderBy('order', 'asc')->get());
	}

	public function create()
	{
		return AttributeForm::make();
	}

	public function store(ListingAttributeRequest $request)
	{
		$listing_attribute = ListingAttribute::create(
			$request->input() + ['order' => ListingAttribute::max('order') + 1]
		);
		app(Manager::class)->flush();

		$this->updateMapping();

		return new ListingAttributeResource($listing_attribute);
	}

	public function show(ListingAttribute $listing_attribute)
	{
		return new ListingAttributeResource($listing_attribute);
	}

	public function edit(ListingAttribute $listing_attribute)
	{
		return AttributeForm::make($listing_attribute);
	}

	public function update(Request $request, ListingAttribute $listing_attribute)
	{
		$listing_attribute->update($request->only(['options']));
		app(Manager::class)->flush();

		$this->updateMapping();

		return new ListingAttributeResource($listing_attribute);
	}

	public function destroy(ListingAttribute $listing_attribute)
	{
		$listing_attribute->delete();

		$manager = app(Manager::class);
		$manager->deleteValues($listing_attribute->name);
		$manager->flush();

		Entities::remapStatus('listing', true);

		return response([], 202);
	}

	protected function updateMapping()
	{
		try {
			Listing::putMapping();
		}
		catch (\Exception $e) {
			report($e);
			Entities::remapStatus('listing', true);
		}
	}
}
