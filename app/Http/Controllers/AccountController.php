<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\AuctionBits;
use App\Http\Resources\UserResource;

class AccountController extends Controller
{

	/**
	 * Get current user info.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return mixed
	 */
	public function user(Request $request)
	{
		$user = $request->user();
		$res = [];
		if(!empty($request->with)){
			$with = explode(',', $request->with);
			$builder = Listing::search();
			foreach ($with as $event) {
				if ($event == 'auctions'){
					$builder->where('author_id', $user->import_id);
					$res['auctions'] = $builder->where('auction', 1)->count();
				}
				if($event == 'cars'){
					$builder->where('author_id', $user->import_id);
					$res['cars'] = $builder->whereNot('auction', true)->count();
				}
				if($event == 'bids'){
					$uid = $request->get('uid');
					$bids = AuctionBits::where('UID', '!=', $request->user()->id)
						->select('auction_bits.*')
						->join('listings', function($join){
							$join->on('auction_bits.VID', '=', 'listings.id')
						->where('listings.status','=','publish');
					})
			   		->where('listings.author_id','=',$uid)
					->orderBy('date_added', 'desc');
					$res['bids_cnt'] = $bids->count();
					$res['bids_ids'] = $bids->pluck('id');
				}
				if($event == 'auctions_ids'){
					$res['auctions_ids'] = Listing::search([
						'context' => 'list',
						'auction' => 1,
						'filters'=>'all',
						'per_page' => 100,
						'stm_lat' => 0,
						'stm_lng' => 0,
						'featured' => 'with',
					])->pluck('id');
				}else{
					$res['auctions_ids'] = [];
				}
			}
		}
		$user->setAttribute('meta_res', $res);
		return $user;
	}
}
