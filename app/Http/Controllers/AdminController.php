<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Stylemix\Listing\EntityManager;

class AdminController extends Controller
{

	public function indexStatus()
	{
		return \Stylemix\Listing\EntityManager::getInstance()->status();
	}

	public function reindex(Request $request, $entity)
	{
		$request->request->set('entity', $entity);
		$this->validate($request, [
			'entity' => 'required|in:' . join(',', array_keys(EntityManager::getInstance()->getBindings())),
		]);

		$remap = EntityManager::getInstance()->remapStatus($entity);

		Artisan::queue('elastic:index', [
			'entity' => $request->entity,
			'--remap' => $remap,
		]);

		return response()->json();
	}
}
