<?php

namespace App\Http\Controllers;

use App\Components\Filter;
use App\Components\Seo;
use App\Http\Middleware\CacheListingIndex;
use App\Http\Requests\ListingRequest;
use App\Http\Resources\ListingForm;
use App\Http\Resources\ListingResource;
use App\ImportProcess;
use App\Listing;
use App\Mail\AdminListingCreated;
use App\Mail\DealerListingCreated;
use App\Term;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Fluent;

class ListingController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth', ['only' => ['my', 'store', 'edit', 'update', 'destroy']]);
//		$this->middleware('cache.response:60', ['only' => ['index', 'coordinates']]);
//		$this->middleware('cache.response:300', ['only' => ['terms', 'filters']]);
	}

	public function index(Request $request, Filter $filter)
	{
		$builder = Listing::search($request);

		if (Gate::denies('manage', Listing::class)) {
			$builder
				->where('status', 'publish')
				->whereNot('sold', true);
		}


		if ($request->has('auction')){
			if($request->auction == '1'){
				$builder->where('auction', 1);
			}else{
				$builder->whereNot('auction', true);
			}
		}

		$filter->buildQuery($builder);

		$listing = $builder->get();

		// Get Locations
		$ct_loc = [];
		if (($aggregations = $listing->getAggregations()) && $aggregations->has('countries') && $aggregations->has('towns')) {
			$ct_loc = $this->getLocationHierarchy($aggregations);
		}

		$additional = compact('ct_loc');

		$with = explode(',', $request->with);

		if (in_array('badges', $with) || $request->filter) {
			$additional['badges'] = $filter->badges();
		}

		if ($request->filters) {
			$additional['filters'] = $filter->form($listing);
		}

		if ($request->featured == 'with') {
			$featured = $builder->where('is_special', true)
				->aggregations([])
				->setPerPage(3)
				->get();
			$additional['featured'] = ListingResource::collection($featured);
		}

		if (in_array('seo', $with)) {
			$seo = app(Seo::class)->inventorySeo();
			$additional += $seo;
		}

		if (in_array('alternate', $with) && !$listing->totalHits()) {
			$this->alternateResults($request, $additional);
		}

		return ListingResource::collection($listing->paginate())
			->additional($additional);
	}


	protected function getLocationHierarchy($aggregations)
	{
		$image_url = '';
		$ct_loc = [];
		$cids = Arr::pluck($aggregations['countries'], 'id');

		$countries = Term::search()->where('id',$cids)->get();
		$towns = Term::search()->where('taxonomy', 'towns')->where('depends',$cids)->setPerPage(9999)->get()->sortBy("title");

		foreach ($countries as $country) {
			if(!empty($country->image)){
				$image_url = $country->image['url'];
			}else{
				$image_url = '';
			}

			$ct_loc[$country->slug]['name'] = $country->title;
			$ct_loc[$country->slug]['lat'] = $country->loc_lat;
			$ct_loc[$country->slug]['lng'] = $country->loc_lon;
			$ct_loc[$country->slug]['flag_url'] = $image_url;
			$ct_loc[$country->slug]['id'] = $country->id;

			foreach ($towns as $town) {
				foreach ($aggregations['towns'] as $temp){
					if(!in_array($country->id, $town->depends_id)) continue;
					if($town->id == $temp['id']){
						if(!empty($town->image)){
							$image_url = $town->image['url'];
						}else{
							$image_url = '';
						}
						$ct_loc[$country->slug]['towns'][] = array(
							'name' => $town->title .' ('.$temp['count'].')',
							'lat' => $town->loc_lat,
							'lng' => $town->loc_lon,
							'flag_url' => $image_url,
							'id' => $town->id,
						);
					}
				}
			}
		}
		return $ct_loc;

	}


	public function auctions(Request $request)
	{
		$builder = Listing::search($request);

		if(!empty($request->v_status)){
			if($request->v_status == 'without_my_proposal'){
				$without_my_proposal = AuctionController::getCarsWithMyBids()->toArray();
				$builder->whereNot('id',$without_my_proposal);
			}elseif($request->v_status == 'pending'){
				$user = Auth::user();
				$auction_exclude = (array)$user->auction_exclude;

				$builder->where('id', $auction_exclude);
			}elseif($request->v_status == 'with_my_proposal'){
				$with_my_proposal = AuctionController::getCarsWithMyBids()->toArray();
				$builder->where('id', $with_my_proposal);
			}
		}else{
			$builder->where('status', 'publish');
		}

		if ($request->has('auction')){
			if($request->auction == '1'){
				$builder->where('auction', 1);
			}else{
				$builder->whereNot('auction', true);
			}
		}


		$listing = $builder->get();

		$bids = array();
		foreach ($listing as $key => $li) {
			if($li->auctionBids){
				foreach ($li->auctionBids as $bid) {
					$bids[$li->id][] = array(
						'id' => $bid->id,
						'offer' => $bid->offer,
						'accepted' => $bid->accepted,
						'uid' => ($bid->author ? $bid->author['import_id'] : 0),
						'dealer_name' => ($bid->author ? $bid->author['name'] : ''),
						'dealer_link' => ($bid->author ? $bid->author['link'] : ''),
					);
				}
			}
		}

		$additional = [
			'took' => $listing->took(),
			'db_queries' => \DB::getQueryLog(),
			'bids' => $bids,
		];


		return ListingResource::collection($listing->paginate())
			->additional($additional);
	}

	public function my(Request $request)
	{
		$builder = Listing::search($request);
		$builder->where('author_id', \Auth::user()->import_id);

		if ($request->has('auction')){
			if($request->auction == '1'){
				$builder->where('auction', 1);
			}else{
				$builder->whereNot('auction', true);
			}
		}


		$listing = $builder->get();

		$bids = array();
		foreach ($listing as $key => $li) {
			if($li->auctionBids){
				foreach ($li->auctionBids as $bid) {
					$bids[$li->id][] = array(
						'id' => $bid->id,
						'offer' => $bid->offer,
						'accepted' => $bid->accepted,
						'uid' => ($bid->author ? $bid->author['import_id'] : 0),
						'dealer_name' => ($bid->author ? $bid->author['name'] : ''),
						'dealer_link' => ($bid->author ? $bid->author['link'] : ''),
					);
				}
			}
		}

		$additional = [
			'took' => $listing->took(),
			'db_queries' => \DB::getQueryLog(),
			'bids' => $bids,
		];


		return ListingResource::collection($listing->paginate())
			->additional($additional);
	}

	public function matches(Request $request)
	{
		$path = $request->get('path');
        $terms = app(Seo::class)->matchInventoryPath($path);
        $matches = $breadcrumbs = [];

        if ($terms && stripos($path, 'tag/') === false) {
            $matches = $terms->pluck('id', 'taxonomy');
            $breadcrumbs = app(Seo::class)->breadcrumbs($terms);
        }

        return compact('path', 'matches', 'breadcrumbs');
	}

	public function filters(Request $request)
	{
		return $this->getFilters($request);
	}

	public function terms(Request $request)
	{
		$taxonomies = Arr::wrap($request->taxonomy);

		if (empty($taxonomies)) {
			return [];
		}

		$aggregations = array_map(function () { return true; }, array_combine($taxonomies, $taxonomies));

		$result = Listing::search()
		    ->whereNot('auction', true)
			->aggregations($aggregations)
			->get();

		$aggregations = $result->getAggregations()->only($taxonomies);

		$aggregations = $aggregations->map(function ($aggregation) use ($request) {
			$aggregation = collect($aggregation)->sortBy('title', SORT_STRING);

			$terms = Term::search()
				->where('id', $aggregation->pluck('id')->all())
				->setPerPage(2000)
				->get()
				->keyBy('id');

			foreach ($aggregation as $i => $item) {
				if (!($term = $terms->get($item['id']))) {
					continue;
				}
				$item['link'] = '/' . $term->slug . '/';
				$item['image'] = $term->image;
				$aggregation[$i] = $item;
			}

			return $aggregation->values();
		});

		return $aggregations;
	}

	public function singleTerms(Request $request)
	{
		$taxonomies = Arr::wrap($request->taxonomy);

		if (empty($taxonomies)) {
			return [];
		}
		$terms = Term::search()
			->where('taxonomy', $taxonomies)
			->setPerPage(2000)
			->get()
			->keyBy('id');
		foreach ($terms as $term) {
			$term['link'] =  '/inventory/?filter['.$request->taxonomy.']=' . $term->id;
		}
		return $terms;

//		$aggregations = array_map(function () { return true; }, array_combine($taxonomies, $taxonomies));
//
//		$result = Listing::search()
//			->whereNot('auction', true)
//			->aggregations($aggregations)
//			->get();
//
//		$aggregations = $result->getAggregations()->only($taxonomies);
//
//		$aggregations = $aggregations->map(function ($aggregation) use ($request) {
//			$aggregation = collect($aggregation)->sortBy('title', SORT_STRING);
//
//			$terms = Term::search()
//				->where('id', $aggregation->pluck('id')->all())
//				->setPerPage(2000)
//				->get()
//				->keyBy('id');
//
//			foreach ($aggregation as $i => $item) {
//				if (!($term = $terms->get($item['id']))) {
//					continue;
//				}
//				$item['link'] = '/' . $term->slug . '/';
//				$item['image'] = $term->image;
//				$aggregation[$i] = $item;
//			}
//
//			return $aggregation->values();
//		});

//		return $aggregations;
	}

	public function coordinates(Request $request)
	{
		if ($request->aggregations == 'all') {
			$aggregations = Listing::getAttributeDefinitions()->where('useInFilter', true)->map(function () {
				return true;
			})->all();
			$request->request->set('aggregations', $aggregations);
		}

		$builder = Listing::search($request)
			->where('status', 'publish')
			->setPerPage(1);
		$filter = app(Filter::class);

		if ($request->has('auction')){
			if($request->auction == '1'){
				$builder->where('auction', 1);
			}else{
				$builder->whereNot('auction', true);
			}
		}
		$filter->buildQuery($builder);
		$res = $builder
			->setSource(['id'])
			->buildWith(function ($body) {
				if (!isset($body['aggs'])) {
					$body['aggs'] = [];
				}

				$grid = [
					'geohash_grid' => [
						'field' => 'coordinates',
						'precision' => 7,
					],
					'aggs' => [
						'center' => [
							'geo_centroid' => [
								'field' => 'coordinates',
							],
						],
					],
				];

				$post_filter = data_get($body, 'post_filter', []);
				if (empty($post_filter)) {
					$body['aggs']['grid'] = $grid;
				}
				else {
					$body['aggs']['grid'] = [
						'filter' => $post_filter,
						'aggs' => [
							'filtered' => $grid,
						],
					];
				}

				return $body;
			});


		if ($request->aggregations) {
			$filters = $this->getFilters($request, $res);
		}

		$total = $res->totalHits();
		$grid  = $res->getAggregations()->get('grid');
		$data  = collect(
			Arr::has($grid, 'filtered')
				? Arr::get($grid, 'filtered.buckets')
				: Arr::get($grid, 'buckets')
		)
			->map(function ($bucket) {
				$count  = $bucket['doc_count'];
				$latlng = join(',', $bucket['center']['location']);

				return compact('latlng', 'count');
			})
			->all();

		return compact('data', 'filters', 'total');
	}

	public function compare(Request $request)
	{
		$result = Listing::search()->where('id', $request->ids)->get();
		$attributes = Listing::getAttributeDefinitions()->where('useInCompare', true);

		foreach ($attributes as $attribute) {
			$values = $result->pluck($attribute->name)->filter();

			if ($values->isEmpty()) {
				$attributes->forget($attribute->name);
			}
		}

		$resource = ListingResource::collection($result);
		$resource->resource->each->setContext('compare');
		$resource->resource->each->setAttributes($attributes);
		$resource->additional(compact('attributes'));

		return $resource;
	}

	public function show($id)
	{
		$listing = Listing::search()->where('id', $id)->first();

		if (!$listing) {
			throw (new ModelNotFoundException)->setModel(Listing::class, $id);
		}

		$this->authorize('view', $listing);

		$seo = app(Seo::class)->singleSeo($listing);
		return (new ListingResource($listing))->additional(compact('seo'));
	}

	public function slug(Request $request, $slug)
	{
		$listing = Listing::search()->where('slug', $slug)->first();

		if (!$listing) {
			throw (new ModelNotFoundException)->setModel(Listing::class, $slug);
		}

		$this->authorize('view', $listing);

		if ($request->similar) {
			$similar = Listing::search()
				->where('status', 'publish')
				->where('serie', $listing->serie_id)
				->whereNot('id', $listing->id)
				->setPerPage(3)
				->get();

			if ($similar->count() < 3 && ($serie_parent = data_get($listing, 'serie.parent_id'))) {
				$_similar = Listing::search()
					->where('status', 'publish')
					->where('serie', $serie_parent)
					->whereNot('id', array_merge([$listing->id], $similar->pluck('id')->all()))
					->setPerPage(3 - $similar->count())
					->get();

				$similar = $similar->merge($_similar);
			}

			if ($similar->count() < 3 && ($types_id = $listing->types_id)) {
				$_similar = Listing::search()
					->where('status', 'publish')
					->where('body', $types_id)
					->whereNot('id', array_merge([$listing->id], $similar->pluck('id')->all()))
					->setPerPage(3 - $similar->count())
					->get();

				$similar = $similar->merge($_similar);
			}

			if ($similar->count() < 3 && ($make_id = $listing->make_id)) {
				$_similar = Listing::search()
					->where('status', 'publish')
					->where('make', $make_id)
					->whereNot('id', array_merge([$listing->id], $similar->pluck('id')->all()))
					->setPerPage(3 - $similar->count())
					->get();

				$similar = $similar->merge($_similar);
			}

			$similar = ListingResource::collection($similar);
			$similar->resource->each->setContext('short');
		}
		$seo = app(Seo::class)->singleSeo($listing);
		return (new ListingResource($listing))
			->additional(compact('similar'))
			->additional(compact('seo'));
	}

	public function attributes()
	{
		return Listing::getAttributeDefinitions();
	}

	public function event(Request $request)
	{
		$this->validate($request, [
			'event' => 'in:view,list,profile,phone,contact',
		]);

		$event = $request->event;
		$user_id = $request->user_id;
		$ids = array_filter(array_map('intval', explode(',', $request->ids)));

		/** @var Listing $listing */
		$results = Listing::findMany($ids);
		$key     = 'counter_' . $event;
		foreach ($results as $listing) {
			if ($listing->author_id == $user_id) {
				$ids = array_diff($ids, [$listing->id]);
				continue;
			}

			$listing->setAttribute($key, $counter = ((int) $listing->getAttribute($key) + 1));
			Listing::withoutEvents(function () use ($listing) {
				$listing->save();
			});
		}

		return [$event => $ids];
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @param array $additional
	 */
	protected function alternateResults(Request $request, &$additional)
	{
		$alternateRequest = new Fluent($request->all());
		$alternate        = null;

		foreach (['serie', 'make', 'types', 's'] as $key) {
			if ($key == 's') {
				unset($alternateRequest['s']);
				unset($alternateRequest['search']);
			}
			else {
				$filter = $alternateRequest->get('filter', []);

				if (!isset($filter[$key])) {
					continue;
				}

				$alternateRequest->filter = Arr::except($filter, $key);
			}

			$_alternate = Listing::search($alternateRequest)
				->where('status', 'publish')
				->whereNot('sold', true)
				->get();

			if (isset($additional['badges'])) {
				$additional['badges'] = $additional['badges']->filter(function ($badge) use ($key) {
					return $badge['slug'] != $key;
				})->values();
			}

			if ($_alternate->totalHits()) {
				$alternate = $_alternate;
				break;
			}
		}

		if ($alternate) {
			$additional['alternate'] = ListingResource::collection($alternate);
		}
	}

	public function create(Request $request)
	{
		return ListingForm::make()
			->context($request->context);
	}

	public function store(ListingRequest $request)
	{
		$user = Auth::user();
		$this->authorize('create', Listing::class);
		$customize = \Settings::get('customize', []);

		$listing = $request->fill(new Listing());

		if (!$listing->author_id || Gate::denies('manage', Listing::class)) {
			$listing->author_id = $user->import_id;
		}

		if ($listing->status == 'publish' && Gate::denies('publish', Listing::class)) {
			$listing->status = 'publish';
		}

		if($user->is_dealer){
			if(!empty($customize['dealer_premoderation'] && $customize['dealer_premoderation'])){
				$listing->status = 'pending';
			}
		}else{
			if(!empty($customize['user_premoderation'] && $customize['user_premoderation'])){
				$listing->status = 'pending';
			}
		}
		$restrictions = $this->getRestrictions($user);
		$listing->save();
		$mail_template = \Settings::get('auction_created');
		$temp = [];
		$temp['link'] = config('wp.url').$listing->link;
		$temp['title'] = $listing->title;
		// Send email of auction to all dealers
		if($listing->auction){
			$dealers = User::search()->where('is_dealer', 1)->setPerPage(9999)->get();
			foreach ($dealers as $key => $dealer) {
				//if($key > 2) break;
				$temp['name'] = !empty($dealer->first_name) ? $dealer->first_name : $dealer->email;
				$temp['email'] = $dealer->email;
				$temp['author_id'] = $dealer->id;
				Mail::to($dealer->email)
					->send(new DealerListingCreated($temp, $mail_template));
			}
			$temp['name'] = '';
			$temp['email'] = '';
			$temp['author_id'] = $listing->author_id;
			// Send admin email
			Mail::to(config('app.admin_email'))
				->send(new DealerListingCreated($temp, $mail_template));
		}else{
		// Send admin email
		$mail_template = \Settings::get('add_a_car_template');
		Mail::to(config('app.admin_email'))
			->send(new AdminListingCreated($listing, $mail_template));
		}


		return ListingResource::make($listing);
	}

	public function edit(Request $request, Listing $listing)
	{
		$this->authorize('update', $listing);

		return ListingForm::make($listing)
			->context($request->context);
	}

	public function update(ListingRequest $request, Listing $listing)
	{
		$this->authorize('update', $listing);

		$request->fill($listing);

		if ($listing->getOriginal('status') !== 'publish'
			&& $listing->status === 'publish'
			&& Gate::denies('publish', Listing::class)) {
			$listing->status = 'pending';
		}

		$listing->save();
		// Send admin email
		if($request->price){
			$mail_template = \Settings::get('update_a_car_template');
			Mail::to(config('app.admin_email'))
				->send(new AdminListingCreated($listing, $mail_template));
		}

		return ListingResource::make($listing);
	}

	public function bulkUpdate(Request $request)
	{
		$this->validate($request, [
			'ids' => 'array',
			'data' => 'required|array',
		]);

		/** @var \Illuminate\Database\Eloquent\Collection $results */
		$results = Listing::whereIn('id', $request->ids)
			->get()
			->each(function (Listing $listing) use ($request) {
				if (Gate::denies('update', $listing)) {
					return;
				}
				$listing->fill($request->data)->save();
			});

		return $results->mapInto(ListingResource::class);
	}

	public function destroy(Listing $listing)
	{
		$this->authorize('delete', $listing);

		$listing->delete();

		return response()->json([], 202);
	}

	public function bulkDestroy(Request $request)
	{
		$this->validate($request, [
			'ids' => 'array',
		]);

		/** @var \Illuminate\Database\Eloquent\Collection $results */
		$results = Listing::whereIn('id', $request->ids)
			->get()
			->each(function (Listing $listing) {
				if (Gate::denies('delete', $listing)) {
					return;
				}

				$listing->delete();
			});

		return $results->mapInto(ListingResource::class);
	}

	public function generated(Request $request)
	{
		$data     = $request->all();
		$listing = new Listing();
		$listing->fill($data);
		$generated = Listing::generatedData($listing);

		return $generated;
	}

	protected function getFilters(Request $request, $results = null)
	{
		return app(Filter::class)->setRequest($request)->form($results);
	}

	public function getImportStatus(){
		$processes = ImportProcess::all();
		return response()->json($processes, 200);
	}

	public function getRestrictions($user) {
		$created_posts = Listing::where('author_id', $user->import_id)->get();
		$customize = \Settings::get('customize', []);
		$posts_allowed = !empty($customize['user_post_limit']) ? $customize['user_post_limit'] : '3';
		$restrictions['posts_allowed'] = intval($posts_allowed);
		$restrictions['premoderation'] = !empty($customize['user_premoderation']) ? $customize['user_premoderation'] : true;
		$restrictions['images'] = !empty($customize['user_post_images_limit']) ? $customize['user_post_images_limit'] : '5';
		$restrictions['role'] = $user->is_dealer ? 'dealer' : 'user';

		$user_can_create = intval($posts_allowed) - intval($created_posts->count());

		if ($user_can_create < 1) {
			$user_can_create = 0;
		}
		$restrictions['posts'] = intval($user_can_create);

		return $restrictions;
	}

}
