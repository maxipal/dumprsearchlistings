<?php

namespace App\Http\Controllers;

use App\AuctionBits;
use App\Mail\AdminListingCreated;
use App\Mail\DealerListingCreated;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Listing;
use App\Mail\DealerAcceptedOffer;
use App\Mail\DealerNotifyOffer;
use Illuminate\Support\Facades\Mail;

class AuctionController extends Controller
{
	public function __construct()
	{
		//$this->middleware('auth', ['only' => ['my', 'store', 'edit', 'update', 'destroy']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		//
	}

	public function hide(Request $request){
		$vid = $request->get('vehicle');
		$user = Auth::user();

		$auction_exclude = (array)$user->auction_exclude;
		$offset = array_search($vid, $auction_exclude);
		if($offset === false){
			$auction_exclude[] = $vid;
			$message = 'Vehicle hidden';
		}else{
			unset($auction_exclude[$offset]);
			$message = 'Vehicle visible';
		}
		$user->auction_exclude = $auction_exclude;
		$user->save();
		return response()->json([
			'message' => $message,
			'errors' => []
		]);
	}

	/**
	 * Add new bit in DB.
	 *
	 * @return
	 */
	public function create(Request $request)
	{
		$this->validate($request, [
			'offer' => 'nullable|required|max:255',
			'uid' => 'nullable|min:1|not_in:0|required',
			'vehicle' => 'nullable|min:1|not_in:0|required',
		]);
		$bid = new AuctionBits;
		$bid->UID = $request->get('uid');
		$bid->VID = $request->get('vehicle');
		$bid->offer = $request->get('offer');
		$bid->save();
		$bid = $bid->toArray();
		$listing = Listing::search()->where('id',$request->get('vehicle'))->first()->toArray();

		$bid['link'] = config('wp.url').$listing['link'];
		$bid['title'] = $listing['title'];
		$author = $this->getAuthorListingByBidId($bid['id']);

		$mail_template = \Settings::get('offer_notify');
		$params = [
			'id' => $bid['id'],
			'link' => $bid['link'],
			'title' => $bid['title'],
			'offer' => $bid['offer']
		];
		$content = stripslashes($mail_template['content']);
		$subject = $mail_template['subject'];
		foreach ($params as $key => $datum) {
			$content = str_replace("{{ $key }}", $datum, $content);
			$subject = str_replace("{{ $key }}", $datum, $subject);
		}
		$template = [
			'content' => $content,
			'subject' => $subject,
			'to' => $author['email']
		];

		Mail::send([],[], function($message) use ($template) {
			$message->setBody($template['content'], 'text/html');
			$message->to($template['to']);
			$message->subject($template['subject']);
		    $message->from(config('mail.from.address'),config('mail.from.name'));
		});

		return response()->json([
			'message' => 'Oferta juaj shpërndahet!',
			'errors' => []
		]);
	}

	/**
	 * Get bid ID
	 *
	 * @param  Bid ID
	 * @return
	 */
	public function getBidById($bid_id)
	{
		return AuctionBits::where('id',$bid_id)->first()->toArray();
	}

	public static function getCarsWithMyBids(){
		$user_id = Auth::user()->import_id;
		$my_auctions = AuctionBits::where('UID', $user_id)->distinct()->pluck('VID');
		return $my_auctions;
	}

	/**
	 * Set bid accept
	 *
	 * @param  Bid ID
	 * @return
	 */
	public function bitAccept(Request $request)
	{
		if(Auth::user()){
			$bid = $this->getBidById($request->get('bit_id'));
			AuctionBits::find($request->get('bit_id'))->update(['accepted' => $bid['UID']]);
			Listing::find($bid['VID'])->update(['auction_status' => 'closed']);
			Listing::find($bid['VID'])->update(['status' => 'pending']);

			$author = User::find(1)->where('import_id',$bid['UID'])->first()->toArray();

			//Mail::to($author['email'])->send(new DealerAcceptedOffer($bid));
			$mail_template = \Settings::get('offer_accepted');
			$params = [
				'id' => $bid['id'],
				'offer' => $bid['offer']
			];
			$content = stripslashes($mail_template['content']);
			$subject = $mail_template['subject'];
			foreach ($params as $key => $datum) {
				$content = str_replace("{{ $key }}", $datum, $content);
				$subject = str_replace("{{ $key }}", $datum, $subject);
			}


			$template = [
				'content' => $content,
				'subject' => $subject,
				'to' => $author['email']
			];

			Mail::send([],[], function($message) use ($template) {
				$message->setBody($template['content'], 'text/html');
				$message->to($template['to']);
			    $message->subject($template['subject']);
			    $message->from(config('mail.from.address'),config('mail.from.name'));
			});
			return response()->json([
				'success' => 1,
				'message' => 'Oferta pranoj! Tregtari do të dërgojë njoftim',
			]);
		}else{
			return response()->json([
				'success' => 0,
				'message' => 'Unauthorize!',
			]);
		}

	}

	/**
	 * Get author car by bit ID
	 *
	 * @param  Bit ID
	 * @return
	 */
	public function getAuthorListingByBidId($bid_id)
	{
		$vehicle_id = AuctionBits::where('id',$bid_id)->first()->toArray()['VID'];
		$vehicle = Listing::search()->where('id',$vehicle_id)->first()->toArray();
		$uid = $vehicle['author']['id'];
		return User::find(1)->where('id',$uid)->first()->toArray();
	}

	/**
	 * Get Offers by Car ID
	 *
	 * @param  Car ID
	 * @return JSON array of bits
	 */
	public static function carOffers($vid)
	{
		$bids = AuctionBits::where('VID', $vid)
		   ->orderBy('id', 'desc')
		   ->get();
		if($bids->count()){
			$res = [];
			foreach ($bids->toArray() as $index => $bid) {
				$user = User::find(1)->where('import_id',$bid['UID'])->first()->toArray();
				$res[$index]['offer'] = $bid['offer'];
				$res[$index]['accepted'] = $bid['accepted'];
			}
			return $res;
		}
	}

	/**
	 * Get Bits by Car ID
	 *
	 * @param  Car ID
	 * @return JSON array of bits
	 */
	public static function carbids($vid)
	{
		$bids = AuctionBits::where('VID', $vid)
		   ->orderBy('date_added', 'desc')
		   ->get();
		if($bids->count()){
			$res = [];
			foreach ($bids->toArray() as $index => $bid) {
				if(!empty(User::find(1)->where('import_id',$bid['UID'])->first()))
					$user = User::find(1)->where('import_id',$bid['UID'])->first()->toArray();
			    else
					continue;

				$res[$index]['id'] = $bid['id'];
				$res[$index]['offer'] = $bid['offer'];
				$res[$index]['uid'] = $user['import_id'];
				$res[$index]['accepted'] = $bid['accepted'];
				$res[$index]['dealer_name'] = $user['name'];
				$res[$index]['dealer_email'] = $user['email'];
				$res[$index]['dealer_link'] = $user['link'];
			}
			return $res;
		}
	}


	/**
	 * Get user Bits by user ID
	 *
	 * @param  User ID
	 * @return JSON array of bits
	 */
	public function my(Request $request)
	{
		if($request->has('status') && $request->status == 'exclude'){
			$uid = $request->get('uid');
			$bits = AuctionBits::where('UID', '!=', $uid)
				->select('auction_bits.*')
				// ->where('accepted',null)
				->join('listings', function($join){
					$join->on('auction_bits.VID', '=', 'listings.id')
					->where('listings.status','=','publish');
				})
			   ->where('listings.author_id','=',$uid)
			   ->orderBy('date_added', 'desc')
			   ->get();
		}else{
			$bits = AuctionBits::where('UID', $request->get('uid'))
			   ->orderBy('date_added', 'desc')
			   ->get();
		}

		if($bits->count()){
			$res = [];
			$builder = new Listing;
			$user = Auth::user();
			$cuser_id = Auth::user()->import_id;

			foreach ($bits->toArray() as $index => $bid) {
				$res[$index]['myaccepted'] = $cuser_id;

				$listing = $builder::find($bid['VID'])->toArray();
				if($listing['auction']){
					$res[$index]['offers'] = $this->carOffers($bid['VID']);;
					$res[$index]['accepted'] = $bid['accepted'];
					$res[$index]['vehicle_name'] = $listing['title'];
					$res[$index]['vehicle_link'] = $listing['link'];
					$res[$index]['id'] = $bid['id'];
				}

			}
			return response()->json([
				'data' => $res,
			]);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$this->{$id}();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}


	public function get_mail_templ(Request $request)
	{
		$template = $request->template;
		$res = \Settings::get($template, ['subject' => '','content' => '']);
		return $res;
	}

	public function set_mail_templ(Request $request)
	{
		$template = $request->template;
		\Settings::set($template, $request->raw);
		die;
	}
}
