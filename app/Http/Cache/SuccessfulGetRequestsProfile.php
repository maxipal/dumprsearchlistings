<?php

namespace App\Http\Cache;

use App\Listing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Spatie\ResponseCache\CacheProfiles\CacheAllSuccessfulGetRequests;

class SuccessfulGetRequestsProfile extends CacheAllSuccessfulGetRequests
{

	public function shouldCacheRequest(Request $request): bool
	{
		if (app()->environment('testing')) {
			return false;
		}

		// too many variations
		if ($request['s'] || $request['filter.mileage'] || $request['sort.location']
			|| $request['stm_lat'] || $request['stm_lng']) {
			return false;
		}

		// No cache for administrators
		if (Gate::allows('manage', Listing::class)) {
			return false;
		}

		// allow client to request without caching
		if ($request->has('nocache')) {
			return false;
		}

		return $request->isMethod('get');
	}
}
