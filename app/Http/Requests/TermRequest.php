<?php

namespace App\Http\Requests;

use App\Http\Resources\TermForm;
use Stylemix\Base\FormRequest;

class TermRequest extends FormRequest
{

	/**
	 * @param mixed $resource
	 *
	 * @return \Stylemix\Base\FormResource
	 */
	protected function formResource()
	{
		return TermForm::make()
 			->taxonomy($this->taxonomy);
	}
}
