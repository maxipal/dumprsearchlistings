<?php

namespace App\Http\Requests;

use App\Http\Resources\ListingForm;
use Stylemix\Base\Fields\Base;
use Stylemix\Base\FormRequest;

class ListingRequest extends FormRequest
{

	/**
	 * @param mixed $resource
	 *
	 * @return \Stylemix\Base\FormResource
	 */
	protected function formResource()
	{
		return ListingForm::make();
	}

	public function attributes()
	{
		return $this->getFormResource()->getFields()->mapWithKeys(function (Base $field) {
			return [$field->attribute => strtolower($field->label)];
		})->all();
	}
}
