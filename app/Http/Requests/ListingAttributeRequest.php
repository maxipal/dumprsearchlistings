<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListingAttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|unique:listing_attributes|max:16',
            'type' => 'required|string|max:64',
            'options' => 'array',
        ];
    }
}
