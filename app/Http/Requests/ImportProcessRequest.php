<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportProcessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimetypes:text/plain',
            'processor' => 'required|in:listing,dependencies',
            'options' => 'sometimes|array',
        ];
    }
}
