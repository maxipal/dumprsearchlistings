<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{

	protected function authenticate($request, array $guards)
	{
		try {
			parent::authenticate($request, $guards);
		}
		catch (AuthenticationException $e) {
			throw new AuthenticationException(trans('auth.unauthenticated'), $guards, $e->redirectTo());
		}
	}

	/**
	 * Get the path the user should be redirected to when they are not authenticated.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return string
	 */
	protected function redirectTo($request)
	{
		return null;
	}
}
