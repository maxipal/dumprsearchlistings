<?php

namespace App\Attributes;

use App\Term;
use Stylemix\Listing\Attribute\Relation;

class TermDepends extends Relation
{
	public function __construct(string $name) {
		parent::__construct($name, Term::class);

		$this->mapProperties(['id', 'title', 'slug', 'order']);
	}

	/**
	 * @inheritdoc
	 */
	public function elasticMapping($mapping)
	{
		$mapping[$this->fillableName] = ['type' => 'integer'];

		$mapping[$this->name] = [
			'type' => 'nested',
			'properties' => [
				'id' => ['type' => 'integer'],
				'title' => ['type' => 'text'],
				'slug' => ['type' => 'keyword'],
				'order' => ['type' => 'integer'],
			],
		];
	}
}
