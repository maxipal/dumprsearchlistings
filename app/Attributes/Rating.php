<?php

namespace App\Attributes;

use Stylemix\Listing\Attribute\AppliesDefaultSort;
use Stylemix\Listing\Attribute\Base;
use Stylemix\Listing\Attribute\Sortable;

class Rating extends Base implements Sortable
{
	use AppliesDefaultSort;

	public function elasticMapping($mapping)
	{
		$mapping[$this->name] = [
			'properties' => [
				'average' => ['type' => 'scaled_float', 'scaling_factor' => 2],
				'rate1' => ['type' => 'scaled_float', 'scaling_factor' => 2],
				'rate2' => ['type' => 'scaled_float', 'scaling_factor' => 2],
				'rate3' => ['type' => 'scaled_float', 'scaling_factor' => 2],
				'likes' => ['type' => 'integer'],
				'dislikes' => ['type' => 'integer'],
				'count' => ['type' => 'integer'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function sorts()
	{
		return [
			$this->name . '.average',
			$this->name . '.rate1',
			$this->name . '.rate2',
			$this->name . '.rate3',
			$this->name . '.likes',
			$this->name . '.dislikes',
			$this->name . '.count',
		];
	}

}
