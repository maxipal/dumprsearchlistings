<?php

namespace App\Attributes;

use App\Listing;
use App\ListingAttribute;
use App\ListingData;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use Stylemix\Base\Fields\Checkbox;
use Stylemix\Base\Fields\Input;
use Stylemix\Listing\Attribute\Keyword;
use Stylemix\Listing\Attribute\Numeric;
use Stylemix\Listing\Attribute\Text;

class Manager
{
	protected static $resolved;

	/**
	 * Defines available attributes types that can be managed
	 *
	 * @return \Stylemix\Listing\Attribute\Base[]
	 */
	public function available()
	{
		if (!isset(static::$resolved)) {
			static::$resolved = [
				Numeric::class => [
					'form' => function () {
						return [
							Checkbox::make('integer')
								->label('Use only integer numbers'),
							Input::make('filterOptions.min')
								->label('Min value in filter'),
							Input::make('filterOptions.max')
								->label('Max value in filter'),
						];
					},
				],

				Text::class => [
					'label' => 'Full text',
				],

				Keyword::class => [
					'label' => 'Term',
				],

				TermRelation::class => [
					'label' => 'Category',
				],
			];
		}

		return static::$resolved;
	}

	/**
	 * Mapped collection of available attributes types.
	 * Attribute class as key, label as value.
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function map()
	{
		return collect($this->available())->mapWithKeys(function ($options, $class) {
			return [$class => Arr::get($options, 'label', Str::words(class_basename($class)))];
		});
	}

	/**
	 * Generate dropdown options for available attribute types.
	 * Attribute type class used as option value.
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function selectOptions()
	{
		return collect($this->available())
			->map(function ($options, $class) {
				return [
					'value' => $class,
					'label' => Arr::get($options, 'label', Str::words(class_basename($class))),
				];
			})
			->values();
	}

	/**
	 * Get list for form fields for specific attribute type.
	 * Used in configuration form.
	 *
	 * @param string $type Attribute type class
	 *
	 * @return array
	 */
	public function formFor($type)
	{
		$fields = value(Arr::get($this->available(), $type . '.form', []));

		foreach ($fields as $field) {
			$field->attribute = 'options.' . $field->attribute;
		}

		return $fields;
	}

	/**
	 * Generate attribute definitions from saved attributes in database for aircraft entity.
	 * Used in attribute definitions.
	 *
	 * @return array
	 */
	public function generate()
	{
		$attributes = Cache::rememberForever($this->cacheKey(), function () {
		    try {
                return ListingAttribute::orderBy('order', 'asc')->get()->toArray();
            } catch (\Exception $e) {
                return null;
            }
		});

		return collect($attributes)
			->map(function ($attribute) {
				$class = $attribute['type'];
				$instance = $class::make($attribute['name']);

				if (is_array($attribute['options'])) {
                    foreach ($attribute['options'] as $option => $value) {
                        $instance->$option($value);
                    }
                }

				return $instance;
			})
			->all();
	}

	/**
	 * Delete attribute values from database for given attribute
	 *
	 * @param string $attribute
	 */
	public function deleteValues($attribute)
	{
		/** @var \Stylemix\Listing\Attribute\Base $attribute */
		$attribute = Listing::getAttributeDefinitions()->get($attribute);

		if (!$attribute) {
			return;
		}

		$keys = [$attribute->name, $attribute->fillableName];
		ListingData::query()->whereIn('name', $keys)->delete();
	}

	/**
	 * Flush cached attribute definitions.
	 * Used when something is changed in database of attributes.
	 */
	public function flush()
	{
		Cache::forget($this->cacheKey());
	}

	protected function cacheKey()
	{
		return 'listing-attributes';
	}
}
