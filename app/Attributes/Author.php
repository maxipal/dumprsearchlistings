<?php

namespace App\Attributes;

use Stylemix\Listing\Attribute\Relation;

class Author extends Relation
{

	public function __construct(string $name)
	{
		parent::__construct($name, 'user');

		$this->otherKey = 'import_id';

		$this->mapProperties([
			'id',
			'import_id',
			'name',
			'phone',
			'avatar',
			'is_dealer',
			'link',
			'logo',
			'rating',
		]);
	}

}
