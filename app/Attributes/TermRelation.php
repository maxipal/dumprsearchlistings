<?php

namespace App\Attributes;

use App\Fields\TermRelationField;
use App\Term;
use Illuminate\Support\Arr;
use Stylemix\Listing\Attribute\Relation;

/**
 * @property $taxonomy
 */
class TermRelation extends Relation
{

	public function __construct(string $name, $taxonomy = null)
	{
		$this->taxonomy = $taxonomy ?? $name;

		parent::__construct($name, 'term');

		$this
			->aggregationSize(999)
			->where('taxonomy', $this->taxonomy)
			->mapProperties(['id', 'title', 'slug', 'parent_id']);
	}

	/**
	 * @inheritdoc
	 */
	public function applyFilter($criteria, $filter)
	{
		$criteria = Arr::wrap($criteria);

		if (is_numeric(Arr::first($criteria))) {
			parent::applyFilter($criteria, $filter);
			return;
		}

		$filter->put($this->name, [
			'nested' => [
				'path' => $this->name,
				'query' => ['terms' => [$this->name . '.slug' => $criteria]]
			]
		]);
	}

	public function applyIndexData($data, $model)
	{
		$ids = Arr::wrap($data->get($this->fillableName));
		// Append parent ids for found terms by incoming ids
		$ids = array_merge($ids, Term::findMany($ids)->pluck('parent_id')->filter()->all());
		$data->put($this->fillableName, array_map('intval', array_unique($ids)));

		// Backup multiple flag if will change
		$originalMultiple = $this->multiple;

		// For singular attribute but with multiple ids
		// mark as multiple to append multiple terms data
		if (count($ids) > 1 && !$this->multiple) {
			$this->multiple = true;
		}

		$indexData = parent::applyIndexData($data, $model);

		$this->multiple = $originalMultiple;

		return $indexData;
	}

	/**
	 * @inheritdoc
	 */
	public function applyHydratingIndexData($data, $model)
	{
		// Some terms has parents and ES indexed data with parents too and value could be array
		// If this is not multiple attribute it should take the first item from array
		foreach ([$this->name, $this->fillableName] as $field) {
			if (!$this->multiple && is_array($value = $data->get($field)) && !Arr::isAssoc($value)) {
				$data->put($field, reset($value));
			}
		}
	}

	/**
	 * @inheritdoc
	 */
	public function formField()
	{
		$depends = Arr::get(Term::dependenceSchema(), $this->taxonomy, []);

		// Since dependence map consist of attribute names
		// we should convert to field names for form
		$depends = array_map(function ($item) {
			return $item . '_id';
		}, $depends);

		return TermRelationField::make($this->fillableName)
			->label($this->label)
			->multiple($this->multiple)
			->required($this->required)
			->rules('integer')
			->taxonomy($this->taxonomy)
			->depends($depends)
			->icon($this->icon)
			->attributeInstance($this);
	}

	/**
	 * @inheritdoc
	 */
	public function shouldTriggerRelatedUpdate ($owner, $related)
	{
		if ($this->taxonomy !== $related->taxonomy) {
			return false;
		}
	}
}
