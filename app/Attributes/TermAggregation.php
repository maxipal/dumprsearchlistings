<?php

namespace App\Attributes;

use App\Term;
use Illuminate\Support\Arr;
use Stylemix\Listing\Attribute\Base;
use Stylemix\Listing\Attribute\Filterable;
use Stylemix\Listing\Attribute\Relation;

class TermAggregation extends Base implements Filterable
{
	public function __construct(string $name)
	{
		parent::__construct($name);
	}

	/**
	 * @inheritdoc
	 */
	public function elasticMapping($mapping)
	{
		$mapping[$this->name] = [
			'type' => 'nested',
			'properties' => [
				'id' => ['type' => 'integer'],
				'title' => ['type' => 'text'],
				'slug' => ['type' => 'keyword'],
				'order' => ['type' => 'integer'],
				'count' => ['type' => 'integer'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function applyFilter($criteria, $filter)
	{
		$criteria = Arr::wrap($criteria);

		$filter->put($this->name, [
			'nested' => [
				'path' => $this->name,
				'query' => ['terms' => [$this->name . '.slug' => $criteria]]
			]
		]);
	}
}
