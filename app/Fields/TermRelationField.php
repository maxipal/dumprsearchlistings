<?php

namespace App\Fields;

use App\Term;
use Stylemix\Listing\Fields\RelationField;

/**
 * @property string $taxonomy
 * @property boolean $ajax
 */
class TermRelationField extends RelationField
{

	public $component = 'term-relation-field';

	public function toArray()
	{
		if (empty($this->depends) && !$this->ajax) {
			$this->options = Term::search()
				->where('taxonomy', $this->taxonomy)
				->sort(['order' => 'asc','title' => 'asc'])
				->setPerPage(999)
				->get()
				->map(function (Term $model) {
					return (object) $model->toOption();
				});
		}
		else {
			$this->source = [
				'url' => 'terms',
				'params' => [
					'where[taxonomy]' => $this->taxonomy,
					'sort[title]' => 'asc',
					'context' => 'options',
					'primary_key' => 'id',
					'per_page' => 999,
				],
			];
		}

		return parent::toArray();
	}
}
