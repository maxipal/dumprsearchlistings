<?php

namespace App\Fields;

use Illuminate\Http\Request;
use Stylemix\Base\Fields\Base;

class LocationField extends Base
{

	public $component = 'location-field';

	public function __construct($attribute)
	{
		parent::__construct($attribute);

		$this->value = [
			'address' => '',
			'coordinates' => '',
		];
	}

	/**
	 * @inheritdoc
	 */
	protected function resolveAttribute($resource, $attribute)
	{
		return [
			'address' => $resource->location,
			'coordinates' => $resource->coordinates,
		];
	}

	/**
	 * @inheritdoc
	 */
	protected function fillAttributeFromRequest(Request $request, $requestAttribute, $model, $attribute)
	{
		$requestAttribute = $requestAttribute ?: $attribute;

		if ($request->exists($requestAttribute)) {
			$location            = $request[$requestAttribute];
			$model->{$attribute} = $location['address'];
			$model->coordinates  = $location['coordinates'];
		}
	}

}
