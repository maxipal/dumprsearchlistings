<?php

namespace App\Fields;

use Stylemix\Base\Fields\Base;

class EditorField extends Base
{

	public $component = 'editor-field';
}
