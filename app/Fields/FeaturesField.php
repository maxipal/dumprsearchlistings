<?php

namespace App\Fields;

use App\Term;
use Stylemix\Listing\Fields\RelationField;

class FeaturesField extends RelationField
{

	public $component = 'features-field';

	public function toArray()
	{
		$options = Term::search()
			->where('taxonomy', 'features')
			->sort(['order' => 'asc'])
			->setPerPage(9999)
			->get()
			->map(function ($term) {
				return (object) [
					'value' => $term->id,
					'label' => $term->title,
					'groups' => collect($term->depends)->all(),
				];
			})
			->where('groups', '!=', null)
			->values()
			->all();

		return array_merge(parent::toArray(), compact('options'));
	}

}
