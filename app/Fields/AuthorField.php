<?php

namespace App\Fields;

use App\User;
use Stylemix\Listing\Fields\RelationField;

class AuthorField extends RelationField
{

	public function resolve($resource, $attribute = null)
	{
		$otherKey = $this->attributeInstance->getOtherKey();

		if ($resource->author_id) {
			$user = User::search()
				->where($otherKey, $resource->author_id)
				->get()
				->first();

			if ($user) {
				$this->options = [
					(object) $user->toOption($otherKey)
				];
			}
		}

		parent::resolve($resource, $attribute);
	}

	public function toArray()
	{
		$this->source = [
			'url' => 'users',
			'params' => [
				'context' => 'options',
				'primary_key' => 'import_id',
			],
		];

		return parent::toArray();
	}

}
