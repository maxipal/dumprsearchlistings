<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Stylemix\Listing\EntityData;

class TermData extends EntityData
{

	protected $table = 'term_data';

	public function entity(): BelongsTo
	{
		return $this->belongsTo(Term::class, 'entity_id');
	}
}
