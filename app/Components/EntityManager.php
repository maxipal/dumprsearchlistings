<?php

namespace App\Components;

use Illuminate\Support\Facades\DB;

class EntityManager extends \Stylemix\Listing\EntityManager
{

    /**
     * Get overall status for entities
     *
     * @return array
     */
    public function status()
    {
        $status = [];

        foreach (array_keys($this->getBindings()) as $entity) {
            $status[$entity] = \Settings::get('index-status.' . $entity, [
                'remap' => false,
                'indexing' => false,
            ]);
            $model = $this->make($entity);
            $status[$entity]['indexed'] = DB::table($model->getTable())->whereNotNull('indexed_at')->count();
            $status[$entity]['unindexed'] = DB::table($model->getTable())->whereNull('indexed_at')->count();
        }

        return $status;
    }

    /**
     * Set flag that remapping required for entity
     *
     * @param string $entity
     * @param bool $value
     *
     * @return boolean Current status
     */
    public function remapStatus($entity, $value = null)
    {
        if (!is_null($value)) {
            \Settings::set("index-status.{$entity}.remap", $value);
            \Settings::save();
        }

        return \Settings::get("index-status.{$entity}.remap", false);
    }

    /**
     * Set flag that indexing is performed for the entity
     *
     * @param string $entity
     * @param bool $value
     *
     * @return boolean Current status
     */
    public function indexingStatus($entity, $value = null)
    {
        if (!is_null($value)) {
            \Settings::set("index-status.{$entity}.indexing", $value);
            \Settings::save();
        }

        return \Settings::get("index-status.{$entity}.indexing", false);
    }

}
