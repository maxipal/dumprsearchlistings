<?php

namespace App\Components;

use App\Listing;
use App\Term;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Stylemix\Settings\Facades\Settings;

class Seo
{

	/**
	 * @var \Illuminate\Http\Request
	 */
	protected $request;
	protected $filters;
	protected $settings;
	protected $seoPanels;
	protected $seoTypes;
	protected $currentSeoType;


	public function __construct(Request $request)
	{
		$this->request = $request;
		$this->filters = $this->request->filter;
		// title, heading, metadesc, pagedesc
		$this->settings = \Settings::get("seo", false);
		$this->seoPanels = [
			"no_filters",
			"location",
			"condition",
			"con_loc",
			"make_model_con",
			"make_model_loc_con",
			"single_private",
			"single_dealer"
		];
		$this->currentSeoType = reset($this->seoPanels);
		// taxonomy => seoType
		$this->seoTypes = [
			'make' => '%make%',
			'serie' => '%model%',
			'condition' => '%condition%',
			'countries' => '%parent_location%',
			'towns' => '%child_location%',
			'site_name' => '%site_name%',
			'years' => '%year_model%',
			'mileage' => '%mileage%',
			'fuel' => '%fuel%',
			'properties' => '%properties%',
			'item_description' => '%item_description%',
			'body' => '%body%',
			'engine' => '%engine%',
			'transmission' => '%transmission%',
			'exterior-color_id' => '%exterior_color%',
			'interior-color_id' => '%interior_color%',
			'company_name' => '%company_name%',
			'make_id' => '%make%',
			'serie_id' => '%model%',
			'price' => '%price%',
			'condition_id' => '%condition%',
			'countries_id' => '%parent_location%',
			'towns_id' => '%child_location%',
			'content' => '%item_description%'
		];
		$this->setSEOTemplate();
		if(!empty($this->settings[$this->currentSeoType]))
			$this->settings = $this->settings[$this->currentSeoType];
	}


	public function singleSeo(Listing $listing)
	{
		$terms = [];

		foreach ($this->seoTypes as $key => $type){
			if(!empty($listing[$key])){
				if(is_array($listing[$key])){
					$terms[$key] = Term::query()
						->where('id', $listing[$key]['id'])
						->get()
						->first();
				}else{
					$terms[$key] = $listing[$key];
				}
			}
		}
		$is_dealer = false;
		$author = User::where('import_id', $listing->author_id)->first();
		if(!empty($author->name)) $terms['company_name'] = $author->name;
		if($author && $author->is_dealer) $is_dealer = true;
		if($is_dealer){
			$this->currentSeoType = 'single_dealer';
		}else{
			$this->currentSeoType = 'single_private';
		}
		$this->settings = \Settings::get("seo", false);
		if(!empty($this->settings[$this->currentSeoType]))
			$this->settings = $this->settings[$this->currentSeoType];

		return [
			'title' => $this->seoTitle($terms),
			'description' => $this->seoDescription($terms),
			'pageTitle' => $this->seoPageTitle($terms),
			'pageDescription' => $this->seoPageDescription($terms)
		];
	}


	private function setSEOTemplate(){
		$filter = $this->filters;
		if(empty($filter) || !count($filter)) return;
		if(
			( array_key_exists('make', $filter) !== false ||
			array_key_exists('serie', $filter) !== false ) &&
			array_key_exists('condition', $filter) !== false &&
			array_key_exists('countries', $filter) !== false
		){
			$this->currentSeoType = 'make_model_loc_con';
		}elseif(
			array_key_exists('make', $filter) !== false ||
			array_key_exists('serie', $filter) !== false
		){
			$this->currentSeoType = 'make_model_con';
		}elseif(
			( array_key_exists('make', $filter) !== false ||
			array_key_exists('serie', $filter) !== false ) &&
			array_key_exists('condition', $filter) !== false
		){
			$this->currentSeoType = 'make_model_con';
		}elseif(
			array_key_exists('condition', $filter) !== false &&
			array_key_exists('countries', $filter) !== false
		){
			$this->currentSeoType = 'con_loc';
		}elseif(array_key_exists('condition', $filter) !== false){
			$this->currentSeoType = 'condition';
		}elseif(array_key_exists('countries', $filter) !== false){
			$this->currentSeoType = 'location';
		}
	}


	public function matchInventoryPath($link)
	{
		$link    = trim($link, '/');
		$request = [];
		$rules = $this->getInventoryMatchRules();

		foreach ($rules as $rule) {
			if (preg_match("#^{$rule->pattern}$#", $link, $matches)) {

				foreach (array_slice($matches, 1) as $i => $slug) {

					if (app()->runningUnitTests()) {
						$term = Term::query()
							->whereIn('taxonomy', $rule->taxes)
							->where('slug', $slug)
							->get()
							->first();
					}
					else {

						$term = Term::search()
							->where('taxonomy', $rule->taxes[$i])
							->where('slug', $slug)
							->get()
							->first();

					}

					if ($term) {
						$request[] = $term;
					}
				}

				if (count($request) == count($rule->taxes)) {
					break;
				}
			}
		}

		return empty($request) ? null : (new Term)->newCollection($request);
	}

	public function inventorySeo()
	{
		$terms = $this->getRequestedTerms();
		$filter_terms = null;
		if(is_array($this->filters)){
			$filter_terms = $this->getRequestedTerms(array_keys($this->filters));
		}

		return [
			'title' => $this->seoTitle($filter_terms),
			'description' => $this->seoDescription($filter_terms),
			'pageTitle' => $this->seoPageTitle($filter_terms),
			'pageDescription' => $this->seoPageDescription($filter_terms),
			'link' => $this->inventoryLink($terms),
		];
	}

	private function seoCreateTemplate($template, $terms){
		// TODO: Add site name to laravel settings
		$template = str_replace('%site_name%', 'damprsearch.com', $template);
		if(!$terms) return $template;

		if($terms){
			foreach ($terms as $key => $term){
				if(is_object($term)){
					$tax = $term->taxonomy;
					if($tax && !empty($this->seoTypes[$tax])){
						$template = str_replace($this->seoTypes[$tax], $term->title, $template);
					}
				}else{
					if(!is_array($term))
						$template = str_replace($this->seoTypes[$key], $term, $template);
				}
			}
		}
		// Remove unapply shortcodes in template
		$template = preg_replace('/%[a-zA-Z0-9_]+%/', '', $template);
		return $template;
	}


	/**
	 * @param \App\Term[]|\Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return string
	 */
	public function seoPageTitle($terms) {
		return $this->seoCreateTemplate($this->settings['heading'], $terms);
	}

	/**
	 * @param \App\Term[]|\Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return string
	 */
	public function seoPageDescription($terms) {
		$desc = '';
		if(!empty($this->settings['pagedesc'])) $desc = $this->settings['pagedesc'];
		return $this->seoCreateTemplate($desc, $terms);
	}


	/**
	 * @param \App\Term[]|\Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return string
	 */
	public function seoDescription($terms) {
		if(!empty($this->settings['metadesc']))
			return $this->seoCreateTemplate($this->settings['metadesc'], $terms);
		else
			return null;
	}


	/**
	 * @param \App\Term[]|\Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return string
	 */
	public function seoTitle($terms) {
		if(!empty($this->settings['title'])){
			return $this->seoCreateTemplate($this->settings['title'], $terms);
		}else{
			return 'Vehicles for sale';
		}
	}

	/**
	 * @param \App\Term[]|\Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return string
	 */
	public function inventoryLink($terms)
	{
		$link  = '';
		$query = $this->request->query->all();

		if ($terms->isEmpty()) {
			return $this->getQueryString($query);
		}

		$terms = $terms->keyBy('taxonomy');

		foreach ($this->inventoryLinkRules() as $pattern) {
			if (!preg_match_all('/\{([^\}]+)\}/', $pattern, $matches)) {
				continue;
			}

			foreach ($matches[1] as $tax) {
				if (!isset($terms[$tax])) {
					continue 2;
				}
			}

			if(count($terms) !== count(explode('/', $pattern))) continue;

			$link = '/' . trim($pattern, '/') . '/';
			foreach ($matches[0] as $i => $replace) {
				$slug = $terms[$matches[1][$i]]->slug;
				$link = str_replace($replace, $slug, $link);
			}

			foreach (['filter', 'where'] as $criteria) {
				$query[$criteria] = Arr::except(Arr::get($query, $criteria, []), $matches[1]);
			}

			$query = array_filter($query);
			//break;
		}

		return $link . $this->getQueryString($query);
	}

	/**
	 * @param \App\Term[]|\Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return string
	 */
	public function inventoryLinkTerm($term)
	{
		$link  = '';
		$query = $this->request->query->all();

		if (!$term) {
			return $this->getQueryString($query);
		}
		foreach ($this->inventoryLinkRules() as $pattern) {
			if (!preg_match_all('/\{([^\}]+)\}/', $pattern, $matches)) {
				continue;
			}
			foreach ($matches[1] as $tax) {
				if ($term->taxonomy !== $tax) {
					continue 2;
				}
			}
			$link = '/' . trim($pattern, '/') . '/';
			foreach ($matches[0] as $i => $replace) {
				$slug = $term->slug;
				$link = str_replace($replace, $slug, $link);
			}
			foreach (['filter', 'where'] as $criteria) {
				$query[$criteria] = Arr::except(Arr::get($query, $criteria, []), $matches[1]);
			}
			$query = array_filter($query);
		}
		return $link . $this->getQueryString($query);
	}

	/**
	 * @param mixed $source
	 *
	 * @return array
	 */
	public function breadcrumbs($source)
	{
		if ($source instanceof Collection) {
			$terms = $source->keyBy('taxonomy');
		}
		elseif ($source instanceof Listing) {
			$terms = Term::search()
				->where('id', [$source->types_id, $source->make_id, $source->serie_id, $source->countries_id, $source->towns_id])
				->get()
				->keyBy('taxonomy');
		}
		else {
			return [];
		}

		$breadcrumb = [];
		$keys = ['towns', 'countries', 'serie', 'make', 'body' ];

		foreach ($keys as $key) {
			$term = Arr::get($terms, $key);
			if (!$term) {
				continue;
			}

			$level = [
				'title' => $term['title'],
				'link' => $this->inventoryLink($terms),
			];

			$terms = $terms->diffKeys([$key => 1]);

			$breadcrumb[] = $level;
		}

		return $breadcrumb;
	}

	protected function hasRequestAttribute(Request $request, $key)
	{
		return data_get($request, 'filter.' . $key) || data_get($request, 'where.' . $key);
	}

	/**
	 * @return \App\Term[]|\Illuminate\Database\Eloquent\Collection
	 */
	protected function getRequestedTerms($taxes = [])
	{
		$request = [];
		if(!count($taxes)) $taxes = $this->getTaxonomies();

		foreach ($taxes as $key) {
			if ($terms = data_get($this->request->where, $key)) {
				if (is_array($terms) && count($terms) > 1) {
					continue;
				}

				$request[$key] = is_array($terms) ? Arr::first($terms) : $terms;
			}

			if ($terms = data_get($this->request->filter, $key)) {
				if (is_array($terms) && count($terms) > 1) {
					continue;
				}

				$request[$key] = is_array($terms) ? Arr::first($terms) : $terms;
			}
		}

		if (empty($request)) {
			return collect();
		}

		$query = Term::query();
		foreach ($request as $taxonomy => $id) {
			$query->orWhere(function ($query) use ($taxonomy, $id) {
				if(is_numeric($id))	return $query->where('taxonomy', $taxonomy)->where('id', $id);
				else return $query->where('taxonomy', $taxonomy)->where('slug', $id);
			});
		}

		return $query->get();
	}

	protected function inventoryLinkRules()
	{
		return array(
			'{body}',
			'{body}/{make}',
			'{body}/{make}/{serie}',
			'{body}/{make}/{serie}/{countries}',
			'{body}/{make}/{serie}/{countries}/{towns}',
			'{body}/{countries}',
			'{body}/{countries}/{towns}',
			'{body}/{make}/{countries}',
			'{body}/{make}/{countries}/{towns}',
			'{make}',
			'{make}/{serie}',
			'{make}/{countries}',
			'{make}/{serie}/{countries}',
			'{make}/{countries}/{towns}',
			'{make}/{serie}/{countries}/{towns}',
			'{countries}',
			'{countries}/{towns}',
		);
	}

	protected function getTaxonomies()
	{
		return ['body', 'make', 'serie', 'countries', 'towns'];
	}

	protected function getQueryString($query): string
	{
		$query = Arr::only($query, [
			'filter',
			'sort',
			'sort_order',
		]);

		return ($query ? '?' . http_build_query($query) : '');
	}

	protected function getInventoryMatchRules()
	{
		$rules = array();

		foreach ($this->inventoryLinkRules() as $pattern) {
			if (!preg_match_all('/\{([^\}]+)\}/', $pattern, $matches)) {
				continue;
			}

			$taxes = array();

			foreach ($matches[0] as $i => $slug) {
				$tax = $matches[1][$i];
				if (in_array($tax, $this->getTaxonomies())) {
					$pattern = str_replace($slug, '([^/]+)', $pattern);
					$taxes[] = $tax;
				}
			}

			$rules[] = (object) compact('pattern', 'taxes');
		}

		return $rules;
	}

	public static function getSeoUrlArray($listing){
		$tax_index = array(
            'body'      => 'body_id',
            'make'      => 'make_id',
            'serie'     => 'serie_id',
            'countries' => 'countries_id',
            'towns'     => 'towns_id'
        );
        $links = $link_parts = [];
        foreach ($tax_index as $tax => $tax_id) {
            $term = Term::where('taxonomy', $tax)->where('id', $listing->{$tax_id})->first();
            if(!empty($term->slug)){
                $links[$tax] = $term->slug.'/';
            }
        }

        if(!empty($links['body'])) $link_parts[] = '/' . $links['body'];
        if(!empty($links['body']) && !empty($links['make'])) $link_parts[] = '/' . $links['body'].$links['make'];
        if(!empty($links['make'])) $link_parts[] = '/' . $links['make'];
        if(!empty($links['make']) && !empty($links['serie'])) $link_parts[] = '/' . $links['make'].$links['serie'];
        if(!empty($links['make']) && !empty($links['serie']) && !empty($links['body'])) $link_parts[] = '/' . $links['body'].$links['make'].$links['serie'];
        if(!empty($links['countries']) && !empty($links['towns'])){
            $link_parts[] = '/' . $links['countries'];
            $link_parts[] = '/' . $links['countries'].$links['towns'];
            if(!empty($links['make']) && !empty($links['serie']) && !empty($links['body']))
            	$link_parts[] = '/' . $links['body'].$links['make'].$links['serie'].$links['countries'];
            if(!empty($links['make']) && !empty($links['serie']) && !empty($links['body']))
            	$link_parts[] ='/' .  $links['body'].$links['make'].$links['serie'].$links['countries'].$links['towns'];
        }

        return $link_parts;
	}
}
