<?php

namespace App\Components;

use App\Listing;
use App\Attributes\TermRelation;
use App\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Stylemix\Listing\Elastic\Builder;

class Filter
{

	/**
	 * @var Request
	 */
	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	/**
	 * Generate filter form configuration.
	 * Optionally use query results for filter options
	 *
	 * @param \Stylemix\Listing\Elastic\Collection $results
	 *
	 * @return array|\Illuminate\Support\Collection
	 */
	public function form($results = null)
	{
		$filters = collect();
		$inFilter = $this->getFilterAttributes();


		foreach ($inFilter as $attribute) {
			$filter = [
				'attribute' => $attribute->toArray(),
				'key' => $attribute->name,
				'multiple' => $attribute->multiple,
				'type' => $this->filterType($attribute),
				'label' => $attribute->label,
				'separatePanel' => $attribute->separateFilterPanel || $attribute->useAsCheckboxes
			];

			switch ($filter['type']) {
				case 'slider':
				case 'ranges':
					$filter['options'] = $this->getSliderFilterOptions($attribute);
					break;
				case 'select':
					$filter['options'] = $this->getSelectFilterOptions($attribute, $results);
					$filter['hierarchical'] = (boolean)$attribute->hierarchical;
					break;
				case 'location' :
					$filter['search_radius'] = true;
					if ($this->request->context === 'frontpage') {
						$filter['search_radius'] = false;
					}
					break;
			}

			$filters[] = $filter;
		}

		return $filters;
	}

	public function badges()
	{
		$badges = collect();
		$requested = $this->request->get('filter', []);
		$attributes = Listing::getAttributeDefinitions()
			->where('useInFilter', true)
			->whereIn('name', array_keys($requested));

		// Collect attributes names responsible for term relations
		$termKeys = $attributes->whereInstanceOf(TermRelation::class)->pluck('name');
		// Take all term relation criteria from request filter by keys above
		$termIds = collect($requested)->only($termKeys)->flatten()->all();
		// Eager load Terms by requested filter
		$terms = Term::whereIn('id', $termIds)
			->orWhereIn('slug', $termIds)
			->get();

		foreach ($attributes as $attribute) {
			$badge = [
				'slug' => $attribute->name,
				'name' => $attribute->label,
				'type' => $this->filterType($attribute),
				'value' => $this->getBadgeValue(Arr::wrap($this->request->filter[$attribute->name]), $attribute, $terms),
			];

			$badges[] = $badge;
		}

		return $badges;
	}

	public function buildQuery(Builder $builder)
	{
		$request = $this->request;

		if ($request->filters) {
			$aggregations = $this->getFilterAttributes()->map(function () {
				return true;
			})->all();
			$builder->aggregations($aggregations);
		}

		if ($request->featured == 'only') {
			$builder->where('is_special', true);
		}

		if ($request->s) {
			$builder->search($request->s);
		}

		$sort = Arr::wrap($request->get('sort', []));
		if ($request->stm_lng && $request->stm_lat) {
			$latlng = $request->stm_lat . ',' . $request->stm_lng;

 			// Sort by nearest to farthest
 			$sort['coordinates'] = $latlng . '|asc';

 			if ($distance = $request->get('max_search_radius')) {
 				// Filter by distance from requested point
 				$builder->where('coordinates', [
 					'latlng' => $latlng,
 					'distance' => $distance . 'm',
 				]);
 			}
		}

		switch ($request->sort_order) {
			case "price_low":
				$sort['price'] = 'asc';
				break;
			case "price_high":
				$sort['price'] = 'desc';
				break;
			case "mileage_low":
				$sort['mileage'] = 'asc';
				break;
			case "mileage_high":
				$sort['mileage'] = 'desc';
				break;
			case "date_low":
				$sort['updated_at'] = 'asc';
				break;
			case "date_high":
				$sort['updated_at'] = 'desc';
				break;
			case "random":
				$builder->random();
				break;
//		case "popular":
//			$sort['counter_view'] = 'desc';
//			break;
			default:
				$sort['updated_at'] = 'desc';
		}

		$builder->sort($sort);

		if ($request->related) {
			$terms = Term::query()
				->whereIn('import_id', Arr::flatten(Arr::wrap($request->related)))
				->get()
				->groupBy('taxonomy');

			foreach (Arr::only($request->related, $terms->keys()->all()) as $tax => $ids) {
				$builder->where($tax, $terms[$tax]->pluck('id')->all());
			}
		}
	}

	/**
	 * @param Request $request
	 *
	 * @return $this
	 */
	public function setRequest(Request $request)
	{
		$this->request = $request;

		return $this;
	}

	/**
	 * Get attributes used for filter.
	 * Depends on request parameters `context` and `filters`.
	 *
	 * @return \Stylemix\Listing\Attribute\Base[]|\Stylemix\Listing\AttributeCollection
	 */
	public function getFilterAttributes()
	{
		$filter = [];
		$taxes = \Settings::get('listing-taxes');
		if($taxes){
			foreach ($taxes as $tax_name => $tax) {
				$filter[] = array(
					'name' => $tax_name
				);
			}
		}

		$filter[] = [
			'name' => 'price',
			'label' => 'Choose the price'
		];
		$filter[] = [
			'name' => 'coordinates',
			'label' => 'Enter a location',
		];
		$filter[] = [
			'name' => 'features',
			'label' => 'Other features',
		];

		$inFilter = collect($filter)->map(function ($config) {

			/** @var \Stylemix\Listing\Attribute\Base $attribute */
			$attribute = Listing::getAttributeDefinitions()->implementsFiltering()->get($config['name']);

			if (!$attribute) {
				return null;
			}

			// Prevent overriding original attribute options
			$attribute = clone $attribute;

			foreach ($config as $key => $value) {
				$attribute[$key] = $value;
			}

			return $attribute;
		})->filter()->keyBy('name');

		if ($this->request->context == 'frontpage') {
			$inFilter = $inFilter->whereIn('name', ['condition', 'make', 'serie', 'coordinates']);
		}

		if ($this->request->filters !== 'all' && count(array_filter($only = explode(',', $this->request->filters)))) {
			$inFilter = $inFilter->whereIn('name', $only);
		}

		return $inFilter;
	}

	protected function filterType($attribute)
	{
		switch ($attribute->type) {
			case 'term_relation':
				if ($attribute->useAsCheckboxes && $this->request->context != 'map') {
					return 'checkboxes';
				} else {
					return 'select';
				}
			case 'numeric':
			case 'price_with_sale':
				return $this->request && $this->request->context == 'frontpage' ? 'ranges' : 'slider';
			case 'location':
				return 'location';
		}

		return null;
	}

	/**
	 * @param \Stylemix\Listing\Attribute\Base $attribute
	 * @param \Stylemix\Listing\Elastic\Collection $listing
	 *
	 * @return array
	 */
	protected function getSelectFilterOptions($attribute, $listing = null)
	{
		$options = [];

		if (!$attribute->multiple) {
			$options[] = [
				'label' => $attribute->label,
				'value' => '',
				'selected' => false,
				'disabled' => false,
				'order' => 0
			];
		}
		if(!empty($attribute->get('placeholder1'))){
			$options[0]['label'] = $attribute->get('placeholder1');
		}
		if (!$listing) {
			return $options;
		}

		if (($aggregations = $listing->getAggregations()) && $aggregations->has($attribute->name)) {
			$rows = $aggregations[$attribute->name];

			if($attribute->name == 'make' || $attribute->name == 'serie'){
				$per_page = 9999;
				if($attribute->name == 'serie' && !empty($this->request->filter['make'])){
					$full_opts = Term::search()
						->where('taxonomy', $attribute->name)
						->where('depends',$this->request->filter['make'])
						->setPerPage($per_page)
						->all();
				}else{
					$full_opts = Term::search()
						->where('taxonomy', $attribute->name)
						->setPerPage($per_page)
						->all();
				}

				foreach ($full_opts as $full_opt) {
					if( $key = in_array($full_opt->id, array_column($rows, 'id')) ){
						$key = array_search($full_opt->id, array_column($rows, 'id'));
						$rows[$key]['order'] = $full_opt->order;
					}else{
						$rows[] = [
							'id' => $full_opt->id,
							'title' => $full_opt->title,
							'slug' => $full_opt->slug,
							'count' => 0,
							'order' => $full_opt->order
						];
					}
				}
			}else{
//				$full_opts = Term::search()
//					->setPerPage(9999)
//					->where('taxonomy', $attribute->name)
//					->where('depends',$this->request->filter['make'])
//					->all();
//				foreach ($full_opts as $full_opt) {
//					if( $key = in_array($full_opt->id, array_column($rows, 'id')) ){
//						$key = array_search($full_opt->id, array_column($rows, 'id'));
//						$rows[$key]['order'] = $full_opt->order;
//					}else {
//						$rows[] = [
//							'id' => $full_opt->id,
//							'title' => $full_opt->title,
//							'slug' => $full_opt->slug,
//							'count' => 0,
//							'order' => $full_opt->order
//						];
//					}
//				}
			}
			if ($attribute->type == 'term_relation') {
				// Order by name first
				$rows = collect($rows)->sortBy('title');

				// Ordered list of terms by hierarchy
				// - Parent 1
				// - - Child 1
				// - - Child 1
				// - Parent 2
				// - - Child 2
				// - - Child 2
				$ordered = [];
				$_rows = $rows->groupBy('parent_id');

				foreach ($_rows->get('', []) as $row) {
					$ordered[] = $row;
					$ordered = array_merge($ordered, $_rows->has($row['id']) ? $_rows[$row['id']]->all() : []);
				}
				$rows = $ordered;
			}

			foreach ($rows as $row) {
				$option = [
					'label' => $row['title'],
					'value' => $row['id'],
					'selected' => false,
					'disabled' => false,
					'count' => $row['count'],
					//'order' => $row['order']
				];

				if ($attribute->type == 'term_relation') {
					$option['parent_id'] = isset($row['parent_id']) ? $row['parent_id'] : null;
				}

				$options[] = $option;
			}

		}



		return $options;
	}

	function getSliderFilterOptions($attribute)
	{
		$options = (array)$attribute->filterOptions;
		$options += [
			'min' => 0,
			'max' => 0,
			'prefix' => '',
			'ranges' => [],
		];

		if ($attribute->type == 'price_with_sale') {
			$options['prefix'] = config('app.currency');
		}

		$options['ranges'] = collect($options['ranges'])
			->mapWithKeys(function ($value) use ($options) {
				$label = $options['prefix'] . number_format($value, 0, '.', ' ');
				return [$value => $label];
			});

		return $options;
	}

	protected function getBadgeValue($requestedParameter, $attribute, $terms)
	{
		$result = "";
		$filterType = $this->filterType($attribute);
		$attributeType = $attribute->type;

		switch ($filterType) {
			case 'slider':
				$result = $this->getSliderBadgeValue($requestedParameter, $attributeType);
				break;
			case 'select':
				$result = $this->getTermBadgeValue($requestedParameter, $terms);
				break;
			case 'location':
//			$result = $this->getLocationBadgeValue();
				break;
			case 'checkboxes':
				$result = $this->getTermBadgeValue($requestedParameter, $terms);
				break;
			default:
				$result = $requestedParameter ?? "";
				break;
		}

		return $result;
	}

	/**
	 * @param mixed $requestedParameter
	 * @param \Illuminate\Database\Eloquent\Collection $terms
	 *
	 * @return array
	 */
	protected function getTermBadgeValue($requestedParameter, $terms)
	{
		$result = [];

		foreach ($requestedParameter as $item) {
			if (is_numeric($item)) {
				$result[] = $terms->pluck('title', 'id')->get($item) ?? "";
			} else {
				$result[] = $terms->pluck('title', 'slug')->get($item) ?? "";
			}
		}

		return join(', ', $result);
	}

	protected function getSliderBadgeValue($param, $attributeType)
	{
		$result = [];
		if (isset($param['gte'])) {
			$result[] = ($attributeType === 'price_with_sale' ? config('app.currency') : '') . $param['gte'];
		}
		if (isset($param['lte'])) {
			$result[] = ($attributeType === 'price_with_sale' ? config('app.currency') : '') . $param['lte'];
		}

		return join(' - ', $result);
	}
}
