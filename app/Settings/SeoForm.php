<?php

namespace App\Settings;

use Stylemix\Base\Fields\Textarea;
use Stylemix\Base\Fields\Input;
use Stylemix\Base\Fields\Editor;
use Stylemix\Base\FormResource;

class SeoForm extends FormResource
{

	/**
	 * @inheritdoc
	 */
	public function fields()
	{
		return [
			Input::make('seo.no_filters.title')
				->label('SEO Title / Page Title')
				->panel('no_filters'),
			Input::make('seo.no_filters.heading')
				->label('Heading 1')
				->panel('no_filters'),
			Textarea::make('seo.no_filters.metadesc')
				->label('Meta Description')
				->panel('no_filters'),
			Editor::make('seo.no_filters.pagedesc')
				->label('Page Description')
				->panel('no_filters'),

			// Location
			Input::make('seo.location.title')
				->label('SEO Title / Page Title')
				->panel('location'),
			Input::make('seo.location.heading')
				->label('Heading 1')
				->panel('location'),
			Textarea::make('seo.location.metadesc')
				->label('Meta Description')
				->panel('location'),
			Editor::make('seo.location.pagedesc')
				->label('Page Description')
				->panel('location'),

			// Condition
			Input::make('seo.condition.title')
				->label('SEO Title / Page Title')
				->panel('condition'),
			Input::make('seo.condition.heading')
				->label('Heading 1')
				->panel('condition'),
			Textarea::make('seo.condition.metadesc')
				->label('Meta Description')
				->panel('condition'),
			Editor::make('seo.condition.pagedesc')
				->label('Page Description')
				->panel('condition'),

			// Condition, location
			Input::make('seo.con_loc.title')
				->label('SEO Title / Page Title')
				->panel('con_loc'),
			Input::make('seo.con_loc.heading')
				->label('Heading 1')
				->panel('con_loc'),
			Textarea::make('seo.con_loc.metadesc')
				->label('Meta Description')
				->panel('con_loc'),
			Editor::make('seo.con_loc.pagedesc')
				->label('Page Description')
				->panel('con_loc'),

			// Make model Condition
			Input::make('seo.make_model_con.title')
				->label('SEO Title / Page Title')
				->panel('make_model_con'),
			Input::make('seo.make_model_con.heading')
				->label('Heading 1')
				->panel('make_model_con'),
			Textarea::make('seo.make_model_con.metadesc')
				->label('Meta Description')
				->panel('make_model_con'),
			Editor::make('seo.make_model_con.pagedesc')
				->label('Page Description')
				->panel('make_model_con'),

			// Make model Location Condition
			Input::make('seo.make_model_loc_con.title')
				->label('SEO Title / Page Title')
				->panel('make_model_loc_con'),
			Input::make('seo.make_model_loc_con.heading')
				->label('Heading 1')
				->panel('make_model_loc_con'),
			Textarea::make('seo.make_model_loc_con.metadesc')
				->label('Meta Description')
				->panel('make_model_loc_con'),
			Editor::make('seo.make_model_loc_con.pagedesc')
				->label('Page Description')
				->panel('make_model_loc_con'),


			// Single page belongs to Private seller
			Input::make('seo.single_private.title')
				->label('SEO Title / Page Title')
				->panel('single_private'),
			Input::make('seo.single_private.heading')
				->label('Heading 1')
				->panel('single_private'),
			Textarea::make('seo.single_private.metadesc')
				->label('Meta Description')
				->panel('single_private'),

			// Single page belongs to a Car Dealer
			Input::make('seo.single_dealer.title')
				->label('SEO Title / Page Title')
				->panel('single_dealer'),
			Input::make('seo.single_dealer.heading')
				->label('Heading 1')
				->panel('single_dealer'),
			Textarea::make('seo.single_dealer.metadesc')
				->label('Meta Description')
				->panel('single_dealer'),
		];
	}
}
