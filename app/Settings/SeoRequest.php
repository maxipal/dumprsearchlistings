<?php

namespace App\Settings;


use Illuminate\Support\Facades\Gate;
use Stylemix\Base\FormRequest;

class SeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('listings.create');
    }

	/**
	 * @inheritdoc
	 */
	protected function formResource()
	{
		return SeoForm::make();
	}
}
