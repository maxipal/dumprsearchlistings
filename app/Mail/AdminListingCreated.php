<?php

namespace App\Mail;

use App\Listing;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminListingCreated extends Mailable //implements ShouldQueue
{
	use Queueable, SerializesModels;

	/**
	 * @var \App\Listing
	 */
	public $listing;
	public $template;
	public $user;

	/**
	 * Create a new message instance.
	 *
	 * @param \App\Listing $listing
	 */
	public function __construct(Listing $listing, $mail_template)
	{
		$this->listing = $listing;
		$this->template = $mail_template;
		$this->user = User::search()
			->where('import_id', $this->listing->author_id)
			->first();
	}

	public function htmlWith($html, $data = []){
		foreach ($data as $key => $datum) {
			$html = str_replace("{{ $key }}", $datum, $html);
		}
		return $html;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$params = [
			'car_id' => $this->listing->id,
			'user_id' => $this->user->import_id
		];
		$content = $this->htmlWith(stripslashes($this->template['content']),$params);
		return $this->html($content)
			->subject($this->htmlWith($this->template['subject'], $params));
	}
}
