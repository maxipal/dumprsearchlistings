<?php

namespace App\Mail;

use App\Listing;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DealerAcceptedOffer extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

	/**
	 * @var \App\Listing
	 */
	public $bid;

	/**
	 * Create a new message instance.
	 *
	 * @param \App\Listing $listing
	 */
    public function __construct($bid)
    {
		$this->bid = $bid;
	}

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.offer_accepted')
			->subject('Your offer accepted')
			->with('user', User::search()
				->where('import_id', $this->bid['UID'])
				->first()
			);
    }
}