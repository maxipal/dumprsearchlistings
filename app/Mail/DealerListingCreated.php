<?php

namespace App\Mail;

use App\Listing;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DealerListingCreated extends Mailable //implements ShouldQueue
{
    use Queueable, SerializesModels;

	/**
	 * @var \App\Listing
	 */
	public $listing;
	public $template;

	/**
	 * Create a new message instance.
	 *
	 * @param array $listing
	 */
    public function __construct($listing, $mail_template)
    {
		$this->listing = $listing;
		$this->template = $mail_template;
	}

	public function htmlWith($html, $data = []){
		foreach ($data as $key => $datum) {
			$html = str_replace("{{ $key }}", $datum, $html);
	}
		return $html;
	}


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

		$params = [
			'name' => $this->listing['name'],
			'email' => $this->listing['email'],
			'link' => $this->listing['link'],
			'title' => $this->listing['title']
		];
		$content = $this->htmlWith(stripslashes($this->template['content']),$params);
		return $this->html($content)
			->subject($this->htmlWith($this->template['subject'], $params));


//        return $this->view('emails.auction_created')
//			->subject('Shto një ankand të ri')
//			->from(config('mail.from.address'),config('mail.from.name'))
//			->with($this->listing);
    }
}
