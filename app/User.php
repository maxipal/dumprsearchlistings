<?php

namespace App;

use App\Attributes\Rating;
use App\Attributes\TermAggregation;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Stylemix\Listing\Attribute\Boolean;
use Stylemix\Listing\Attribute\Date;
use Stylemix\Listing\Attribute\Id;
use Stylemix\Listing\Attribute\Keyword;
use Stylemix\Listing\Attribute\Location;
use Stylemix\Listing\Attribute\Numeric;
use Stylemix\Listing\Attribute\Text;
use Stylemix\Listing\Entity;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Entity implements
	AuthenticatableContract,
	AuthorizableContract,
	CanResetPasswordContract,
	JWTSubject
{
	use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable;

	public $dbFields = [
		'id',
		'import_id',
		'name',
		'email',
		'email_verified_at',
		'password',
		'password',
		'remember_token',
		'created_at',
		'updated_at',
	];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	 * Attributes relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function dataAttributes(): HasMany
	{
		return $this->hasMany(UserData::class, 'entity_id');
	}

	public function toOption($otherKey = null)
	{
		return [
			'value' => $this->getAttribute($otherKey ?: $this->getKeyName()),
			'label' => $this->name . ' (' . trim($this->first_name . ' ' . $this->last_name) . ')',
			'image' => $this->logo ?: $this->avatar,
			'extra' => $this->email,
		];
	}

	public function fillCounts()
	{
		$counts = Listing::search()
			->where('author_id', $this->import_id)
			->where('status', 'publish')
			->whereNot('sold', true)
			->aggregate('condition')
			->aggregate('make')
			->get();

		$this->count_listings = $counts->totalHits();
		$this->conditions = $counts->getAggregations()->get('condition', []);
		$this->makes = $counts->getAggregations()->get('make', []);

		return $this;
	}

	/**
	 * Attribute definitions
	 *
	 * @return array
	 */
	protected static function attributeDefinitions(): array
	{
		return [
			Id::make(),
			Text::make('name')->useInSearch(),
			Keyword::make('email')->useInSEarch(),
			Date::make('created_at'),
			Date::make('updated_at'),
			Numeric::make('import_id')->integer(),
			Text::make('first_name')->useInSearch(),
			Text::make('last_name')->useInSearch(),
			Keyword::make('roles')->multiple(),
			Boolean::make('is_dealer'),
			Keyword::make('link'),
			Keyword::make('avatar'),
			Keyword::make('logo'),
			Boolean::make('show_email'),
			Keyword::make('phone'),
			Text::make('address')->useInSearch(),
			Location::make('location'),
			Rating::make('rating'),
			Numeric::make('count_listings')->integer(),
			TermAggregation::make('conditions')->multiple(),
			TermAggregation::make('makes')->multiple(),
			Numeric::make('favorites')->multiple()->integer(),
			Keyword::make('auction_exclude')->multiple(),
		];
	}

	/**
	 * @inheritdoc
	 */
	public function getJWTIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * @inheritdoc
	 */
	public function getJWTCustomClaims()
	{
		return [];
	}
}
