<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WpFlushQueue extends Model
{
	public $timestamps = false;

	protected $table = 'wp_flush_queue';

	public static function pushItem($path)
	{
		return static::query()->insertOrIgnore(compact('path'));
	}

	public static function pushItems($paths)
	{
		return static::query()->insertOrIgnore(array_map(function ($path) {
			return compact('path');
		}, $paths));
	}
}
