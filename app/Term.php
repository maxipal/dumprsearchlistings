<?php

namespace App;

use App\Attributes\TermDepends;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Stylemix\Listing\Attribute\Date;
use Stylemix\Listing\Attribute\Id;
use Stylemix\Listing\Attribute\Image;
use Stylemix\Listing\Attribute\Keyword;
use Stylemix\Listing\Attribute\LongText;
use Stylemix\Listing\Attribute\Numeric;
use Stylemix\Listing\Attribute\Slug;
use Stylemix\Listing\Attribute\Text;
use Stylemix\Listing\Entity;

class Term extends Entity
{

	public $dbFields = [
		'id',
		'taxonomy',
		'title',
		'slug',
		'parent_id',
		'order',
		'level',
		'import_id',
		'created_at',
		'updated_at',
	];

	protected $fillable = ['title'];

	protected $casts = [
		'parent_id' => 'integer',
		'order' => 'integer',
		'level' => 'integer',
	];

    protected $table = 'terms';

	/**
	 * Attributes relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function dataAttributes(): HasMany
	{
		return $this->hasMany(TermData::class, 'entity_id');
	}

	public function toOption()
	{
		return [
			'value' => $this->id,
			'label' => $this->title,
			'parent' => $this->parent_id,
			'order' => $this->order
		];
	}

	public function toCheckbox() {
		return [
			'value' => $this->id,
			'label' => $this->title,
			'image' => $this->image
		];
	}

	public static function taxonomies()
	{
		$taxonomies = Listing::getAttributeDefinitions()
			->where('type', 'term_relation')
			->map(function ($attribute) {
				return [
					'name' => $attribute->taxonomy,
					'label' => $attribute->label
				];
			})
			->values()
			->all();

		$taxonomies[] = [
			'name' => 'feature_group',
			'label' => 'Feature groups'
		];

		return $taxonomies;
	}

	public static function dependenceSchema()
	{
		return [
			'serie' => [
				'make',
			],
			'trim' => [
				'serie',
			],
			'towns' => [
				'countries',
			],
			'features' => [
				'feature_group',
			],
		];
	}

	/**
	 * Attribute definitions
	 *
	 * @return array
	 */
	protected static function attributeDefinitions(): array
	{
		return [
			Id::make(),
			Text::make('title')
				->search(['boost' => 3]),
			Date::make('created_at'),
			Date::make('updated_at'),
			Keyword::make('taxonomy'),
			Slug::make(),
			Numeric::make('parent_id')
				->integer(),
			Numeric::make('order')
				->integer(),
			TermDepends::make('depends')
				->multiple(),
			Image::make('image')
				->toDirectory('terms'),
			LongText::make('description'),
		];
	}
}
