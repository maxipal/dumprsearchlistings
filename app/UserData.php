<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Stylemix\Listing\EntityData;

class UserData extends EntityData
{

	protected $table = 'users_data';

	public function entity(): BelongsTo
	{
		return $this->belongsTo(User::class, 'entity_id');
	}
}
