<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Stylemix\Listing\EntityData;

class ListingData extends EntityData
{

	protected $table = 'listing_data';

	public function entity() : BelongsTo
	{
		return $this->belongsTo(Listing::class, 'entity_id');
	}

}
