<?php

namespace App\Jobs\Wp;

use App\Term;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ImportTermJob implements ShouldQueue
{

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	private $item;

	private $taxonomy;

	/**
	 * Create a new job instance.
	 *
	 * @param mixed $item
	 * @param       $taxonomy
	 */
	public function __construct($item, $taxonomy)
	{
		$this->item = $item;
		$this->taxonomy = $taxonomy;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$item = $this->item;
		$term = Term::firstOrNew(['import_id' => $item->id]);
		$term->forceFill([
			'import_id' => $item->id,
			'title' => html_entity_decode($item->name),
			'slug' => $item->slug,
			'taxonomy' => $this->taxonomy,
			'parent_id' => $this->getParentId($item),
			'image_id' => $this->getImage(),
			'depends_id' => $this->getDepends(),
			'description' => $item->description,
		]);

		Term::withoutEvents(function () use ($term) {
			$term->save();
		});
	}

	private function getMeta($name, $multiple = false)
	{
		$meta = collect(data_get($this->item->meta, $name));

		return $multiple ? $meta->all() : $meta->first();
	}

	private function getParentId($item)
	{
		if (isset($item->parent) && $item->parent != 0) {
			$term = Term::firstOrNew(['import_id' => $item->parent]);

			if ($term->exists) {
				return $term->id;
			}
			else {
				$term->forceFill([
					'import_id' => $item->parent,
					'taxonomy' => "-",
					'slug' => "-",
					'title' => "-",
				]);

				Term::withoutEvents(function () use ($term) {
					$term->save();
				});

				return $term->id;
			}
		}
	}

	protected function getImage()
	{
		$id = $this->getMeta('stm_image');

		if (!empty($id)) {
			try {
				$res = (new Client())->get(config('wp.url') . '/wp-json/wp/v2/media/' . $id);
			}
			catch (RequestException $e) {
				if ($e->getResponse()->getStatusCode() == 404) {
					return null;
				}

				// Any other HTTP error should fail the command
				return null;
			}

			$res = collect(json_decode($res->getBody()->getContents()));

			return $res->get('source_url');
		}
	}

	protected function getDepends()
	{
		$slugs = $this->getMeta('stm_parent', true);
		if (empty($slugs)) {
			return [];
		}

		$dependTaxes = [];

		if ($this->taxonomy == 'make') {
			$dependTaxes = ['types'];
		}

		if ($this->taxonomy == 'serie') {
			$dependTaxes = ['make'];
		}

		if ($this->taxonomy == 'trim') {
			$dependTaxes = ['serie'];
		}

		$depends = Term::query()
			->whereIn('taxonomy', $dependTaxes)
			->whereIn('slug', $slugs)->get()->keyBy('slug');

		$slugs = array_diff($slugs, $depends->keys()->all());
		if (count($slugs)) {
			foreach ($slugs as &$slug) {
				foreach ($depends->keys()->all() as $found_slug) {
					$slug = trim(str_replace($found_slug, '', $slug), '-');
				}
			}

			if ($this->taxonomy == 'serie') {
				$dependTaxes = ['types'];
			}

			$depends = $depends->merge(
				Term::query()
					->whereIn('taxonomy', $dependTaxes)
					->whereIn('slug', $slugs)->get()->keyBy('slug')
			);
		}

		return $depends->pluck('id')->all();
	}
}
