<?php

namespace App\Jobs\Wp;

use App\Listing;
use App\Jobs\Wp\ImportListingJob;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ImportListingAttachmentsJob extends ImportListingJob
{

	protected $listingId;

	protected $auth;

	public function __construct($listingId, $auth) {
		$this->listingId = $listingId;
		$this->auth = $auth;
		parent::__construct(null);
	}

	public function handle()
	{
		/** @var Listing $listing */
		$listing = Listing::findOrFail($this->listingId);

		$res = (new Client())->get(config('wp.url') . '/wp-json/wp/v2/listings/' . $listing->id, [
			'headers' => [
				'Authorization' => 'Bearer ' . $this->auth->token,
			],
		]);
		$this->item = json_decode($res->getBody()->getContents());

		if (count($listing->gallery_id) < count($this->getGalleryIds())) {
			$listing->gallery_id = $this->getGallery();
		}

		if (!$listing->brochure_id) {
			$listing->brochure_id = $this->getAttachment($this->getNumericMeta('car_brochure'));
		}

//		if (!$listing->valuation_pdf_id) {
//			$listing->valuation_pdf_id = $this->getAttachment($this->getNumericMeta('valuation_pdf'));
//		}

		Listing::withoutEvents(function () use ($listing) {
			$listing->save();
		});
	}

}
