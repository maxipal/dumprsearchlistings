<?php

namespace App\Jobs\Wp;

use App\Listing;
use App\Term;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportListingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	protected $item;

	/**
	 * Create a new job instance.
	 *
	 * @param mixed $item
	 */
    public function __construct($item)
    {
		$this->item = $item;
	}

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

		$item = $this->item;

		$dataToImport = [
			'id' => $item->id,
//			'import_id' => $item->import_id,
//			'import_group_id' => $item->import_id ? 1 : null,
			'author_id' => intval($this->getMeta('stm_car_user')) ?: $item->author,
			'created_at' => $item->date_gmt,
			'title' => $item->title->raw,
			'slug' => $item->slug,
			'status' => $item->status,
			'content' => $this->sanitizeContent($item->content->raw),
			'price' => $this->getNumericMeta('price'),
			'price_sale' => $this->getNumericMeta('sale_price'),
			'currency' => $this->getMeta('stm_currency'),
			'price_label' => $this->getMeta('car_price_form_label'),
			'stock_number' => $this->getMeta('stock_number'),
			'coordinates' => $this->getLocation(),
			'location' => $this->getMeta('stm_car_location'),
			'mileage' => $this->getNumericMeta('mileage'),
//			'transmission' => $this->getMeta('transmission'),
//			'fuel' => $this->getMeta('fuel'),
//			'targa' => $this->getMeta('targa'),
			'engine' => $this->getMeta('engine'),
			'videos' => array_filter($this->getMeta('gallery_video', true)),
			'sold' => trim($this->getMeta('car_mark_as_sold')) !== '',
			'is_special' => trim($this->getMeta('special_car')) !== '',
			'special_front' => $this->getMeta('special_front') == 'true',
			'badge_text' => $this->getMeta('badge_text'),
			'badge_bg_color' => $this->getMeta('badge_bg_color'),
			'years' => $this->getNumericMeta('ca-year'),
			'auction' => trim($this->getMeta('auction')) !== '',
			'import_id' => trim($this->getMeta('id')) !== '',
		];

		$termsToImport = [
			'condition',
			'vehicle-type',
			'body',
			'make',
			'serie',
			'transmission',
			'fuel',
			'interior-color',
			'exterior-color',
			'targa',
			'drive'
		];

		foreach ($termsToImport as $value) {
			$term = $this->getTerm($value);
			if (is_null($term)) {
				continue;
			}
			else {
				$dataToImport[$value . "_id"] = $term;
			}
		}

		$features_term = $this->getTerm('stm_additional_features', 'id', true);

		if (!is_null($features_term)) {
			$dataToImport['features_id'] = $features_term;
		}
		/** @var Listing $listing */
		$listing = Listing::findOrNew($item->id);
        $listing->forceFill($dataToImport);

		$generated = Listing::generatedData($listing);
//		$listing->title = $generated['title'];
//		$listing->slug = $generated['slug'];

		Listing::withoutEvents(function () use ($listing) {
			$listing->save();
		});
	}

	private function sanitizeContent($content)
	{
		$content = str_replace("<div class=\"stm-car-listing-data-single stm-border-top-unit\"><div class=\"title heading-font\">Seller Note</div></div>", '', $content);

		return trim($content);
    }

	/**
	 * @param string $taxonomy
	 * @param string $field
	 * @param bool   $multiple
	 *
	 * @return \Illuminate\Support\Collection|Term
	 */
	private function getTerm($taxonomy, $field = 'id', $multiple = false)
	{
		$terms = collect($this->item->terms)->where('taxonomy', $taxonomy)->map(function ($item) {
			return $this->prepareTerm($item);
		})->pluck($field);

		return $multiple ? $terms->all() : $terms->first();
    }

	private function prepareTerm($item)
	{
		$term = Term::firstOrNew(['import_id' => $item->term_id]);

		if (!$term->exists) {
			$taxonomy = strtr($item->taxonomy, [
				'stm_additional_features' => 'features',
			]);

			$term->forceFill([
				'import_id' => $item->term_id,
				'taxonomy' => $taxonomy,
				'title' => $item->name,
				'slug' => $item->slug,
			]);

			Term::withoutEvents(function () use ($term) {
				$term->save();
			});
		}

		return $term;
    }

	protected function getMeta($name, $multiple = false)
	{
		$meta = collect(data_get($this->item->meta, $name));

		return $multiple ? $meta->all() : $meta->first();
    }

	protected function getNumericMeta($name, $multiple = false)
	{
		$meta = collect(data_get($this->item->meta, $name))
			->filter(function ($value) {
				return trim($value) !== '';
			})
			->map(function ($value) {
				return intval(preg_replace('/[^0-9]/', '',$value));
			});

		return $multiple ? $meta->all() : $meta->first();
    }

	protected function getLocation()
	{
		$lat = trim($this->getMeta('stm_lat_car_admin'));
		$lng = trim($this->getMeta('stm_lng_car_admin'));

		if (!$lng || !$lat) {
			return null;
		}

		return $lat . ',' . $lng;
    }

	protected function getGallery()
	{
		// Limit attachment for testing
		if (app()->environment() === 'local') {
			return [];
		}

		$ids = $this->getGalleryIds();

		if (app()->environment() === 'staging') {
			$ids = array_slice($ids, 0, 2);
		}

		try {
			$res = (new Client())->get(config('wp.url') . '/wp-json/wp/v2/media?per_page=99&parent=' . $this->item->id);
		}
		catch (RequestException $e) {
			if ($e->getResponse()->getStatusCode() == 404) {
				return null;
			}
		}

		$res = collect(json_decode($res->getBody()->getContents()))->keyBy('id');

		$sources = [];
		foreach ($ids as $id) {
			if (!($attachment = $res->get($id))) {
				$sources[] = $this->getAttachment($id);
				continue;
			}

			$sources[] = $attachment->source_url;
		}

		return array_filter($sources);
    }

	/**
	 * @return array|mixed
	 */
	protected function getGalleryIds()
	{
		$ids = array_filter($this->getMeta('gallery'));

		if (!is_array($ids)) {
			$ids = [];
		}

		array_unshift($ids, $this->item->featured_media);

		return $ids;
	}

	protected function getAttachment($id)
	{
		if (empty($id)) {
			return null;
		}

		try {
			$res = (new Client())->get(config('wp.url') . '/wp-json/wp/v2/media/' . $id);
		}
		catch (RequestException $e) {
			if ($e->getResponse()->getStatusCode() == 404) {
				return null;
			}

			// Any other HTTP error should fail the command
			throw $e;
		}

		$res = collect(json_decode($res->getBody()->getContents()));

		return $res->get('source_url');
	}
}
