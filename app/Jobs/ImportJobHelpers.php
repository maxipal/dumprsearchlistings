<?php

namespace App\Jobs;

use App\ImportProcess;


/**
 * @property \App\ImportProcess $import
 */
trait ImportJobHelpers
{

	public function failed(\Exception $exception)
	{
		if (!$this->import) {
			$this->import = ImportProcess::find($this->importId);
		}

		$this->import->increment('failed');
		$this->processed();
	}

	protected function dataGet($key, $default = null)
	{
		return data_get($this->data, $key, $default);
	}

	/**
	 * Increment import processed counter
	 */
	protected function processed() : void
	{
		$this->import->increment('processed', 1);
	}

}
