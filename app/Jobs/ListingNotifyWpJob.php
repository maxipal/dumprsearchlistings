<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Components\Seo;
use GuzzleHttp\Client;

class ListingNotifyWpJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $listing;
    protected $event;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($listing, $event)
    {
        $this->listing = $listing;
        $this->event = $event;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = [
            'listing' => $this->listing->toArray(),
            'changes' => $this->listing->getDirty(),
        ];

        (new Client())
            ->post(config('wp.url') . '?laravel-notification=' . $this->event, [
                'http_errors' => false,
                'form_params' => $data,
            ]);
    }
}
