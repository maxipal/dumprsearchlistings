<?php

namespace App\Jobs;

use App\ImportListing;
use App\ImportProcess;
use App\Listing;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ListingXMLImportPrepareJob implements ShouldQueue
{

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $timeout = 60 * 60 * 24;

	/**
	 * @var \App\ImportProcess
	 */
	protected $import;

	/**
	 * @var \Illuminate\Log\Logger
	 */
	protected $logger;

	protected $trans_obj;
	protected $ad_count = 0;
	protected $import_prefix;
	protected $import_gID = 1;
	protected $forceUpdate = false;
	protected $sync = false;
	protected $dry = false;

	/**
	 * Create a new job instance.
	 *
	 * @param string|boolean $prefix
	 * @param bool $forceUpdate
	 * @param bool $sync
	 * @param bool $dry
	 */
	public function __construct($prefix = false, $forceUpdate = false, $sync = false, $dry = false)
	{
		$this->queue = 'xml';
		$this->forceUpdate = $forceUpdate;
		$this->sync = $sync;
		$this->dry = $dry;

		if (!$prefix) {
			$this->import_prefix = 'url';
			$this->import_gID = 1;
		}
		else {
			$this->import_prefix = $prefix;
			$this->import_gID = 2;
		}
	}

	private function translate($data)
	{
		foreach($data as $attr_name => $val){
			if(!empty($this->trans_obj[$attr_name])){
				foreach ($this->trans_obj[$attr_name] as $obj) {
					$name = $obj['name'];
					$trans = $obj['tran'];
					if(trim($val) == $name){
						$data[$attr_name] = $trans;
					}
				}
			}
		}
		return $data;
	}

	private function replaceMilage($data)
	{
		foreach($data as $attr_name => $val){
			if($attr_name == 'Kilometrazha'){
				$_val = explode('-', $val);
				if(!empty($_val[0])){
					$val = str_replace(' ', '', $_val[0]);
					$data[$attr_name] = $val;
				}
			}
		}
		return $data;
	}

	private function addAdImport($user, $data)
	{

		if (!empty($data['images']) && isset($data['images']['image']) && is_array($data['images']['image'])) {
			foreach ($data['images']['image'] as $key => $image) {
				$data['Image' . ($key + 1)] = trim($image);
			}
		}

		if (!empty($data['props']) && is_array($data['props']['prop'])) {
			foreach ($data['props']['prop'] as $prop) {
				if (!isset($prop['@attributes'])) continue;
				$data[$prop['@attributes']['name']] = $prop['@attributes']['value'];
			}
		}

		foreach ($data as $key => $item) {
			if (empty($item) && is_array($item)) {
				$data[$key] = '';
			}
		}

		unset($data['advertiserEmail'], $data['advertiserPhone'], $data['subSubCategory'], $data['images'], $data['props']);

		$data['description']  = trim($data['description']);
		$data['advertiserId'] = $user['id'];

		// Fill data with blank values
		// in case there are missing in xml
		$data += [
			'Viti' => '',
			'Transmetuesi' => '',
			'Kilometrazha' => '',
			'Karburanti' => '',
			'Targa' => '',
		];

		// Check coordinates
		if(empty($data['mapAltitude'])) $data['mapAltitude'] = '40.98138838366699';
		if(empty($data['mapLongitude'])) $data['mapLongitude'] = '20.06588459014893';

		// Translate Terms
		$data = $this->translate($data);
		$data = $this->replaceMilage($data);


		$this->processRecord($data);
		$this->logInfo("Ad queued: [{$data['id']}] {$data['title']} ${data['changeStatusDate']}");
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$terms_trans = base_path() . '/terms_trans.json';
		$this->trans_obj = json_decode( file_get_contents($terms_trans, true), true);
		$this->configureLogger();
		$this->logInfo('Modes: '
			. join( ' | ', array_filter([
				($this->forceUpdate ? 'force update' : 'skip not modified'),
				($this->sync ? 'full sync' : 'create/update'),
				($this->dry ? 'dry run' : null),
			]))
		);

		$xml_config = config('services.xml_import.'.$this->import_prefix);
		if (!$xml_config) {
			$this->logInfo('No XML source configured. Exiting.');
			return;
		}

		$this->import = (new ImportProcess)->forceFill([
	        'file' => $xml_config,
	        'processor' => $this->import_gID,
	        'options' => '',
	        'status' => 'pending',
	        'total' => 0,
	        'processed' => 0,
	        'created' => 0,
	        'updated' => 0,
	        'deleted' => 0,
	        'failed' => 0,
	    ]);

		if (!$this->dry) {
			$this->import->save();
		}

		$this->logInfo('XML import parsing started at: ' . date('H:i:s', time()));

		$xml = new \XMLReader();
		$doc = new \DOMDocument();

		if ($this->import->status != 'processing') {
			$this->download();
		}

		$xml->open(storage_path($this->import->file));

		$user = [];

		$userElementNames = [
			'email',
			'telephone',
			'viber',
			'skype',
			'whatsapp',
			'password',
			'companyName',
			'advertiserName',
			'address',
			'zip',
			'siteLink',
			'siteName',
			'mapAltitude',
			'mapLongitude',
			'location',
			'subLocation',
			'companyLogo',
			'companyDescription',
		];

		$skip = array(
			'email' => 0,
			'phone' => 0,
			'user_not_found' => 0,
			'category' => 0,
			'make_model' => 0,
			'status_date' => 0,
		);

		while ($xml->read()) {
			try {
				if (!$this->dry) {
					$this->import->options = $skip;
					$this->import->save();
				}

				if ($xml->name == 'advertiser' && $xml->nodeType == \XMLReader::ELEMENT) {
					// A new user data starts here
					$user = [
						'advertiserId' => $xml->getAttribute('id'),
					];

					// Fill empty user data with known data keys
					foreach ($userElementNames as $name) {
						$user[$name] = '';
					}

					// Default password
					$user['password'] = 'qwerty';

					continue;
				}

				foreach ($userElementNames as $name) {
					if ($xml->name == $name && $xml->nodeType == \XMLReader::ELEMENT) {
						// Fill only non empty strings
						// since we have blank data values by default
						if (trim($string = $xml->readString()) != '') {
							$user[$name] = $string;
						}
					}
				}

				// End of user data
				if ($xml->name == 'ads' && ($xml->nodeType == \XMLReader::ELEMENT)) {
					// Don't import if user has neither email nor phone
					if (empty($user['email'])){
						$user['id'] = null;
						$this->logInfo("User has neither email: {$user['advertiserId']}");
						++$skip['email'];
						continue;
					}
					if (empty($user['email']) && empty($user['telephone'])) {
						$user['id'] = null;
						$this->logInfo("User has neither email nor phone: {$user['advertiserId']}");
						++$skip['phone'];
						continue;
					}

					$user = $this->resolveUser($user);
				}

				if ($xml->name == 'ad' && ($xml->nodeType == \XMLReader::ELEMENT || $xml->nodeType == \XMLReader::CDATA)) {
					// No user - no ads
					if (!$user['id']) {
						$this->logInfo("User skipped: {$user['advertiserId']}");
						++$skip['user_not_found'];
						continue;
					}

					$node              = simplexml_import_dom($doc->importNode($xml->expand(), true));
					$node->description = (string) $node->description;
					$data              = json_decode(json_encode($node), true);

					// Filter only cars from xml
					if($this->import_gID == 2){
						if (!empty($data['subCategory']) && $data['subCategory'] !== 'Vetura') {
							$this->logInfo("Ad skipped: Category not Vetura.");
							++$skip['category'];
							continue;
						}
					}else{
						if ($data['subCategory'] !== 'Makina') {
							$this->logInfo("Ad skipped: Category not Makina");
							++$skip['category'];
							continue;
						}
					}

					// Check on empty make or model
					if(empty($data['model']) || empty($data['make'])){
						$this->logInfo("Ad skipped: Empty make or model");
						++$skip['make_model'];
						continue;
					}

					$this->ad_count++;

					// Check ID
					$vid = $data['id'];

					$this->createImportListing($vid);

					$listing = Listing::where('import_id', $vid)->first();
					if (!$listing) {
						$this->addAdImport($user, $data);
					} else {
						if ($this->shouldSkipListing($data, $listing)) {
							$this->import->increment('processed');
							$this->logInfo("Ad skipped $listing->title ($listing->id:$vid) - not modified");
							++$skip['status_date'];
						}else{
							$this->addAdImport($user, $data);
						}
					}
				}
			}
			catch(\Exception $e) {
				report($e);
			}
		}

		$xml->close();

		if (!$this->dry) {
			$this->import->total = $this->ad_count;
			$this->import->save();
		}
		$this->logInfo("XML import parsing finished");

		$this->deleteListings();

		// Create sitemap xml files
		//Artisan::call('create:sitemap', ["--force" => true ]);
	}

	public function processRecord($record)
	{
		if ($this->dry) {
			return;
		}

		dispatch(
			(new ListingXMLImportJob($record, $this->import->id, $this->import_gID))
				->onQueue('import')
		);
	}

	/**
	 * The job failed to process.
	 *
	 * @param  \Exception  $exception
	 * @return void
	 */
	public function failed(\Exception $exception)
	{
		$this->logInfo('XML import error: ' . $exception->getMessage());
	}

	protected function configureLogger()
	{
		// Configure logger at runtime,
		// since it does not exist in config/logger.php
		// this way we will get a new logger instance for each import group
		$channel = 'import' . $this->import_gID;
		Config::set('logging.channels.' . $channel . '', [
			'driver' => 'daily',
			'path' => storage_path("logs/import-xml-{$this->import_gID}.log"),
			'level' => 'debug',
			'days' => 60,
		]);
		$this->logger = Log::channel($channel);
	}

	protected function logInfo($text)
	{
		echo $text."\n";
		$this->logger->info($text);
	}

	protected function download()
	{
		$filename = rtrim(config('services.xml_import.storage_path'), '/');
		$filename .= '/' . md5($this->import->file . date('w j Y', time())) . '.xml';
		if (file_exists(storage_path('app/' . $filename))) {
			$this->logInfo("XML import already downloaded. File: $filename");
		}
		else {
			Storage::disk()->put($filename, fopen($this->import->file, 'r'));
			$this->logInfo("XML import downloaded. Size: "
				. filesize(storage_path('app/' . $filename)));
		}

		$this->import->forceFill([
			'file' => 'app/' . $filename,
			'total' => 0,
			'status' => 'processing',
		]);

		if (!$this->dry) {
			$this->import->save();
		}
	}

	protected function resolveUser(array $user)
	{
		$existing = User::where('email', $user['email'])->first();
		if ($existing) {
			$user['id'] = $existing->import_id;
			$log = "User found";
		}
		else {
			try {
				if ($this->dry) {
					$user['id'] = 'new';
				}
				else {
					$user['id'] = (new Client())->post(config('wp.url'), [
						'form_params' => ['laravel_import_user' => 1] + $user,
						'http_errors' => true,
					])->getBody()->getContents();

					// We expect that response contains number only
					// If not we shouldn't accept this user
					if ($user['id'] && !is_numeric($user['id'])) {
						$user['id'] = null;
					}
				}
				$log = 'User imported';
			}
			catch (\Exception $e) {
				report($e);
				$user['id'] = null;
				$log = 'User import failed';
			}
		}

		$this->logInfo("{$log}: {$user['email']} [{$user['advertiserId']}] => {$user['id']}");

		return $user;
	}

	private function createImportListing($import_id)
	{
		if (!$this->sync || $this->dry) {
			return;
		}

		ImportListing::create([
			'import_group_id' => $this->import_gID,
			'import_id' => $import_id,
			'import_processes_id' => $this->import->id,
		]);
	}

	protected function shouldSkipListing($data, Listing $listing): bool
	{
		if ($this->forceUpdate) {
			return false;
		}

		$modified = false;
		if (!empty($data['changeStatusDate'])) {
			$modified = \Carbon\Carbon::parse($data['changeStatusDate']);
		}

		return $modified
			&& $listing->updated_at
			&& $listing->updated_at->gte($modified);
	}

	/**
	 * Deleting listing difference
	 */
	protected function deleteListings()
	{
		if (!$this->sync || $this->dry) {
			return;
		}

		Listing::query()
			->where('import_group_id', $this->import_gID)
			->chunk(10, function ($results) {
				foreach ($results as $listing) {
					$exists = ImportListing::query()
						->where([
							'import_group_id' => $this->import_gID,
							'import_id' => $listing->import_id,
							'import_processes_id' => $this->import->id,
						])
						->exists();
					if (!$exists) {
						$listing->delete();
						$this->import->increment('deleted');
					}
				}
			});

		ImportListing::where([
			'import_group_id' => $this->import_gID,
			'import_processes_id' => $this->import->id,
		])->delete();
	}

	public static function updatesStatuses()
	{
		$processes = ImportProcess::query()->whereIn('status', ['processing', 'pending'])->get();
		/** @var \App\ImportProcess $process */
		foreach ($processes as $process) {
			$overdue = now()->subHour();
			if ($process->updated_at->lessThan($overdue)) {
				$process->status = 'failed';
				$process->save();
				continue;
			}

			if ($process->total !== 0 && $process->processed >= $process->total) {
				$process->status = 'finished';
				$process->save();
				continue;
			}
		}
	}
}
