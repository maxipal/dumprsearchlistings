<?php

namespace App\Jobs;

use App\ImportLocation;
use App\Listing;
use App\User;
use App\Term;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use App\Listeners\ListingObserver;
use Illuminate\Support\Str;

class ListingCountriesTownsJob implements ShouldQueue
{

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	public $timeout = 60 * 60 * 24;

	/**
	 * @var \App\ImportProcess
	 */
	protected $import;
	protected $ad_count = 0;
	protected $skipEx = true;

	/**
	 * Create a new job instance.
	 *
	 * @param \App\ImportProcess $import
	 *
	 * @return void
	 */
	public function __construct($skipEx, $onlyxml)
	{
		$this->skipEx = $skipEx;
		$this->only_xml = $onlyxml;
	}


	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		if(!$this->only_xml){
			$path = storage_path( 'app/import-downloads/sitemap.smap');
			if(file_exists($path)) unlink($path);

			$skipEx = $this->skipEx;

			// Drop links file
			self::put_url_to_sitemap('', false);

			$listings = Listing::chunk(100,function($listings) use ($skipEx){
				foreach ($listings as $index => $listing) {
					self::addCountryTownAlt($listing, $skipEx);
				}
			});
		}
		ini_set('memory_limit','4048M');
		self::createSiteMap();
	}

	public static function addCountryTownAlt($listing, $skipExists = true)
	{
		$c = array(
			'al' => 'Shqipëria',
			'xk' => 'Kosova',
		);

		// Get data if town from location
		$town_name = trim($listing->location);
		$town_slug = Str::slug($town_name);
		// if(!$skipExists && (empty($listing->countries_id) || !empty($listing->towns_id))){
		// 	return;
		// }

		// Check exists town
		if($town = Term::where('taxonomy', 'towns')->where('slug', $town_slug)->first()){
			// Get country from town
			if(!empty($town->depends_id[0])){
				$country = Term::where('taxonomy', 'countries')->where('id', $town->depends_id[0])->first();
			}else{
				$country_name = $listing->import_group_id == 1 ? $c['al'] : $c['xk'];
				$country_slug = Str::slug($country_name);
				$country = Term::where('taxonomy', 'countries')->where('slug', $country_slug)->first();
				if(!$country){
					// Log::channel('import_location')->debug("$listing->id:Cant find country: $country_name");
					return;
				}
			}
		}else{
			// Get data of town from OpenStreetMap
			$place = self::getPlaceParams($town_name, $c);
			// Log::channel('import_location')->debug("$listing->id:Town place array: ".var_export($place, true));

			$t_name = $t_slug = $t_country = false;
			if(!empty($place['address']['city'])){
				$t_name = $place['address']['city'];
				$t_slug = Str::slug($t_name);
				$t_country = $place['address']['country'];
			}elseif(!empty($place['address']['town'])){
				$t_name = $place['address']['town'];
				$t_slug = Str::slug($t_name);
				$t_country = $place['address']['country'];
			}else{
				// Log::channel('import_location')->debug("$listing->id:Skip listing {$listing->title} ({$listing->id}) that cant find location '{$town_name}'");
				// Log::channel('import_location')->debug("$listing->id:Return: ".var_export($place, true)."\n");
			}

			if(!$t_country || !$t_name){
				// Log::channel('import_location')->debug("$listing->id:Country or town empty from response: $t_country:$t_name\n");
				return;
			}

			$country = Term::where('taxonomy', 'countries')->where('slug', Str::slug($t_country))->first();
			if(!$country){
				// Log::channel('import_location')->debug("$listing->id:Cant find country in DB: $t_country:$t_name\n");
				return;
			}

			// Add town in DB
			$param = array_filter([
				'taxonomy' => 'towns',
				'title' => $t_name,
				'slug' => Str::slug($t_name),
			]);
			$town = Term::firstOrNew($param);
			$town->depends_id = $country->id;

			// Check lat lng params
			if(empty($town->loc_lat) || empty($town->loc_lon)){
				$temp = self::getTownParams($t_name);
				if(!empty($temp['lat']) && !empty($temp['lon'])){
					$town->loc_lat = $temp['lat'];
					$town->loc_lon = $temp['lon'];
				}
			}
			$town->save();
		}


		// Log::channel('import_location')->debug("$listing->id:Country: {$country->title}");
		// Log::channel('import_location')->debug("$listing->id:Town: {$town->title}\n");

		$listing->countries_id = $country->id;
		$listing->towns_id = $town->id;

		Listing::withoutEvents(function() use ($listing){
			$listing->save();
		});

		$tax_index = array(
			'body' 		=> 'body_id',
			'make' 		=> 'make_id',
			'serie' 	=> 'serie_id',
			'countries' => 'countries_id',
			'towns' 	=> 'towns_id'
		);
		$link = '';
		foreach ($tax_index as $tax => $tax_id) {
			$term = Term::where('taxonomy', $tax)->where('id', $listing->{$tax_id})->first();
			if(!empty($term->slug)){
				$link .= $term->slug.'/';
				self::put_url_to_sitemap($link, true);
			}
		}
	}


	/**
	 * Get array address of town.
	 *
	 * @return void
	 */
	public static function getTownParams($townName)
	{
		$uri = 'https://nominatim.openstreetmap.org/search?format=jsonv2&addressdetails=1&city='.$townName;
		$res = (new Client)->get($uri);
		$response = json_decode($res->getBody(), true);
		if(!empty($response[0])) return $response[0];
		else return false;
	}

	/**
	 * Get array address of place (town).
	 *
	 * @return void
	 */
	public static function getPlaceParams($place, $c)
	{
		$uri = 'https://nominatim.openstreetmap.org/search?format=jsonv2&addressdetails=1&q='.$place;
		$res = (new Client)->get($uri);
		$response = json_decode($res->getBody(), true);
		if(!empty($response[0])){
			foreach ($response as $res) {
				if(!empty($res['address']['country_code']) && array_key_exists($res['address']['country_code'], $c)) return $res;
			}
		}
		return false;
	}

	/**
	 * Get array address of country.
	 *
	 * @return void
	 */
	public static function getCountryParams($countryName)
	{
		$uri = 'https://nominatim.openstreetmap.org/search?format=jsonv2&addressdetails=1&country='.$countryName;
		$res = (new Client)->get($uri);
		$response = json_decode($res->getBody(), true);
		if(!empty($response[0])) return $response[0];
		else return false;
	}

	/**
	 * Create temp sitemap file with only pathes
	 *
	 * @return null
	 */

	public static function put_url_to_sitemap($url, $append = false){
		$path = storage_path( 'app/import-downloads/sitemap.smap');
		if($append)
			file_put_contents($path, $url."\n",FILE_APPEND);
		else
			file_put_contents($path, $url);
	}


	public static function getLinesFromFile($path, $limit = 2000, $offset = 0)
	{
		$lines = array();
		$fp = fopen($path, 'r');
		fseek($fp, $offset);


		while(!feof($fp)) {
			$line = fgets($fp);
			if(!empty($line)){
				array_push($lines, str_replace(array("\n", "\r"), '', $line));
			}
			$offset += strlen($line);


			if($limit == count($lines)){
				break;
			}
		}
		fclose($fp);
		return [
			'lines' => $lines,
			'offset' => $offset
		];
	}



	public static function fixRemoveDuplicateFromFile($path)
	{
		$file = file_get_contents($path);
		$lines = explode("\n",$file);
		return array_unique($lines,SORT_STRING);
	}

	/**
	 * Create sitemap files categories_index.xml and categories-sitemap0.xml in public path
	 *
	 * @return null
	 */
	public static function createSiteMap($offset = 0, $step = 0, $list = [])
	{
		$limit = 2000;
		$urls = array();
		$modified = date('c');

		$path = storage_path( 'app/import-downloads/sitemap.smap');
		if(!file_exists(storage_path( 'app/import-downloads/sitemap-fix.smap'))){
			$lines = self::fixRemoveDuplicateFromFile($path);
			$path = storage_path( 'app/import-downloads/sitemap-fix.smap');
			file_put_contents($path, implode("\n", $lines));
		}
		$path = storage_path( 'app/import-downloads/sitemap-fix.smap');

		if(file_exists($path)){
			// $urls = explode("\n", file_get_contents($path));
			$temp = self::getLinesFromFile($path, $limit, $offset);
			$urls = $temp['lines'];
			if(!empty($urls)){
				$urls_cnt = count($temp['lines']);
				$offset = $temp['offset'];
			}
		}



		$item = '';
		if(isset($urls_cnt)){
			$item .= '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/').'/css/main-sitemap.xsl"?>'."\n";
			$item .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'."\n";
			for($i = 0; $i <= $urls_cnt; $i++){
				if(!isset($urls[$i])) break;
				$item .= "<url>\n\t<loc>".config('wp.url')."/".$urls[$i]."</loc>\n\t<changefreq>weekly</changefreq>\n\t<lastmod>$modified</lastmod>\n\t<priority>1</priority></url>\n";
			}
			$item .= "</urlset>\n";
			file_put_contents(public_path("categories-sitemap$step.xml"), $item);
			echo "File saved: ".public_path("categories-sitemap$step.xml")."\n";
			$list[] = url('/')."/categories-sitemap$step.xml";
		}




		if(!$urls || $limit > count($urls)){
			$main_xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/').'/css/main-sitemap.xsl"?>'."\n";
			$main_xml .= '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
			foreach ($list as $url) {
				$main_xml .= "\t<sitemap>\n\t\t<loc>$url</loc>\n\t\t<lastmod>$modified</lastmod>\n\t</sitemap>\n";
			}

			$main_xml .= '</sitemapindex>';

			file_put_contents(public_path('categories_index.xml'), $main_xml);
			echo "File saved index: ".public_path('categories_index.xml')."\n";
		}

		if(!$urls){
			exit;
		}

		$step++;
		self::createSiteMap($offset, $step, $list);
		// return;
	}
}
