<?php

namespace App\Jobs;

use App\Attributes\Author;
use App\Attributes\TermRelation;
use App\Components\Seo;
use App\ImportProcess;
use App\Listing;
use App\Term;
use App\WpFlushQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Plank\Mediable\SourceAdapters\RemoteUrlAdapter;
use Stylemix\Listing\Attribute\Attachment;
use Stylemix\Listing\Attribute\Base;
use Stylemix\Listing\Attribute\Boolean;
use Stylemix\Listing\Attribute\Numeric;
use Stylemix\Listing\Attribute\PriceWithSale;
use Carbon\Carbon;
use App\Listeners\ListingObserver;

class ListingXMLImportJob implements ShouldQueue
{

	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, ImportJobHelpers;

	public $timeout = 60 * 5;

	protected $data;

	protected $car;

	/** @var Listing */
	protected $model;

	/** @var int */
	protected $importId;

	/** @var \App\ImportProcess */
	protected $import;

	protected $tempFiles = [];

	protected $import_gID = 1;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data, $importId, $gid = 1)
	{
		$this->data     = $data;
		$this->importId = $importId;
		$this->import_gID = $gid;
	}

	/**
	 * Execute the job.
	 *
	 * @inheritdoc
	 */
	public function handle()
	{
		$this->import = ImportProcess::find($this->importId);
		if (!$this->import) {
			return;
		}

		$import_id = intval($this->dataGet('id'));

		if (!$import_id) {
			throw new \Exception('Unique ID is empty');
		}

		$fields = [
			$this->mapField('author', '{advertiserId}'),
			$this->mapField('title', '{title}'),
			$this->mapField('content', '{description}'),
			$this->mapField('location', '{location}'), // custom Location
			$this->mapField('coordinates', '{mapAltitude},{mapLongitude}'), // custom Location
			$this->mapField('body', '{carBody}'),
			$this->mapField('make', '{make}'),
			$this->mapField('serie', '{group} > {model}'),
			$this->mapField('price', ['original' => '{priceOrginal}', 'sale' => '{price}']),
			$this->mapField('currency', '{priceCurrency}'), // custom Text
			$this->mapField('gallery', '{Image1},{Image2},{Image3},{Image4},{Image5},{Image6},{Image7},{Image8},{Image9},{Image10},{Image11},{Image12}'),
			$this->mapField('years', '{Viti}'),
			$this->mapField('mileage', '{Kilometrazha}'),
			$this->mapField('transmission', '{Transmetuesi}'),
			$this->mapField('fuel', '{Karburanti}'),
			$this->mapField('targa', '{Targa}'),
			$this->mapField('exterior-color', '{carColor}'),
		];

		$this->model = Listing::withTrashed()->firstOrNew([
			'import_group_id' => $this->import_gID,
			'import_id' => $import_id,
		], [
			'status' => 'publish',
		]);

		ListingObserver::skipNotifyingWp(function () {
			if ($this->model->trashed()) {
				$this->model->restore();
			}
		});

		$this->model->import_group_id = $this->import_gID;
		$this->model->import_id = $import_id;

		// to prevent setting fresh timestamps for date columns
		$this->model->timestamps = false;
		$this->model->updated_at = Carbon::parse($this->dataGet('changeStatusDate'));
		$this->model->import_updated_at = $this->model->updated_at; // for backward compatibility
		if (!$this->model->exists) {
			$this->model->created_at = Carbon::parse($this->dataGet('createDate'));
		}

		$conditions = Term::where('taxonomy', 'condition')->pluck('id', 'slug');
		if(empty($conditions['used-cars'])){
			$term = Term::firstOrNew(array_filter([
				'taxonomy' => 'condition',
				'title' => 'Used car',
				'slug' => 'used-cars',
			]));
			$term->save();
			$conditions = Term::where('taxonomy', 'condition')->pluck('id', 'slug');
		}
		$this->model->condition_id = $this->dataGet('Kilometrazha') ? $conditions['used-cars'] : $conditions['used-cars'];

		foreach (array_filter($fields) as $field) {
			$this->resolveField($field['attribute'], $field['template'], $field['resolve']);
		}

		/* Import Dependencies */

		if ($serie = Term::find($this->model->serie_id)) {
			$serie->depends_id = [$this->model->make_id];
			$serie->save();

			if ($parentSerie = Term::find($serie->parent_id)) {
				$parentSerie->depends_id = [$this->model->make_id];
				$parentSerie->save();
			}
		}

		/* End of Import Dependencies */

		ListingObserver::skipNotifyingWp(function () {
			if (!$this->model->save()) {
				return;
			}

			ListingCountriesTownsJob::addCountryTownAlt($this->model);
		});

		$this->import->increment($this->model->wasRecentlyCreated ? 'created' : 'updated');
		$this->processed();

		Storage::delete($this->tempFiles);
	}

	protected function mapField($attribute, $template)
	{
		if (!($attribute = $this->getAttributes()->keyByAll()->get($attribute))) {
			return null;
		}

		$resolve   = 'Text';

		$resolvers = [
			TermRelation::class => 'Term',
			Attachment::class => 'Attachment',
			Author::class => 'Author',
			PriceWithSale::class => 'Price',
			Boolean::class => 'Boolean',
			Numeric::class => 'Numeric',
		];

		foreach ($resolvers as $class => $key) {
			if ($attribute instanceof $class) {
				$resolve = $key;
				break;
			}
		}

		return [
			'attribute' => $attribute,
			'template' => $template,
			'resolve' => 'resolve' . $resolve . 'Field',
		];
	}

	protected function resolveField(Base $attribute, $template, $resolveMethod)
	{
		if (is_array($template)) {
			$value = [];
			foreach ($template as $key => $template_item) {
				$value[$key] = $this->resolveTemplateValue($template_item);
			}
		}
		else {
			$value = $this->resolveTemplateValue($template);
		}

		$this->{$resolveMethod}($value, $attribute);
	}

	protected function resolveTextField($value, Base $attribute)
	{
		$this->model->setAttribute($attribute->fillableName, trim($value));
	}

	protected function resolveNumericField($value, Numeric $attribute)
	{
		if (trim($value) === '') {
			$this->model->setAttribute($attribute->fillableName, null);
		}

		$this->model->setAttribute($attribute->fillableName, $attribute->integer ? intval($value) : floatval($value));
	}

	protected function resolvePriceField($value, PriceWithSale $attribute)
	{
		$price = $value['original'];
		$sale = $value['sale'];

		if (!$price && $sale) {
			$price = $sale;
			$sale = '';
		}

		$price = trim($price) === '' ? null : floatval($price);
		$sale = trim($sale) === '' ? null : floatval($sale);
		$this->model->setAttribute($attribute->fillableName, $price);
		$this->model->setAttribute($attribute->saleName, $sale);
	}

	protected function resolveBooleanField($value, Boolean $attribute)
	{
		$this->model->setAttribute($attribute->fillableName, !!$value);
	}

	protected function resolveAuthorField($value, Author $attribute)
	{
		$this->model->setAttribute($attribute->fillableName, intval($value));
	}

	protected function resolveAttachmentField($value, Attachment $attribute)
	{
		$urls = collect(array_map('trim', explode(',', $value)))->filter();
		$files = collect();
		$existing = $this->model->getMedia($attribute->name);

		foreach ($urls as $url) {
			$source = new RemoteUrlAdapter($url);

			if ($source->size()) {
				// Search attached media for the same size
				// If found just take its ID to prevent downloading
				if ($found = $existing->where('size', $source->size())->first()) {
					$files[] = $found->id;
					// Continue to next source
					continue;
				}
			}

			$url_path = parse_url($url, PHP_URL_PATH);
			$path = rtrim(config('services.xml_import.storage_path'), '/');
			$this->tempFiles[] = $file = $path . '/img-temp/' . md5($url) . '.' . (pathinfo($url_path, PATHINFO_EXTENSION) ?? 'jpg');
			Storage::put($file, file_get_contents($url));
			$file = storage_path('app/' . $file);

			// Search attached media for the same size
			// If found just take its ID to prevent downloading
			if ($found = $existing->where('size', filesize($file))->first()) {
				$files[] = $found->id;
			}
			else {
				$files[] = $file;
			}
		}

		$this->model->setAttribute($attribute->fillableName, $attribute->multiple ? $files->all() : $files->first());
	}

	protected function resolveTermField($value, TermRelation $attribute)
	{
		$values = array_filter(array_map('trim', explode(',', $value)));
		$terms  = collect();

		foreach ($values as $value) {
			$hierarchy = array_filter(array_map('trim', explode('>', $value)));
			$parent    = null;
			foreach ($hierarchy as $name) {
				$term   = $this->prepareTerm($name, $parent, $attribute);
				$parent = $term->id;
			}

			if (!isset($term)) {
				continue;
			}

			$terms->push($term);
		}

		$value = $attribute->multiple ? $terms->pluck('id')->all() : $terms->pluck('id')->first();
		$this->model->setAttribute($attribute->fillableName, $value);
	}

	protected function prepareTerm($title, $parent_id, TermRelation $attribute)
	{
		$term = Term::firstOrNew(array_filter([
			'taxonomy' => $attribute->taxonomy,
			'title' => $title,
			'parent_id' => $parent_id,
		]));

		if (!$term->exists) {
			$term->forceFill([
				'parent_id' => $parent_id,
				'taxonomy' => $attribute->taxonomy,
				'title' => $title,
				'slug' => Str::slug($title),
			]);

			Term::withoutEvents(function () use ($term) {
				$term->save();
			});
		}

		return $term;
	}

	protected function prepareDependencyTerm($title, $parent_id, $taxonomy)
	{
		if (empty($title)) {
			return null;
		}

		$term = Term::firstOrNew(array_filter([
			'taxonomy' => $taxonomy,
			'title' => $title,
			'parent_id' => $parent_id,
		]));

		if (!$term->exists) {
			$term->forceFill([
				'parent_id' => $parent_id,
				'taxonomy' => $taxonomy,
				'title' => $title,
				'slug' => Str::slug($title),
			]);
		}

		return $term;
	}

	/**
	 * @return \Stylemix\Listing\Attribute\Base[]|\Stylemix\Listing\AttributeCollection
	 */
	protected function getAttributes()
	{
		return Listing::getAttributeDefinitions();
	}

	protected function resolveTemplateValue($template)
	{
		return preg_replace_callback('/\{([^\}]+)\}/', function ($matches) {
			if (!empty($this->data[$matches[1]])) {
				return data_get($this->data, $matches[1], '');
			}
			else {
				return '';
			}
		}, $template);
	}
}
