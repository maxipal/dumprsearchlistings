<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class ImportProcess
 * @property integer $total
 * @property integer $processed
 * @property array $options
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @mixin  \Eloquent
 */
class ImportProcess extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var  array
     */
    protected $guarded = ['*'];

    protected $casts = [
    	'total' => 'int',
    	'processed' => 'int',
    	'created' => 'int',
    	'updated' => 'int',
    	'failed' => 'int',
    	'deleted' => 'int',
    	'options' => 'array',
	];

    protected $table = 'import_processes';

    protected static function boot()
	{
		parent::boot();

		static::deleting(function (ImportProcess $listings_import) {
			Storage::disk()->delete($listings_import->file);
		});
	}

}
