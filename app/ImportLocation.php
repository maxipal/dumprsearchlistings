<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class ImportLocation
 * @property array $options
 * @mixin  \Eloquent
 */
class ImportLocation extends Model
{

    /**
     * The attributes that aren't mass assignable.
     *
     * @var  array
     */
    protected $guarded = ['*'];

    protected $casts = [
    	'total' => 'int',
    	'processed' => 'int',
    	'created' => 'int',
    	'updated' => 'int',
    	'passed' => 'int',
    	'options' => 'array',
	];

    protected $table = 'import_locations';

    protected static function boot()
	{
		parent::boot();

	}

}
