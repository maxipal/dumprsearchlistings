<?php

namespace App\Console\Commands;

use App\Term;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ImportFeaturesGroup extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'import:features-group';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import features group';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console commands
	 */
	public function handle()
	{
		$file_content = $this->get_file_content();

		if (!$file_content) {
			$this->error('Error reading groups file.');

			return;
		}

		$groups = $this->parse_data_from_file($file_content);

		foreach ($groups as $gi => $group_data) {
			$group = Term::firstOrNew([
				'taxonomy' => 'feature_group',
				'title' => $group_data['name'],
				'slug' => $group_data['slug'],
			]);

			$group->order = $gi;

			Term::withoutEvents(function () use ($group) {
				$group->save();
			});
			$created = 0;
			$updated = 0;

			foreach ($group_data['items'] as $i => $feature) {
				$term = Term::query()
					->where('taxonomy', 'features')
					->where('title', $feature)
					->first();

				if (empty($term)) {
					$term = new Term([
						'taxonomy' => 'features',
						'title' => $feature,
					]);
					$created ++;
				}
				else {
					$updated ++;
				}

				$term->order = $i;
				$term->depends_id = array_unique(array_merge($term->depends_id, [$group->id]));
				Term::withoutEvents(function () use ($term) {
					$term->save();
				});
			}

			$this->comment('Imported <info>' . $group_data['name'] . "</info>: $created created, $updated updated.");
		}
	}

	public function get_file_content()
	{
		$file_name = 'features_groups.txt';
		if (Storage::disk('local')->exists($file_name)) {
			$file = Storage::disk('local')->get($file_name);
		}
		else {
			$this->error('Put file with data in storage/app/features_groups.txt');
			exit;
		}

		return $file;
	}

	public function parse_data_from_file($file_content)
	{
		$groups = [];
		$lines  = explode(PHP_EOL, $file_content);

		foreach ($lines as $line) {
			$line  = explode(':', $line);
			if (count($line) < 2 || empty($line[1])) {
				continue;
			}

			list($group_name, $group_slug) = explode('|', $line[0]);
			$group = [
				'name' => $group_name,
				'slug' => $group_slug,
				'items' => [],
			];

			$features = explode(',', $line[1]);

			foreach ($features as $features_datum) {
				array_push($group['items'], $features_datum);
			}

			$groups[] = $group;
		}

		return $groups;
	}
}
