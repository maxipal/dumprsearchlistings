<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Listing;
use App\Jobs\ListingCountriesTownsJob;

class CreateCatSiteMap extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'create:catsitemap';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create Categories sitemap';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console commands
	 */
	public function handle()
	{
		$path = storage_path( 'app/import-downloads/sitemap.smap');
		if(file_exists($path)) unlink($path);

		$listings = Listing::search()
		    ->where('loc_assign', false)
		    ->setPerPage(2000)
		    ->get();

		foreach ($listings as $index => $listing) {
			$tax_index = array(
				'body' 		=> 'body_id',
				'make' 		=> 'make_id',
				'serie' 	=> 'serie_id',
				'countries' => 'countries_id',
				'towns' 	=> 'towns_id'
			);
			$link = '';
			foreach ($tax_index as $tax => $tax_id) {
				$term = Term::where('taxonomy', $tax)->where('id', $listing->{$tax_id})->first();
				if(!empty($term->slug)){
					$link .= $term->slug.'/';
					self::put_url_to_sitemap($link, true);
				}
			}
		}
		self::createSiteMap();
	}

}
