<?php

namespace App\Console\Commands;

use App\Jobs\Wp\ImportTermJob;
use App\ListingAttribute;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;

class ImportTermsCommand extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'import:terms {taxonomy?} {--limit=}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import terms data';

	/**
	 * @var int Limit records to import
	 */
	protected $limit;

	/**
	 * @var int Number of records queued for imports
	 */
	protected $queued;

	protected $taxes;

	/**
	 * @var \GuzzleHttp\Client
	 */
	private $http;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Client $http)
	{
		parent::__construct();
		$this->http = $http;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$taxonomy = $this->argument('taxonomy');

		if ($taxonomy) {
			$this->importTaxonomy($taxonomy);
		} else {
			$taxes = $this->taxonomies();

			$this->importListingAttributes();

			foreach ($taxes as $taxonomy) {
				$this->importTaxonomy($taxonomy);
			}
		}
	}

	protected function importListingAttributes() {

		if(!empty($this->taxes)){
			$i = 0;
			foreach ($this->taxes as $tax_slug => $tax) {
				$this->comment('Import Tax: ' . $tax['label']);
				$type = 'App\Attributes\TermRelation';
				if($tax['numeric']) $type = 'Stylemix\Listing\Attribute\Numeric';
				$attribute = ListingAttribute::firstOrNew(['name' => $tax_slug]);
				$attribute->type = $type;
				$attribute->name = $tax_slug;
				$attribute->options = \Illuminate\Support\Arr::except($tax, ['slug', 'numeric']);
				$attribute->order = $i;
				$attribute->save();
				$i++;
			}
		}
	}

	protected function limitReached($queued)
	{
		return $this->limit && $queued == $this->limit;
	}

	protected function finished()
	{
		$this->info('Queue finished');
	}

	protected function taxonomies()
	{
		try {
			$url = config('wp.url') . "/wp-json/wp/v2/hybrid-taxes";
			$this->comment('Requesting Taxes: ' . $url);
			$res = $this->http->get($url);
		}
		catch (RequestException $e) {
			// We get reached end of the records
			if ($e->getResponse()->getStatusCode() == 400) {
				$this->info('400');
			}

			// Wrong url
			if ($e->getResponse()->getStatusCode() == 404) {
				$this->info('404: ' . $url);
			}

			// Any other HTTP error should fail the command
			throw $e;
		}

		$res = json_decode($res->getBody()->getContents(), true);
		$this->taxes = $res;
		\Settings::set('listing-taxes', $this->taxes);
		\Settings::save();
		$temp = array();
		foreach ($res as $tax => $opt) {
			$temp[] = $tax;
		}
		$temp[] = 'stm_additional_features';
		return $temp;
	}

	/**
	 * @param $taxonomy
	 */
	protected function importTaxonomy($taxonomy)
	{
		$page        = 1;
		$queued      = 0;
		$this->limit = $this->option('limit');

		while (true) {
			try {
				$url = config('wp.url') . "/wp-json/wp/v2/taxonomy-{$taxonomy}?per_page=20&page=" . $page;
				$this->comment('Requesting: ' . $url);
				$res = $this->http->get($url);
			}
			catch (RequestException $e) {
				// We get reached end of the records
				if ($e->getResponse()->getStatusCode() == 400) {
					$this->finished();
					break;
				}

				// Wrong url
				if ($e->getResponse()->getStatusCode() == 404) {
					$this->info('404: ' . $url);
					break;
				}

				// Any other HTTP error should fail the command
				throw $e;
			}

			$res = json_decode($res->getBody()->getContents());

			if (empty($res)) {
				$this->finished();
				break;
			}

			foreach ($res as $item) {
				$localTaxonomy = $taxonomy;
				if ($taxonomy == 'stm_additional_features') {
					$localTaxonomy = 'features';
				}
				if($localTaxonomy == 'price') continue;
				dispatch((new ImportTermJob($item, $localTaxonomy))->onQueue('damprsearch'));
				$queued++;

				if ($this->limitReached($queued)) {
					break;
				}
			}

			$this->info('Queued ' . $queued . ' items for import');

			if ($this->limitReached($queued)) {
				break;
			}

			// Continue to the next page
			$page++;
		}
	}
}
