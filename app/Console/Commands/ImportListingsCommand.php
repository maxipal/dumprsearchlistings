<?php

namespace App\Console\Commands;

use App\Jobs\Wp\ImportListingJob;
use App\Listing;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;

class ImportListingsCommand extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'import:listings {--limit=} {--start-page=} {--per-page=} {add?} {--v}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import data';

	/**
	 * @var int Limit records to import
	 */
	protected $limit;

	/**
	 * @var mixed Auth info
	 */
	protected $auth;

	/**
	 * @var \GuzzleHttp\Client
	 */
	private $http;

	/**
	 * Create a new command instance.
	 *
	 * @param \GuzzleHttp\Client $http
	 */
	public function __construct(Client $http)
	{
		parent::__construct();
		$this->http = $http;
	}

	/**
	 * Execute the console command.
	 */
	public function handle()
	{
		$this->authorize();

		$page        = $this->option('start-page') ?? 1;
		$queued      = 0;
		$this->limit = $this->option('limit');
		$perPage     = $this->limit ? min($this->limit, 10) : 10;

		while (true) {
			try {
				$url = config('wp.url') . "/wp-json/wp/v2/listings/?context=edit&status=any&order=asc&per_page=$perPage&page=" . $page;
				$this->comment('Requesting: ' . $url);
				$res = $this->http->get($url, [
					'headers' => [
						'Authorization' => 'Bearer ' . $this->auth->data->token,
					],
					'debug' => $this->option('v'),
				]);
			}
			catch (RequestException $e) {
				// We get reached end of the records
				if ($e->getResponse()->getStatusCode() == 400) {
					$this->info('Queue finished');
					break;
				}

				// Any other HTTP error should fail the command
				throw $e;
			}

			$res = json_decode($res->getBody()->getContents());

			foreach ($res as $item) {
				if ($this->argument('add')) {
					if (Listing::find($item->id)) {
						$this->comment('Skipping: id - ' . $item->id);
						continue;
					}
				}

				dispatch(
					(new ImportListingJob($item))->onQueue('damprsearch')
				);
				$queued++;

				if ($this->limitReached($queued)) {
					break;
				}
			}

			$this->info('Queued ' . $queued . ' items for import');

			if ($this->limitReached($queued)) {
				break;
			}


			// Continue to the next page
			$page++;
		}
	}

	protected function limitReached($queued)
	{
		return $this->limit && $queued == $this->limit;
	}

	protected function authorize()
	{
		$uri = config('wp.url') . '/wp-json/jwt-auth/v1/token';
		$this->comment('Requesting: ' . $uri);
		$res  = $this->http->post($uri, [
			'form_params' => [
				'username' => 'enteron',
				'password' => '!cc123456br',
			],
			'debug' => $this->hasOption('v') ? $this->option('v') : false,
		]);
		$this->auth = json_decode($res->getBody());
		$this->comment('Token: <info>' . $this->auth->data->token . '</info>');
	}
}
