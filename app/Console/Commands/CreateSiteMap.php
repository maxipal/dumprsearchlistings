<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Listing;

class CreateSiteMap extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'create:sitemap';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create sitemap';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console commands
	 */
	public function handle()
	{
		$limit = 2000;
		$offset = 0;
		$step = 0;
		$urls = array();
		$modified = date('c');

		ini_set('memory_limit','4048M');

		while($listing = $this->get_listing($limit, $offset)){
			if(count($listing)){
				$offset = $offset + $limit;
			}else break;

			$item = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/').'/css/main-sitemap.xsl"?>'."\n";
			$item .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">'."\n";

			foreach ($listing as $count => $vehicle) {
				$galleries = $vehicle->getMedia('gallery');
				$xml_img = '';
				if($galleries){
					foreach ($galleries as $image) {
						$xml_img .= "<image:image>\n\t\t<image:loc>".$image->getUrl()."</image:loc>\n\t</image:image>\n";
					}
				}
				$item .= "<url>\n\t<loc>".config('wp.url').'/automjete/'.$vehicle->slug."</loc>\n\t<lastmod>$vehicle->updated_at</lastmod>\n\t<changefreq>weekly</changefreq>\n\t<priority>1</priority>\n\t".$xml_img."</url>\n";
			}
			$item .= "</urlset>\n";
    		$urls[] = url('/').'/listing-sitemap'.$step.'.xml';
    		file_put_contents(public_path('listing-sitemap'.$step.'.xml'), $item);
			$step++;
		}
		$main_xml = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="'.url('/').'/css/main-sitemap.xsl"?>'."\n";
		$main_xml .= '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
		foreach ($urls as $url) {
			$main_xml .= "\t<sitemap>\n\t\t<loc>$url</loc>\n\t\t<lastmod>$modified</lastmod>\n\t</sitemap>\n";
		}
		$main_xml .= '</sitemapindex>';

		file_put_contents(public_path('listing_index.xml'), $main_xml);
		return;
	}


	function get_listing($begin, $offset){
		return Listing::take($begin)->skip($offset)->get();
	}

}
