<?php

namespace App\Console\Commands;

class ImportListingsAttachmentsCommand extends ImportListingsCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:attachments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import missed attachments for all listings.';

    public function handle()
    {
		$this->authorize();

		$queued = 0;
		\App\Listing::query()
			->chunk(30, function ($results) use (&$queued) {
				$results->each(function (\App\Listing $listing) use (&$queued) {
//					if (count($aircraft->gallery_id) && $aircraft->brochure_id && $aircraft->valuation_pdf_id) {
//						return;
//					}

					$queued ++;
					dispatch(
						(new \App\Jobs\Wp\ImportListingAttachmentsJob($listing->id, $this->auth))
							->onQueue('import')
					);
				});

				$this->comment('Queued ' . $queued . ' records');
			});
    }
}
