<?php

namespace App\Console\Commands;

use App\WpFlushQueue;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

class WpFlushCommand extends Command
{

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'wp:flush {--limit=10} {--prime}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Flush queued WP pages';

	protected $counter = 0;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		// to safe resources
		if ($this->option('limit') > 100) {
			$this->error('Sorry, maximum 100 allowed');
			return 1;
		}

		WpFlushQueue::query()
			->toBase()
			->chunkById(10, function ($paths) {
				$this->processPaths($paths);

				if ($this->counter >= $this->option('limit')) {
					$this->comment('Limit reached.');
					// Laravel will stop iteration if false is returned
					return false;
				}

				return true;
			});

		return 0;
	}

	protected function processPaths(Collection $paths)
	{
		$wpRoot = config('wp.root') . '/';
		$w3PgCache = config('wp.w3_pgcache');

		foreach ($paths->pluck('path') as $path) {
			$cachePath = rtrim($wpRoot . $w3PgCache . $path, '/');
			if (!file_exists($cachePath)) {
				continue;
			}

			$files = File::glob($cachePath . '/*.html');
			if (!count($files)) {
				continue;
			}

			foreach ($files as $file) {
				if (File::delete($file)) {
					$this->line('<info>deleted:</info> ' . $file);
				}
			}

			if ($this->option('prime')) {
				$this->primeCache($path);
			}

			$this->counter ++;
		}

		WpFlushQueue::query()
			->whereIn('id', $paths->pluck('id'))
			->delete();
	}

	/**
	 * @param $path
	 */
	protected function primeCache($path): void
	{
		$url = config('wp.url') . $path;

		$agents = [
			'Laravel',
			'Laravel Mobile',
		];
		foreach ($agents as $agent) {
			(new Client())
				->get($url, [
					'http_errors' => false,
					'headers' => [
						'User-Agent' => $agent,
					],
				]);
		}

		$this->line('<info>primed:</info> ' . $url);
	}
}
