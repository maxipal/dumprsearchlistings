<?php

namespace App\Console;

use App\Console\Commands\WpFlushCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Jobs\ListingXMLImportPrepareJob;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
    	// Flushes queued WP pages
		$schedule->command(WpFlushCommand::class, [
			config('wp.flush_prime', true) ? '--prime' : '',
			'--limit' => config('wp.flush_limit', 30),
		])->everyMinute();

		// First import (al site)
        $schedule->job(
        	(new ListingXMLImportPrepareJob(false, false, true))
		)->weeklyOn(5,"00:00");
        // Second import (com site)
        $schedule->job(
        	(new ListingXMLImportPrepareJob('url_com', false, true))
		)->weeklyOn(5,"00:01");
        // Create sitemap on daily
        $schedule->command('create:sitemap')->daily();
        // Assign locations to listings
        // $schedule->command('import-location')->daily();
		$schedule->call([ListingXMLImportPrepareJob::class, 'updatesStatuses'])->everyMinute();

		// Clean up to maintain optimal storage usage
		$schedule->command('cleanup:import')->dailyAt("00:00");
		$schedule->command('cleanup:trashed')->dailyAt("03:00");
		$schedule->command('cleanup:elastic')->dailyAt('03:30');
		$schedule->command('cleanup:failed-jobs')->dailyAt("04:00");
		$schedule->command('responsecache:clear')->dailyAt("05:00");
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
