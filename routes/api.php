<?php

use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
	'prefix' => 'auth'
], function ($router) {
	Route::post('login', 'Auth\LoginController@login');
	Route::post('logout', 'Auth\LoginController@logout');
	Route::post('refresh', 'Auth\LoginController@refresh');
});

Route::middleware('auth:api')->get('user', 'AccountController@user');

Route::pattern('user', '\d+');
Route::resource('users', 'UserController');
Route::get('users/{user}/token', 'UserController@token');

Route::pattern('listing', '\d+');
Route::resource('listings', 'ListingController');
Route::put('listings/bulk-update', 'ListingController@bulkUpdate');
Route::post('listings/bulk-destroy', 'ListingController@bulkDestroy');
Route::get('listings/my', 'ListingController@my');
Route::get('listings/auctions', 'ListingController@auctions');
Route::get('listings/slug/{slug}', 'ListingController@slug');
Route::get('listings/attributes', 'ListingController@attributes');
Route::get('listings/filters', 'ListingController@filters');
Route::get('listings/terms', 'ListingController@terms');
Route::get('listings/get-terms', 'ListingController@singleTerms');
Route::get('listings/coordinates', 'ListingController@coordinates');
Route::get('listings/compare', 'ListingController@compare');
Route::post('listings/matches', 'ListingController@matches');
Route::post('listings/event', 'ListingController@event');
Route::post('listings/generated', 'ListingController@generated');
Route::get('listings/getImportStatus', 'ListingController@getImportStatus');


Route::pattern('term', '\d+');
Route::resource('terms', 'TermController');
Route::put('terms/bulk-update', 'TermController@bulkUpdate');
Route::post('terms/bulk-destroy', 'TermController@bulkDestroy');
Route::get('terms/slug/{slug}', 'TermController@slug');
Route::get('terms/taxonomies', 'TermController@taxonomies');
Route::get('terms/taxterm', 'TermController@termTaxonomies');
Route::get('terms/attributes', 'TermController@attributes');
Route::get('terms/suggestions', 'TermController@suggestions');

Route::get('terms/locations', 'TermController@locations');
Route::get('terms/sitemap', 'TermController@sitemap');
Route::get('terms/get-parents', 'TermController@getParentTerms');
Route::get('terms/import-features', 'TermController@importFeatures');


Route::pattern('listing-attribute', '\d+');
Route::resource('listing-attributes', 'ListingAttributesController');

// Administrative routes
Route::group(['middleware' => ['auth', 'can:administer']], function ($router) {
	$router->get('index-status', 'AdminController@indexStatus');
	$router->post('reindex/{entity}', 'AdminController@reindex');
});

Route::pattern('import-process', '\d+');
Route::resource('import-processes', 'ImportProcessesController');
Route::post('import-processes/start-xml-import', 'ImportProcessesController@startXmlImport')
	->middleware(['can:administer']);

Route::get('bits/bitAccept', 'AuctionController@bitAccept');
Route::resource('bits', 'AuctionController');
Route::post('bits/create', 'AuctionController@create');
Route::post('bits/hide', 'AuctionController@hide');
Route::post('bits/my', 'AuctionController@my');
Route::post('bits/carbits', 'AuctionController@carbits');

Route::get('get-mail', 'AuctionController@get_mail_templ');
Route::post('set-mail', 'AuctionController@set_mail_templ');

Route::get('settings/seo', 'SettingsController@listingsSeoForm');
Route::post('settings/seo', 'SettingsController@listingsSeo');

Route::post('settings/customize', 'SettingsController@saveCustomize');
