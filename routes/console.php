<?php

use App\Jobs\ListingCountriesTownsJob;
use App\Jobs\ListingXMLImportPrepareJob;
use App\Listeners\ListingObserver;
use App\Listing;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/


Artisan::command('dev:reset', function () {
	if (app()->environment() == 'production') {
		$this->error('Forbidden. App in production mode.');
		return;
	}

	Artisan::call('cache:clear');

	if (\App\User::indexExists()) {
		\App\User::deleteIndex();
	}
	\App\User::createIndex();

	if (\App\Term::indexExists()) {
		\App\Term::deleteIndex();
	}
	\App\Term::createIndex();

	if (\App\Listing::indexExists()) {
		\App\Listing::deleteIndex();
	}
	\App\Listing::createIndex();

	Artisan::call('migrate:fresh', ['--seed' => true]);
	$disk = \Illuminate\Support\Facades\Storage::disk('local');
	$dirs = $disk->directories('public');
	foreach ($dirs as $dir) {
		$disk->deleteDirectory($dir);
	}
	$disk->delete(array_diff($disk->files('public'), ['public/.gitignore']));
})->describe('Reset all data.');

Artisan::command('dev:users', function () {
	if (app()->environment() == 'production') {
		$this->error('Forbidden. App in production mode.');
		return;
	}

	$users = \App\User::query();
	$total_users = $users->count();

	$this->comment('Total to process: ' . $total_users);
	$bar = $this->output->createProgressBar($total_users);
	$bar->start();

	$users->chunk(100, function ($users) use ($bar) {
		foreach ($users as $user_key => $user) {
			\App\User::withoutEvents(function () use ($user) {
				$user->fillCounts()->save();
			});
			$bar->advance();
		}
	});

	$bar->finish();
})->describe('User operations for dev.');

Artisan::command('dev:generate-title', function () {
	\App\Listing::chunk(50, function (\Illuminate\Database\Eloquent\Collection $results) {
		$results->each(function ($listing) {
			$listing_data = \App\Listing::generatedData($listing);

			$listing->title = $listing_data['title'];

			$result = $listing->save();

			if ($result) {
				$this->comment('Processed ' . ($listing->id) . '. Title - ' . $listing_data['title']);
			} else {
				$this->comment('Failed ' . ($listing->id));
			}
		});
	});
});

Artisan::command('dev:fix-update-dates', function () {
	$this->count = 0;
	\App\Listing::query()
		->whereNotNull('import_id')
		->whereNotNull('import_updated_at')
		->where('updated_at', '!=', \Illuminate\Support\Facades\DB::raw('import_updated_at'))
		->chunkById(10, function (\Illuminate\Database\Eloquent\Collection $results) {
			$results->each(function (Listing $listing) {
				$listing->updated_at = $listing->import_updated_at;
				ListingObserver::skipNotifyingWp(function () use ($listing) {
					$listing->save();
					$this->count ++;
				});
			});
			$this->comment('Processed ' . $this->count);
		});
});

Artisan::command('dev:index-listing {{--id=}}', function ($id = false) {
	if (!$id) {
		$this->comment('Enter listing id');
	}
	$listing = \App\Listing::find($id);
	if ($listing) {
//		$listing->valuation_active = null;
		$listing->save();
		$this->comment('Indexed - ' . $id);
	} else {
		$this->comment('Listing not found');
	}
});

Artisan::command('import-xml {--force} {--sync} {--dry}', function () {
    dispatch_now(
		(new ListingXMLImportPrepareJob(
			false,
			$this->option('force'),
			$this->option('sync'),
			$this->option('dry')
		))
	);
})->describe('Import XML');

Artisan::command('import-xml-com {--force} {--sync} {--dry}', function () {
    dispatch_now(
		(new ListingXMLImportPrepareJob(
			'url_com',
			$this->option('force'),
			$this->option('sync'),
			$this->option('dry')
		))
	);
})->describe('Import XML from COM domain');


Artisan::command('import-location {--onlyxml}', function ($onlyxml = false) {
    dispatch_now(
		(new ListingCountriesTownsJob(false, $onlyxml))
			->onQueue('import')
	);
})->describe('Import Location');

Artisan::command('cleanup:trashed', function () {
	$query = \App\Listing::onlyTrashed()
		->where('deleted_at', '<', now()->subMonth()->toIso8601String());

	$query
		->chunkById(50, function (\Illuminate\Database\Eloquent\Collection $results) {
		$results->each(function (Listing $listing) {
			$listing->media->each->delete();
			$listing->forceDelete();
		});
	});
})->describe('Deletes obsolete trashed listings and its media attachments.');

Artisan::command('cleanup:elastic', function () {
	Listing::search()->chunk(1000, function (\Stylemix\Listing\Elastic\Collection $results) {
		$ids = $results->pluck('id');
		$exist = Listing::query()
			->whereIn('id', $ids->all())
			->pluck('id')
			->all();

		$delete = $results->whereNotIn('id', $exist);
		if ($delete->count()) {
			$delete
				->each->removeFromIndex();
			$this->info("Deleted {$delete->count()} entries");
		}
	});
})->describe('Deletes orphan Elastic Search entries whose appropriate DB records were deleted.');

Artisan::command('cleanup:failed-jobs', function () {
	$phrases = [
		'%DealerListingCreated%',
		'%ListingNotifyWpJob%',
	];
	foreach ($phrases as $phrase) {
		\Illuminate\Support\Facades\DB::table('failed_jobs')
			->where('payload', 'like', $phrase)
			->where('failed_at', '<', now()->subWeek()->toIso8601String())
			->delete();
	}
})->describe('Deletes obsolete failed jobs.');

Artisan::command('cleanup:import', function () {
	$disk = \Illuminate\Support\Facades\Storage::disk();
	$path = config('services.xml_import.storage_path');
	$files = $disk->allFiles($path);
	foreach ($files as $file) {
		$lastModified = \Carbon\Carbon::createFromTimestamp($disk->lastModified($file));
		if ($lastModified->lessThan(now()->subWeek())) {
			$disk->delete($file);
		}
	}
})->describe('Deletes obsolete downloaded import files.');

Artisan::command('queue:retry-on {queue}', function () {
	$ids = \Illuminate\Support\Facades\DB::table('failed_jobs')
		->where('queue', $this->argument('queue'))
		->pluck('id');

	$buffer = $this->getOutput();
	foreach ($ids as $id) {
		Artisan::call('queue:retry', compact('id'), $buffer);
	}
});
