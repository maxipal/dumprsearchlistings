@component('mail::message')
**User added listing.**<br>
User: {{ $user->name }} &lt;{{ $user->email }}&gt;, ID: {{ $user->id }}<br>
Listing: {{ $listing->title }}, ID: {{ $listing->id }}<br>

@component('mail::button', ['url' => env('WP_URL') . '/console/#/listing/' . $listing->id])
Check listing
@endcomponent

@endcomponent