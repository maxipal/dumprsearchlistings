<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Custom Elasticsearch Client Configuration
    |--------------------------------------------------------------------------
    |
    | This array will be passed to the Elasticsearch client.
    | See configuration options here:
    |
    | http://www.elasticsearch.org/guide/en/elasticsearch/client/php-api/current/_configuration.html
    */

    'config' => [
        'hosts' => [
			env('ELASTICSEARCH_HOST', 'localhost:9200'),
        ],
        'retries' => 1,
    ],


	'prefix' => env('ELASTICSEARCH_PREFIX', 'damprsearch-'),
];
