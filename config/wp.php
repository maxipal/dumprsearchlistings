<?php
/**
 * WordPress configuration
 */

return [

	'url' => rtrim(env('WP_URL', 'https://damprsearch.com'), '/'),

	'root' => rtrim(env('WP_ROOT', '/home/damprsearch/public_html'), '/'),

	// listing archive page path
	'listing_archive_path' => '/' . trim(env('WP_LISTING_ARCHIVE_PATH', 'inventory'), '/') . '/',

	// listing archive page path
	'listing_single_path' => '/' . trim(env('WP_LISTING_SINGLE_PATH', 'inventory'), '/') . '/',

	// prime page cache while flushing
	'flush_prime' => env('WP_FLUSH_PRIME', true),

	// number of primed page caches in a single call
	'flush_limit' => env('WP_FLUSH_LIMIT', 30),

	'w3_pgcache' => env('WP_W3_PGCACHE', 'wp-content/cache/page_enhanced/damprsearch.com'),

];
