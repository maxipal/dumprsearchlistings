<?php

return [
	/*
	 * Determine if the response cache middleware should be enabled.
	 */
	'enabled' => env('RESPONSE_CACHE_ENABLED', false),

	/*
	 *  The given class will determinate if a request should be cached. The
	 *  default class will cache all successful GET-requests.
	 *
	 *  You can provide your own class given that it implements the
	 *  CacheProfile interface.
	 */
	'cache_profile' => \App\Http\Cache\SuccessfulGetRequestsProfile::class,

	/*
	 * This setting determines if a http header named with the cache time
	 * should be added to a cached response. This can be handy when
	 * debugging.
	 */
	'add_cache_time_header' => env('RESPONSE_CACHE_HEADER', true),

	/*
	 * This setting determines the name of the http header that contains
	 * the time at which the response was cached
	 */
	'cache_time_header_name' => env('RESPONSE_CACHE_HEADER_NAME', 'X-Laravel-Cache'),

];
