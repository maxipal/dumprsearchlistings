<?php

return [
	'max_result_window' => env('LISTING_MAX_RESULT_WINDOW', 10000),
];
