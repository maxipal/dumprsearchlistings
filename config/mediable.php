<?php

return [

	/*
	 * The maximum file size in bytes for a single uploaded file
	 */
	'max_size' => 1024 * 1024 * 40,

];
