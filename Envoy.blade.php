@setup
$server = isset($server) ? $server : 'staging';
$servers = ['localhost' => '127.0.0.1'];
$deployment = false;
$deployments = [];

if (file_exists('.git/config')) {
	$ini = parse_ini_file('.git/config', true);
	foreach ($ini as $key => $section) {
		if (strpos($key, 'git-ftp ') === 0 && isset($section['url'])) {
			$key = str_replace('git-ftp ', '', $key);
			$deployments[$key] = parse_url($section['url']);
			if (isset($section['user'])) {
				$deployments[$key]['user'] = $section['user'];
			}
			$servers[$key] = $deployments[$key]['user'] . '@' . $deployments[$key]['host'];
		}
	}

	$deployment = isset($deployments[$server]) ? $deployments[$server] : null;
}
@endsetup

@servers($servers)

@task('install', ['on' => 'localhost'])
	cd stylemix
	git clone https://gitlab.com/azamath/laravel-listing-v01.git laravel-listing
	cd ..
	composer check-platform-reqs
	composer install --no-scripts --no-progress
	composer run-script post-root-package-install
	composer run-script post-create-project-cmd
	composer run-script post-autoload-dump
	php artisan storage:link
@endtask

@task('deploy-code', ['on' => 'localhost'])
	git ftp push -s {{ $server }}
@endtask

@task('deploy-remote', ['on' => $server])
	cd {{ $deployment['path'] }}
	php7.3 composer install --no-dev --no-progress
	@if ($server == 'production')
	php7.3 artisan config:cache
	php7.3 artisan route:cache
	@endif
	php7.3 artisan migrate
	php7.3 artisan queue:restart
	php7.3 artisan horizon:terminate
	chmod -R 0777 storage/framework storage/logs
@endtask

@story('deploy')
	deploy-code
	deploy-remote
@endstory
