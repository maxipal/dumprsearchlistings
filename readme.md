### Requirements
- Elastic Search running on localhost:9200
- Redis (optional for queues and Horizon)

### Installation

Clone or download from Git repository.

Installation and deployment scripts require [Envoy](https://laravel.com/docs/5.8/envoy) utility to be installed globally.

Run packages installer:
```bash
envoy run install
```

### Setup

Configure database, urls and other stuff in `.env`:
```
APP_URL=
WP_URL=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
ELASTICSEARCH_HOST=
```

Run setup script that will run database and elasticsearch initializations:
```bash
php artisan dev:reset
```

### Deployment
To upload changes install [git-ftp](https://git-ftp.github.io/) and configure uploading with specific scopes.
Each scope name is the name of the target server `staging`, `production`.

Deploy to specific server `staging`, `production`
```bash
envoy run deploy --server=staging
```

### Commands
Run command for generate sitemap in listings
``` bash
php artisan create:sitemap
```

Run command for generate sitemap in listings categories
``` bash
php artisan import-location --onlyxml
```

Run for create categories \body\make\model\country\city
``` bash
php artisan import-location
```
