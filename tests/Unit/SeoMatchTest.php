<?php
namespace Tests;

use App\Components\Seo;
use App\Term;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Request;

class SeoMatchTest extends TestCase
{
	use DatabaseMigrations;

	protected function setUp(): void
	{
		parent::setUp();

		factory(Term::class)->create(['title' => 'Type1', 'slug' => 'type1', 'taxonomy' => 'types']);
		factory(Term::class)->create(['title' => 'Make1', 'slug' => 'make1', 'taxonomy' => 'make']);
		factory(Term::class)->create(['title' => 'Serie1', 'slug' => 'serie1', 'taxonomy' => 'serie']);
	}

	/**
	 * @dataProvider matchesProvider
	 */
	public function testMatchInventoryLink($link, $match)
	{
		$seo = app(Seo::class);
		$this->assertEquals($match, $seo->matchInventoryPath($link));
	}

	public function matchesProvider()
	{
		return [
			[
				'link' => '/type1/',
				'match' => ['types' => 'type1'],
			],
			[
				'link' => '/make1/',
				'match' => ['make' => 'make1'],
			],
			[
				'link' => 'type1/make1',
				'match' => ['types' => 'type1', 'make' => 'make1'],
			],
			[
				'link' => '/type1/make1/serie1',
				'match' => ['types' => 'type1', 'make' => 'make1', 'serie' => 'serie1'],
			],
			[
				'link' => 'make1/serie1',
				'match' => ['make' => 'make1', 'serie' => 'serie1'],
			],
			[
				'link' => '/type1/make1/missed',
				'match' => null,
			],
		];
	}
}
