<?php
namespace Tests;

use App\Components\Seo;
use App\Term;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\Request;

class SeoTest extends TestCase
{
	use DatabaseMigrations;

	protected function setUp(): void
	{
		parent::setUp();

		factory(Term::class)->create(['title' => 'Type1', 'slug' => 'type1', 'taxonomy' => 'types']);
		factory(Term::class)->create(['title' => 'Make1', 'slug' => 'make1', 'taxonomy' => 'make']);
		factory(Term::class)->create(['title' => 'Serie1', 'slug' => 'serie1', 'taxonomy' => 'serie']);
	}

	/**
	 * @dataProvider linksProvider
	 *
	 * @param $params
	 * @param $link
	 */
	public function testInventoryLink($params, $link)
	{
		$request = Request::create('/', "GET", $params);
		$seo = new Seo($request);
		$result = $seo->inventorySeo();
		$this->assertEquals($link, $result['link']);
	}

	public function linksProvider() {
		return [
			[
				'params' => [
					'param1' => 'value1',
				],
				'link' => '?param1=value1',
			],
			[
				'params' => [
					'where' => ['types' => 'type1'],
				],
				'link' => '/type1/',
			],
			[
				'params' => [
					'filter' => ['types' => 'type1'],
				],
				'link' => '/type1/',
			],
			[
				'params' => [
					'where' => ['types' => 'type1', 'make' => 'make1'],
				],
				'link' => '/type1/make1/',
			],
			[
				'params' => [
					'where' => ['types' => 'type1', 'make' => 'make1', 'serie' => 'serie1'],
				],
				'link' => '/type1/make1/serie1/',
			],
			[
				'params' => [
					'where' => ['types' => 'type1'],
					's' => 'kkk',
				],
				'link' => '/type1/?s=kkk',
			],
		];
	}

}
