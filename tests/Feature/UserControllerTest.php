<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserControllerTest extends TestCase
{

	use DatabaseMigrations;

	public function testStoreCreating()
	{
		$this->assertEquals(0, User::count());

		$userData = factory(User::class)->make()->getAttributes();

		$this->withoutMiddleware()
			->postJson('api/users', $userData)
			->assertJsonFragment(['import_id' => $userData['import_id']]);

		$this->assertEquals(1, User::count());
	}

	public function testStoreUpdating()
	{
		$user = factory(User::class)->create();
		$this->assertEquals(1, User::count());

		$userData = $user->getAttributes();
		$this->withoutMiddleware()
			->postJson('api/users', $userData)
			->assertJsonFragment(['import_id' => $userData['import_id']]);

		$this->assertEquals(1, User::count());
	}
}
