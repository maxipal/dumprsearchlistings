<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Tests\TestCase;

class AuthorizationTest extends TestCase
{

	use DatabaseMigrations;

	public function testUserPolicy()
	{
		$gate = $this->app->make(GateContract::class);
		$user = factory(User::class)->create();
		$user2 = factory(User::class)->create();

		$this->assertTrue($gate->forUser($user)->allows('view', $user));
		$this->assertTrue($gate->forUser($user)->allows('view', $user2));
		$this->assertTrue($gate->forUser($user)->allows('update', $user));
		$this->assertFalse($gate->forUser($user)->allows('update', $user2));
		$this->assertFalse($gate->forUser($user)->allows('create', User::class));

		$admin = factory(User::class)->create(['roles' => ['administrator']]);

		$this->assertTrue($gate->forUser($admin)->allows('view', $user));
		$this->assertTrue($gate->forUser($admin)->allows('view', $user2));
		$this->assertTrue($gate->forUser($admin)->allows('create', User::class));
		$this->assertTrue($gate->forUser($admin)->allows('manage', User::class));
	}
}
