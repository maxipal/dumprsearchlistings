<?php

namespace Tests\Feature;

use App\WpFlushQueue;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WpFlushQueueTest extends TestCase
{
	use DatabaseMigrations;

	public function testPushing()
	{
		$path = 'path1';
		$path2 = 'path2';
		$table = (new WpFlushQueue())->getTable();

		WpFlushQueue::pushItem($path);
		$this->assertDatabaseHas($table, compact('path'));

		WpFlushQueue::pushItems([$path, $path2]);
		$this->assertDatabaseHas($table, ['path' => $path2]);
	}
}
