<?php

use Faker\Generator as Faker;
/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Listing::class, function (Faker $faker) {
    return [
    	'status' => $faker->randomElement(['publish', 'unpublished']),
		'slug' => $faker->slug,
		'title' => \Illuminate\Support\Str::random(10),
    ];
});

$factory->state(\App\Listing::class, 'is_special', function (\Faker\Generator $faker) {
	return [
		'is_special' => 'true',
	];
});
