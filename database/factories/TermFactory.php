<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Term::class, function (Faker $faker) {
	$title = $faker->word;

	return [
        'title' => $title,
		'slug' => \Illuminate\Support\Str::slug($title),
		'taxonomy' => $faker->randomElement(['types', 'make', 'serie']),
    ];
});
