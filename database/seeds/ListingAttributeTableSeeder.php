<?php

use App\Attributes\Manager;
use App\Attributes\TermRelation;
use Illuminate\Database\Seeder;
use Stylemix\Listing\Attribute\Keyword;
use Stylemix\Listing\Attribute\Numeric;

class ListingAttributeTableSeeder extends Seeder
{

	public function run()
	{
		foreach ($this->attributes() as $i => $attribute) {
			$model = \App\ListingAttribute::firstOrNew(['name' => $attribute->name]);
			$model->type = get_class($attribute);
			$model->name = $attribute->name;
			$model->options = \Illuminate\Support\Arr::except($attribute->toArray(), ['name', 'type']);
			$model->order = $i;
			$model->save();
		}

		app(Manager::class)->flush();
	}

	/**
	 * @return \Stylemix\Listing\Attribute\Base[]
	 */
	protected function attributes()
	{
		return [
			TermRelation::make('condition')
				->order(1)
				->formOrder(1)
				->label('gjendjen')
				->required()
				->useInSingle()
				->useInCompare()
				->useInFilter(),
			TermRelation::make('body')
				->order(2)
				->formOrder(4)
				->label('llojin')
				->icon('stm-service-icon-body_type')
				->useInSingle()
				->required()
				->useInFilter()
				->useInGrid()
				->useInList()
				->useInCompare()
				->useAsCheckboxes(),
			TermRelation::make('make')
				->order(3)
				->formOrder(2)
				->label('markën')
				->required()
				->useInSingle()
				->useInCompare()
				->useInFilter(),
			TermRelation::make('serie')
				->order(4)
				->formOrder(3)
				->label('modelin')
				->useInFilter()
				->useInSingle()
				->required()
				->useInCompare()
				->hierarchical(),
			Numeric::make('mileage')
				->label('kilometrazhën')
				->affix('km')
				->order(5)
				->icon('stm-icon-road')
				->integer()
				->useInList()
				->useInGrid()
				->useInMap()
				->useInCompare()
				->useInSingle()
				->useInFilter()
				->filterOptions([
					'min' => 0,
					'max' => 500000,
				]),
			TermRelation::make('fuel')
				->label('llojin e karburantit')
				->order(6)
				->icon('stm-icon-fuel')
				->useInSingle()
				->useInGrid()
				->useInList()
				->useInCompare()
				->useInFilter(),
			TermRelation::make('targa')
				->label('Targa')
				->order(7)
				->useInSingle()
				->useInCompare()
				->useInFilter(),
			Keyword::make('engine')
				->label('specifikimin e motorit')
				->icon('stm-icon-engine_fill')
				->order(8)
				->useInSingle()
				->useInCompare()
				->useInFilter(),
			Numeric::make('years')
				->order(9)
				->formOrder(5)
				->label('vitin')
				->icon('stm-icon-road')
				->required()
				->integer()
				->useInCompare()
				->useInSingle()
				->useInGrid()
				->useInList()
				->useInFilter()
				->filterOptions([
					'min' => 1950,
					'max' => 2019,
				]),
			Numeric::make('price-advanced')
				->label('çmimin')
				->affix('€')
				->order(9)
				->icon('stm-icon-road')
				->useInSingle()
				->useInCompare()
				->useInFilter(),
			TermRelation::make('transmission')
				->label('transmetimin')
				->order(10)
				->icon('stm-icon-transmission_fill')
				->useInSingle()
				->useInGrid()
				->useInList()
				->useInCompare()
				->useInFilter(),
			TermRelation::make('drive')
				->label('Drive')
				->useInSingle()
				->order(11)
				->useInCompare()
				->icon('stm-icon-drive_2')
				->useInFilter(),
			TermRelation::make('exterior-color')
				->label('ngjyrën e jashtme')
				->order(12)
				->useInSingle()
				->useInCompare()
				->icon('stm-service-icon-color_type'),
			TermRelation::make('interior-color')
				->label('ngjyrën e brendshme')
				->order(13)
				->useInSingle()
				->useInCompare()
				->icon('stm-service-icon-color_type'),
		];
	}
}
