<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\App\User::firstOrNew(['email' => 'wordpress@damprsearch.com'])
			->forceFill([
				'import_id' => 0,
				'name' => 'WordPress',
				'email' => 'wordpress@damprsearch.com',
				'password' => bcrypt('7$kQWA'),
				'roles' => ['administrator'],
			])
			->save();
	}
}
