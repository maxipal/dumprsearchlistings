<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(UsersTableSeeder::class);
		$this->call(ListingAttributeTableSeeder::class);
		$this->call(ListingsTableSeeder::class);
		\Settings::set('limits', [
			'user' => [
				'posts' => 50,
				'images' => 10,
			],
			'dealer' => [
				'posts' => 50,
				'images' => 15,
			],
		]);
	}
}
