<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status', 16)->default('draft')->index();
            $table->string('slug')->index();
            $table->string('title');
            $table->unsignedInteger('author_id')->nullable();
			$table->unsignedInteger('import_group_id')->nullable()->index();
			$table->unsignedInteger('import_id')->nullable()->index();
            $table->timestamp('import_updated_at')->nullable()->index();
			$table->timestamps();
			$table->timestamp('indexed_at')->nullable()->index();
            $table->softDeletes();
        });

		Schema::create('listing_data', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('entity_id');
			$table->foreign('entity_id')->references('id')->on('listings')->onDelete('cascade')->onUpdate('cascade');
			$table->string('lang', 5)->nullable();
			$table->string('name', 32);
			$table->longText('value');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
		Schema::dropIfExists('listing_data');
        Schema::dropIfExists('listings');
    }
}
