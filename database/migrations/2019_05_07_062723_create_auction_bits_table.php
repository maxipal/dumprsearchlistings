<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionBitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auction_bits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('VID')->nullable()->index();
            $table->unsignedInteger('UID')->nullable()->index();
            $table->timestamp('date_added');
            $table->longText('offer');
            $table->unsignedInteger('accepted')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auction_bits');
    }
}
