<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_listings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('import_group_id')->nullable()->index();
            $table->unsignedInteger('import_id')->nullable()->index();
            $table->unsignedInteger('import_processes_id')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_listings');
    }
}
