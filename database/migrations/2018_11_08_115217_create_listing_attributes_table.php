<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingAttributesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return  void
	 */
	public function up()
	{
		Schema::create('listing_attributes', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 32);
			$table->string('type', 64)->index();
			$table->text('options')->nullable();
			$table->integer('order')->default(0)->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return  void
	 */
	public function down()
	{
		Schema::dropIfExists('listing_attributes');
	}
}
