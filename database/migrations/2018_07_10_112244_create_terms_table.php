<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
			$table->string('taxonomy')->index();
			$table->string('title')->index();
            $table->string('slug')->index();
            $table->unsignedInteger('parent_id')->nullable()->index();
			$table->integer('order')->default(0)->index();
			$table->unsignedTinyInteger('level')->default(0)->index();
			$table->unsignedInteger('import_id')->nullable()->index();
			$table->timestamps();
			$table->timestamp('indexed_at')->nullable()->index();
			$table->softDeletes();
        });

		Schema::create('term_data', function (Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('entity_id');
			$table->foreign('entity_id')->references('id')->on('terms')->onDelete('cascade')->onUpdate('cascade');
			$table->string('lang', 5)->nullable();
			$table->string('name', 32);
			$table->longText('value');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::dropIfExists('term_data');
        Schema::dropIfExists('terms');
    }
}
